<?php

namespace Svkuaod\PageConstructor\Models\Blocks;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class BlockInfo extends BaseInfoModel
{
    protected static function getBaseTable()
    {
        return Block::TABLE;
    }

}
