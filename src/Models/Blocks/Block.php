<?php

namespace Svkuaod\PageConstructor\Models\Blocks;

use Illuminate\Database\Eloquent\Builder;
use Svkuaod\PageConstructor\Models\BaseModel;
use Svkuaod\PageConstructor\Traits\JsonData;

class Block extends BaseModel
{
    use JsonData;

    const TABLE = 'blocks';
    protected $table = self::TABLE;

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
        }

        static::saving(function (Block $model){
            $model->key = static::getModelKey();
        });
    }

    public function info()
    {
        return $this->hasOne(BlockInfo::class, BlockInfo::getForeignField(), 'id');
    }

}
