<?php

namespace Svkuaod\PageConstructor\Models\Blocks\Custom;

use SleepingOwl\Admin\Traits\OrderableModel;
use Svkuaod\PageConstructor\Components\Services\StoreFile;
use Svkuaod\PageConstructor\Models\Blocks\Block;

class SocialBlock extends Block
{
    use OrderableModel;

    public static function getModelKey(){
        return 'social';
    }

    public function getImagePathAttribute(){
        return StoreFile::getDataStoragePath($this->getDataField('image'));
    }

    public function setImagePathAttribute($value)
    {
        $this->setPhotoToData('image',$value,'main_slider');
    }

    public function getImageTwoPathAttribute(){
        return StoreFile::getDataStoragePath($this->getDataField('image_two'));
    }

    public function setImageTwoPathAttribute($value)
    {
        $this->setPhotoToData('image_two',$value,'main_slider');
    }

    public function getLinkPathAttribute(){
        return $this->getDataField('link_path');
    }

    public function setLinkPathAttribute($value)
    {
        $this->setDataField('link_path',$value);
    }
}
