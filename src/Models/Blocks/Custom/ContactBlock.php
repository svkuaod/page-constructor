<?php

namespace Svkuaod\PageConstructor\Models\Blocks\Custom;

use Svkuaod\PageConstructor\Models\Blocks\Block;
use Svkuaod\PageConstructor\Traits\JsonData;

class ContactBlock extends Block
{
    use JsonData;

    public static function getModelKey(){
        return 'contacts';
    }

    public function getPhonesAttribute(){
        return $this->getDataField('phones');
    }

    public function setPhonesAttribute($value)
    {
        $this->setDataField('phones',$value);
    }

    public function getPhonesLinksAttribute(){
        return $this->getDataField('phones_links');
    }

    public function setPhonesLinksAttribute($value)
    {
        $this->setDataField('phones_links',$value);
    }

    public function getEmailsAttribute(){
        return $this->getDataField('emails');
    }

    public function setEmailsAttribute($value)
    {
        $this->setDataField('emails',$value);
    }
}
