<?php

namespace Svkuaod\PageConstructor\Models\Menu;

use Svkuaod\PageConstructor\Components\Services\StoreFile;
use Svkuaod\PageConstructor\Models\BaseModel;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;
use Svkuaod\PageConstructor\Components\Services\UrlGenerator;

class Menu extends BaseModel
{
    const TABLE = 'menu';
    protected $table = self::TABLE;

    public static function getMenuKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function (Menu $model){
            $model->key = static::getMenuKey();
        });
    }

    public function setUrlAdminAttribute()
    {
        $this->url = UrlGenerator::getUniqueUrl($_POST['urlAdmin'] ?? '', $_POST['name'] ?? '', $this->attributes['id'] ?? 0,$this);
    }

    public function getUrlAdminAttribute()
    {
        return $this->url;
    }

    public function getNameAdminAttribute()
    {
        return $this->info->name ?? '';
    }

    public function getIconPathAttribute(){
        return StoreFile::getStoragePath($this,'icon');
    }

    public function setIconPathAttribute($value)
    {
        if ($this->iconPath == $value) return true;
        if(!isset($this->attributes['id'])) $this->save();
        StoreFile::deleteFileByPath($this->icon);
        if(!$value) return true;
        $folderPath = StoreFile::getMainPhotoFolder($this);
        $this->icon = StoreFile::getStoredFilePath($folderPath, new File($value));
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->with('info', 'constructor');
    }

    public function subMenu()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->where('active', 1)->orderBy('order');
    }

    public function constructor()
    {
        return $this->hasOne(Constructor::class, 'id', 'constructor_id')->with('info');
    }

    public function info()
    {
        return $this->hasOne(MenuInfo::class, 'menu_id', 'id');
    }
}
