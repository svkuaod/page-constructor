<?php

namespace Svkuaod\PageConstructor\Models\Menu;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class MenuInfo extends BaseInfoModel
{
    protected static function getBaseTable()
    {
        return Menu::TABLE;
    }
}
