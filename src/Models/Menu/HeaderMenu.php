<?php

namespace Svkuaod\PageConstructor\Models\Menu;

use Illuminate\Database\Eloquent\Builder;

class HeaderMenu extends Menu
{
    public static function getMenuKey(){
        return 'header_menu';
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('key', function (Builder $builder) {
            $builder->where('key', static::getMenuKey());
        });
    }

}