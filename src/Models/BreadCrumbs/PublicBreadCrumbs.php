<?php

namespace Svkuaod\PageConstructor\Models\Constructor;


use Svkuaod\PageConstructor\Components\ConstructorConfig;

class PublicBreadCrumbs
{
    private static $instance;
    private $view;
    private $itemsArray = [];

    public function __construct(string $view)
    {
        $this->view = $view;
    }

    public static function getInstance(string $view = null)
    {
        $pathBreadCrumbs = ConstructorConfig::getInstance()->getTemplateFolderPath() . '.widgets.bread_crumbs';
        if (!(self::$instance instanceof self))
            self::$instance = new self($view ?: $pathBreadCrumbs);

        return self::$instance;
    }

    public function render(): string
    {
        return view($this->view, ['items' => $this->itemsArray])->render();
    }

    public function addItem(string $name, string $url): self
    {
        $this->itemsArray[$url] = compact('name', 'url');
        return $this;
    }

    public function clear(): self
    {
        $this->itemsArray = [];
        return $this;
    }

}
