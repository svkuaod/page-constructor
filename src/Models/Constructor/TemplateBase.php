<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

class TemplateBase extends Template
{
    const TABLE = 'templates_base';
    protected $table = self::TABLE;

    public static function getMorphKey()
    {
        return 'base';
    }
}
