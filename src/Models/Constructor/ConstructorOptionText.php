<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Svkuaod\PageConstructor\Models\BaseModel;

class ConstructorOptionText extends BaseModel
{
    const TABLE = 'constructor_option_texts';
    const KEY_TEXT = 'text';

    protected $table = self::TABLE;

    protected $fillable = ['option_id', 'key'];

    public function info()
    {
        return $this->hasOne(ConstructorOptionTextInfo::class, 'option_texts_id', 'id');
    }

    public function option()
    {
        return $this->belongsTo(ConstructorOption::class, 'option_id', 'id');
    }
}
