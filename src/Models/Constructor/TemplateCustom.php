<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

class TemplateCustom extends Template
{
    const TABLE = 'templates_custom';
    protected $table = self::TABLE;

    public static function getMorphKey()
    {
        return 'custom';
    }

}
