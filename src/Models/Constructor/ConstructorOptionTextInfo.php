<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class ConstructorOptionTextInfo extends BaseInfoModel
{
    protected $fillable = ['option_texts_id', 'value'];


    protected static function getBaseTable()
    {
        return ConstructorOptionText::TABLE;
    }
}
