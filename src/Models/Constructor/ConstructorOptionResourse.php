<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Illuminate\Database\Eloquent\Builder;
use Svkuaod\PageConstructor\Models\BaseModel;

class ConstructorOptionResourse extends BaseModel
{
    const TABLE = 'constructor_option_resources';
    protected $table = self::TABLE;

    protected $fillable = ['option_id', 'key', 'parent_id'];

    public function info()
    {
        return $this->hasOne(ConstructorOptionResourceInfo::class, 'option_resources_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->with('childs', 'info');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function option()
    {
        return $this->belongsTo(ConstructorOption::class, 'option_id', 'id');
    }

    public function optionText(){
        return $this->hasOne(ConstructorOptionText::class,'resource_id','id');
    }

    public function getChildrenValueByKey($key){
        return $this->childs->where('key',$key)->first()->info->value ?? null;
    }

    public function getChildrenValuePathByKey($key){
        return $this->childs->where('key',$key)->first()->info->valuePath ?? null;
    }

    public function getChildrenValuePathByKeySmall($key){
        return $this->childs->where('key',$key)->first()->info->valuePathSmall ?? null;
    }

    public function getChildrenValueSvgPathByKey($key){
        return $this->getSvgPath($this->getChildrenValueByKey($key));
    }

    public function getSvgPath($path = null){
        $path = $path ? $path : $this->info->value ?? null;
        if(!$path) return null;
        $path = @file_get_contents(storage_path('app/public/' . $path));
        if(strpos($path,'<svg') !== false) return $path;
        return null;
    }
}
