<?php

namespace Svkuaod\PageConstructor\Models\Constructor\Custom;

use Svkuaod\PageConstructor\Components\SEO\Seoable;
use Svkuaod\PageConstructor\Components\SEO\SeoableInterface;

class MainPage extends CustomPage implements SeoableInterface
{
    use Seoable;

    public static function getPageKey() :string{
        return 'main_page';
    }
}