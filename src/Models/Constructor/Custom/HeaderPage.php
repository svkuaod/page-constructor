<?php

namespace Svkuaod\PageConstructor\Models\Constructor\Custom;

class HeaderPage extends CustomPage
{
    public static function getPageKey() : string{
        return 'header_page';
    }

}