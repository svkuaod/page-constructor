<?php

namespace Svkuaod\PageConstructor\Models\Constructor\Custom;;

class FooterPage extends CustomPage
{
    public static function getPageKey() : string{
        return 'footer_page';
    }
}