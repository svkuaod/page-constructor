<?php

namespace Svkuaod\PageConstructor\Models\Constructor\Custom;

use Svkuaod\PageConstructor\Components\Descriptions\Descriptionable;
use Svkuaod\PageConstructor\Components\Descriptions\DescriptionableInterface;
use Svkuaod\PageConstructor\Components\SEO\Seoable;
use Svkuaod\PageConstructor\Components\SEO\SeoableInterface;

class InfoPage extends CustomPage implements SeoableInterface,DescriptionableInterface
{
    use Seoable;
    use Descriptionable;

    public static function getPageKey():string {
        return 'info_page';
    }
}