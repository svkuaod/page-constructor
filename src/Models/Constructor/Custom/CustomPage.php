<?php

namespace Svkuaod\PageConstructor\Models\Constructor\Custom;

use Illuminate\Database\Eloquent\Builder;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;

abstract class CustomPage extends Constructor
{
    public abstract static function getPageKey() : string;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('key', function (Builder $builder) {
            $builder->where('key', static::getPageKey());
        });

        static::saving(function (CustomPage $model){
            $model->key = $model->getPageKey();
        });
    }
}