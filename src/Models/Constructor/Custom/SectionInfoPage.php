<?php

namespace Svkuaod\PageConstructor\Models\Constructor\Custom;

class SectionInfoPage extends CustomPage
{
    public static function getPageKey() : string{
        return 'section_info_page';
    }
}