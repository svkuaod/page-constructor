<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Illuminate\Database\Eloquent\Relations\Relation;
use Svkuaod\PageConstructor\Models\BaseModel;

class ConstructorOption extends BaseModel
{
    const TABLE = 'constructor_options';
    protected $table = self::TABLE;

    protected $fillable = ['template_id', 'template_type', 'order'];

    public function page()
    {
        return $this->belongsTo(Constructor::class, 'constructor_id', 'id')->with('info');
    }

    public function template()
    {
        Relation::morphMap([
            TemplateBase::getMorphKey() => TemplateBase::class,
            TemplateCustom::getMorphKey() => TemplateCustom::class
        ]);
        return $this->morphTo();
    }

    public function resources()
    {
        return $this->hasMany(ConstructorOptionResourse::class, 'option_id', 'id')->with('info', 'childs', 'option')->orderBy('order');
    }

    public function text()
    {
        return $this->hasOne(ConstructorOptionText::class, 'option_id', 'id')->with('info');
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->with('childs', 'resources', 'text')->orderBy('order');
    }

    public static function getOptionById($id) : self{
        self::where('id', $id)->with(['childs', 'text', 'resources'])->first();
    }
}
