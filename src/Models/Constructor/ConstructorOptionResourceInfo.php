<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class ConstructorOptionResourceInfo extends BaseInfoModel
{

    protected $fillable = ['option_resources_id', 'value'];

    public function getValuePathAttribute()
    {
        if(!isset($this->attributes['value'])) return null;
        $storagePrefix = substr($this->attributes['value'],0,1) === '/' ? '/storage' : '/storage/';
        return $storagePrefix . $this->attributes['value'];
    }

    public function getValuePathSmallAttribute()
    {
        if(!isset($this->attributes['value'])) return null;
        $storagePrefix = substr($this->attributes['value'],0,1) === '/' ? '/storage' : '/storage/';
        $fillename = last(explode('/',$this->attributes['value']));
        return $storagePrefix . str_replace($fillename,'s_' . $fillename, $this->attributes['value']);
    }

    protected static function getBaseTable()
    {
        return ConstructorOptionResourse::TABLE;
    }
}
