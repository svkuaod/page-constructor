<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class ConstructorInfo extends BaseInfoModel
{
    protected static function getBaseTable()
    {
        return Constructor::TABLE;
    }
}
