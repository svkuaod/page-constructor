<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Illuminate\Http\File;
use Svkuaod\PageConstructor\Components\Services\StoreFile;
use Svkuaod\PageConstructor\Models\BaseModel;

abstract class Template extends BaseModel
{
    protected $folderPath = 'constructor/templates/';

    public static abstract function getMorphKey();

    public function setPhotoAdminAttribute($value)
    {
        if ($this->getPhotoAdminAttribute() == $value) return true;
        StoreFile::deleteFileByPath($this->photoPublicPath);
        $this->photo = StoreFile::getStoredFilePath($this->folderPath . $this->attributes['id'], new File($value));
    }

    public function getPhotoAdminAttribute()
    {
        return $this->photo;
    }

    public function getNameAdminAttribute()
    {
        return $this->name ?? '';
    }

    public function getPhotoAttribute()
    {
        if (!$this->attributes['photo']) return null;
        if (file_exists('storage/' . $this->attributes['photo'])) return '/storage/' . $this->attributes['photo'];
        return file_exists($this->attributes['photo']) ? '/' . $this->attributes['photo'] : null;
    }

    public function getPhotoPublicPathAttribute()
    {
        return $this->attributes['photo'] ?? null;
    }

    public static function getTemplateClassByType($type)
    {
        switch ($type) {
            case TemplateBase::getMorphKey():
                return TemplateBase::class;
                break;
            case TemplateCustom::getMorphKey():
                return TemplateCustom::class;
                break;
            default: return null;
        }
    }
}