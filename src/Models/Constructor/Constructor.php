<?php

namespace Svkuaod\PageConstructor\Models\Constructor;

use Svkuaod\PageConstructor\Components\Services\UrlGenerator;
use Svkuaod\PageConstructor\Models\BaseModel;

class Constructor extends BaseModel
{
    const TABLE = 'constructor';
    protected $table = self::TABLE;

    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = UrlGenerator::getUniqueUrl($_POST['url'] ?? $value, $_POST['info']['name'] ?? $value, $this->attributes['id'] ?? 0,$this);
    }

    public function info()
    {
        return $this->hasOne(ConstructorInfo::class, 'constructor_id', 'id');
    }

    public function options()
    {
        return $this->hasMany(ConstructorOption::class, 'constructor_id', 'id')->with('resources', 'childs', 'text', 'template')->orderBy('order');
    }

    public static function getPageByUrl($url) : ?self{
        return static::getActivePage(static::where('url', $url));
    }

    public static function getPageById($id) : ?self{
        return static::getActivePage(static::where('id', $id));
    }

    public static function getPageByKey($key) : ?self{
        return static::getActivePage(static::where('key', $key));
    }

    private static function getActivePage($builder) : ?self{
        return $builder->where('active', 1)->with(['info', 'options' => function ($q) {
            $q->where('active', 1);
        }])->first();
    }
}
