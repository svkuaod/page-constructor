<?php

namespace Svkuaod\PageConstructor\Models\MarketingCodes;

use Svkuaod\PageConstructor\Models\BaseModel;

class MarketingCode extends BaseModel
{
    const HEAD_KEY = 'head';
    const BODY_TOP_KEY = 'body_top';
    const BODY_BOTTOM_KEY = 'body_bottom';

    private static $codes;

    const TABLE = 'marketing_codes';
    protected $table = self::TABLE;

    public static function getArrKeys(){
        return [self::HEAD_KEY,self::BODY_TOP_KEY,self::BODY_BOTTOM_KEY];
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->orderBy('order');
    }

    public static function getCodesByKey($key){
        $res = '';
        if(!self::$codes) self::$codes = self::whereNull('parent_id')->whereNotNull('key')->where('active',1)
            ->with(['children'=>function ($q){$q->where('active',1);}])->get()->keyBy('key');
        if(self::$codes[$key] ?? false){
            foreach (self::$codes[$key]->children as $item){
                $res .= $item->body;
            }
        }
        return $res;
    }



}
