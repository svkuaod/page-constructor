<?php

namespace Svkuaod\PageConstructor\Models;

use Illuminate\Database\Eloquent\Model;
use Svkuaod\PageConstructor\Components\ConstructorConfig;

abstract class BaseInfoModel extends Model
{
    protected $table;

    public static function getTablePrefix(){
        return static::getBaseTable() . self::getInfoTableSuffix();
    }

    protected static abstract function getBaseTable();

    private static function getInfoTableSuffix(){
        return '_info_';
    }

    public static function getForeignField(){
        return static::getBaseTable() . '_id';
    }

    public function setLocaleToTable($locale){
        $this->table = static::getTablePrefix() . $locale;
    }

    public function __construct()
    {
        parent::__construct();
        if(isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'],'/admin/') === 0) {
            $locale = session('locale') ?? ConstructorConfig::getInstance()->getDefaultLanguage();
        }
        else $locale = app()->getLocale() ?? ConstructorConfig::getInstance()->getDefaultLanguage();
        $this->table =  static::getTablePrefix() . $locale;
    }
}