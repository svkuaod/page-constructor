<?php

namespace Svkuaod\PageConstructor\Models\Subscribers;

use Svkuaod\PageConstructor\Models\BaseModel;

class Subscriber extends BaseModel
{
    const TABLE = 'subscribers';
    protected $table = self::TABLE;

    public static function getFormPath(){
        return '/add-subscriber';
    }

}