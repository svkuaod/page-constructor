<?php

namespace Svkuaod\PageConstructor\Models\Translates;

use Svkuaod\PageConstructor\Models\BaseModel;

class Translate extends BaseModel
{
    const TABLE = 'translates';
    protected $table = self::TABLE;

    public function info()
    {
        return $this->hasOne(TranslateInfo::class, TranslateInfo::getForeignField(), 'id');
    }
}
