<?php

namespace Svkuaod\PageConstructor\Models\Translates;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class TranslateInfo extends BaseInfoModel
{
    protected static function getBaseTable()
    {
        return Translate::TABLE;
    }
}
