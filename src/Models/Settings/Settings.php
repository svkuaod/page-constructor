<?php

namespace Svkuaod\PageConstructor\Models\Settings;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\BaseModel;
use Svkuaod\PageConstructor\Traits\JsonData;

class Settings extends BaseModel
{
    const TABLE = 'settings';
    protected $table = self::TABLE;
    protected $fillable = ['key','value'];
    const KEY_EMAIL_ADMIN = 'email_admin';

    public static function getEmailAdmin()
    {
        $mails =  self::where('key', self::KEY_EMAIL_ADMIN)->pluck('value')->first();
        return explode(';',str_replace(' ','',$mails));
    }

    public static function getArrKeys()
    {
        return ConstructorConfig::getInstance()->getSettingsKeysArr() ?? [self::KEY_EMAIL_ADMIN];
    }
    
    

}