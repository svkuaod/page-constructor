<?php

namespace Svkuaod\PageConstructor\Models\FeedBack;

class FeedBack extends FeedBackBase
{

    public static function getTableKey(){
        return 'feedback';
    }

    public function getAdminSectionPath(){
        return 'feed_back';
    }

    public function getDescription()
    {
        return 'Форма обратной связи';
    }
}
