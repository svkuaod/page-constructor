<?php

namespace Svkuaod\PageConstructor\Models\FeedBack;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

use Svkuaod\PageConstructor\Models\BaseModel;
use Svkuaod\PageConstructor\Models\Settings\Settings;
use Svkuaod\PageConstructor\Services\Mail\FeedBack;

abstract class FeedBackBase extends BaseModel
{
    const TABLE = 'feedback';
    protected $table = self::TABLE;

    public abstract static function getTableKey();

    public abstract function getAdminSectionPath();

    public abstract function getDescription();

    public static function getControllerActionPath(){
        return '/feedback/';
    }

    public static function getFullActionPath(){
        return self::getControllerActionPath() . static::getTableKey();
    }

    protected function validateRules():array{
        return [
            'name' => 'max:128',
            'phone' => 'required|min:18|max:18',
            'email' => 'email|max:128',
            'target_id' => 'int',
        ];
    }

    public function store(Request $request){
        $request->validate($this->validateRules());
        $this->name = $request->name;
        $this->phone = $request->phone;
        $this->body = $this->getBodyJSon($request);
        $this->target_id = $request->target_id;
        $this->email = $request->email;
        $this->save();
        $this->sendNotifications();
        if ($request->ajax()) return json_encode(['status' => 'success']);
        else return back();
    }

    public function sendNotifications(){
        try{
            (new FeedBack($this))->sendMail(Settings::getEmailAdmin());
        }
        catch (\Exception $e){
            Log::error($e->getMessage());
        }
    }

    protected function getBodyJSon(Request $request){
        $bodyFields = $request->except(['_token','name','phone','email','target_id']);
        return count($bodyFields) ? json_encode($bodyFields) : null;
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->key = static::getTableKey();
        });
        static::updating(function ($model) {
            $model->admin_id = auth()->id();
        });
        static::addGlobalScope('key', function (Builder $builder) {
            $builder->where('key', static::getTableKey());
        });
    }

}
