<?php

namespace Svkuaod\PageConstructor\Models\Reviews;

use Illuminate\Database\Eloquent\Builder;

class ReviewMain extends ReviewBase
{
    public static function getFormPath()
    {
        return '';
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('main_page', function (Builder $builder) {
            $builder->where('main_page', 1)->where('archive', 0);
        });
    }
}
