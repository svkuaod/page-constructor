<?php

namespace Svkuaod\PageConstructor\Models\Reviews;

use Illuminate\Database\Eloquent\Builder;

class ReviewPublic extends ReviewBase
{
    public static function getFormPath()
    {
        return '/reviews/public/add';
    }
}
