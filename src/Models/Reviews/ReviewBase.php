<?php

namespace Svkuaod\PageConstructor\Models\Reviews;

use Svkuaod\PageConstructor\Models\BaseModel;

abstract class ReviewBase extends BaseModel
{
    const TABLE = 'reviews';
    protected $table = self::TABLE;

    protected $fillable = ['name', 'email', 'country', 'body', 'ip'];

    public abstract static function getFormPath();
}
