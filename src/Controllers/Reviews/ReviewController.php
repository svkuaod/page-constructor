<?php

namespace Svkuaod\PageConstructor\Controllers\Reviews;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Svkuaod\PageConstructor\Models\Reviews\ReviewPublic;

class ReviewController extends \Svkuaod\PageConstructor\Controllers\Controller
{
    public function addPublicReview(Request $request){
        $request->validate([
            'name' => 'max:250',
            'phone' => 'max:250',
            'body' => 'required|max:2000',
        ]);
        $userIp = \Illuminate\Support\Facades\Request::ip();
        $check = $this->isApprovedByIpActivity($userIp);
        if(!$check) return json_encode(['status' => 'error','head'=>Lang::get('page_constructor::messages.head_error'),
            'body'=>Lang::get('page_constructor::messages.body_error')]);
        $review = new ReviewPublic();
        $review->name = $request->name;
        $review->phone = $request->phone;
        $review->body = $request->body;
        $review->ip = $userIp;
        $review->save();
        return json_encode(['status' => 'success']);
    }

    private function isApprovedByIpActivity($userIp){
        $lastReview = ReviewPublic::where('ip',$userIp)->orderBy('created_at','desc')->first();
        if(!$lastReview) return true;
        $lastDate = new \DateTime($lastReview->created_at);
        $now = new \DateTime(date('Y-m-d H:i:s'));
        $interval = $lastDate->diff($now);
        $minutes = (int)$interval->i;
        if($minutes > 1) return true;
        return false;
    }
}
