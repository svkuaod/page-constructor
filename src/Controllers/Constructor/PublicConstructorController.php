<?php

namespace Svkuaod\PageConstructor\Controllers\Constructor;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\SEO\SeoableInterface;
use Svkuaod\PageConstructor\Components\Services\CheckImplementing;
use Svkuaod\PageConstructor\Controllers\Controller;
use Svkuaod\PageConstructor\Facade\Constructor as ConstructorFacade;
use Svkuaod\PageConstructor\Models\Constructor\Constructor as ConstructorModel;
use Svkuaod\PageConstructor\Models\Constructor\Custom\InfoPage;
use Svkuaod\PageConstructor\Models\Constructor\Custom\MainPage;

class PublicConstructorController extends Controller
{

    public function getMainPage()
    {
        $page = MainPage::getPageByKey(MainPage::getPageKey());
        return $this->renderAppView($page,false);
    }

    public function getPageByUrl($url)
    {
        $page = null;
        $urlArr = explode('/', $url);
        if (count($urlArr) == 1) {
            $page = InfoPage::getPageByUrl($urlArr[0]);
        }
        return $this->renderAppView($page);
    }

    protected function renderAppView(?ConstructorModel $page){
        if(!$page) return ConstructorFacade::getNotFoundPageResponse();
        $view = ConstructorFacade::getRenderedPage($page);
        $template = ConstructorConfig::getInstance()->getAppPath();
        $variables = array_merge(['view' => $view], ConstructorConfig::getInstance()->getPublicPaths());
        if(CheckImplementing::isImplementing(get_class($page),SeoableInterface::class)) {
            $variables = array_merge($variables,$page->getSeoData());
        }
        return view($template, $variables)->render();
    }

}
