<?php

namespace Svkuaod\PageConstructor\Controllers\Constructor;

use Illuminate\Http\Request;
use Svkuaod\PageConstructor\Controllers\Controller;
use Svkuaod\PageConstructor\Entities\Constructor\ConstructorEntity;
use Svkuaod\PageConstructor\Entities\Constructor\OptionEntity;
use Svkuaod\PageConstructor\Entities\Constructor\OptionTextEntity;
use Svkuaod\PageConstructor\Entities\Constructor\ResourceEntity;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;

class AdminConstructorController extends Controller
{
    private $resourceEntity;
    private $optionEntity;
    private $optionTextEntity;
    private $constructorEntity;

    public function __construct()
    {
        $this->resourceEntity = new ResourceEntity();
        $this->optionEntity = new OptionEntity();
        $this->optionTextEntity = new OptionTextEntity();
        $this->constructorEntity = new ConstructorEntity();
    }

    public function generateColumns(Request $request)
    {
        return response()->json($this->optionEntity->generateColumns($request));
    }

    public function getPreview($type, $id)
    {
        return $this->constructorEntity->getPreview($type, $id);
    }

    public function changeActiveResource(Request $request)
    {
        return $this->resourceEntity->switchActive($request->resource_id);
    }

    public function addOption(Request $request)
    {
        return response()->json($this->optionEntity->add($request->constructor_id, $request->template_id, $request->template_type, $request->parent_id));
    }

    public function addResourceToOption(Request $request)
    {
        return response()->json($this->resourceEntity->addResourceToOption($request->option_id, $request->key, $request->sub_view, $request->names, $request->items));
    }

    public function addResourceToResource(Request $request)
    {
        return response()->json($this->resourceEntity->addResourceToResource($request->resource_id, $request->key, $request->sub_view));
    }

    public function deleteOption(Request $request, ConstructorOption $option = null)
    {
        return $this->optionEntity->deleteRecursive($option, $request->option_id);
    }

    public function deleteResource(Request $request)
    {
        return $this->resourceEntity->deleteResource($request->resource_id);
    }

    public function fullWidthOption(Request $request)
    {
        return $this->optionEntity->fullWidthOption($request->option_id);
    }

    public function changeOptionStyle(Request $request){
        return json_encode($this->optionEntity->changeOptionStyle($request->option_id,$request->style));
    }

    public function switchActiveOption(Request $request)
    {
        return $this->optionEntity->switchActive($request->option_id);
    }

    public function changeOrderOption(Request $request)
    {
        return $this->optionEntity->changeOrderOption($request->constructor_id, $request->option_id, $request->parent_id, $request->flag);
    }

    public function changeOrderResource(Request $request)
    {
        return $this->resourceEntity->changeOrderResource($request->option_id, $request->resource_id, $request->key, $request->flag);
    }

    public function uploadFile(Request $request)
    {
        return response()->json($this->resourceEntity->uploadFile($request->constructor_id, $request->option_id,
            $request->resource_id, $request->prev_value, $request->key, $request->file));
    }

    public function changeResourceValue(Request $request)
    {
        $request->validate([
            'value' => 'max:1500',
            'resource_id' => 'int'
        ]);
        return response()->json($this->resourceEntity->changeResourceValue($request->option_id, $request->resource_id, $request->key, $request->value));
    }

    public function changeResourceOptionTextValue(Request $request)
    {
        $request->validate([
            'option_id' => 'int',
            'resource_id' => 'int',
        ]);
        return response()->json($this->resourceEntity->changeResourceOptionTextValue($request->option_id, $request->resource_id, $request->key, $request->value));
    }

    public function saveChangesAjax(Request $request)
    {
        return $this->optionTextEntity->saveChangesAjax($request->option_id, $request->type, $request->value);
    }

    public function changeBackgroundValue(Request $request)
    {
        return $this->optionEntity->changeBackgroundValue($request->option_id, $request->value);
    }

    public function copyInfoConstructorModelById(Request $request)
    {
        return json_encode($this->constructorEntity->copyInfo($request->constructor_id));
    }
}
