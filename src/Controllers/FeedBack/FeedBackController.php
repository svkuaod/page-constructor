<?php

namespace Svkuaod\PageConstructor\Controllers\FeedBack;

use Illuminate\Http\Request;
use Svkuaod\PageConstructor\Controllers\Controller;
use Svkuaod\PageConstructor\Models\FeedBack\FeedBackBase;

class FeedBackController extends Controller
{
   public function store(Request $request){
       $feedBackClass = $this->getFeedbackClassByKey($request->key);
       if(!$feedBackClass) return back()->withErrors();
       return (new $feedBackClass())->store($request);
   }

   private function getFeedbackClassByKey($key = null){
       foreach(get_declared_classes() as $class){
           if(get_parent_class($class) === FeedBackBase::class){
               if($key === $class::getTableKey()) return $class;
           }
       }
       return null;
   }

}
