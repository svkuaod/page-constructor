<?php

namespace Svkuaod\PageConstructor\Controllers\Subscribers;

use Illuminate\Http\Request;
use Svkuaod\PageConstructor\Controllers\Controller;
use Svkuaod\PageConstructor\Models\Subscribers\Subscriber;

class SubscriberController extends Controller
{
    public function addSubscriber(Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:180|unique:subscribers',
            'phone' => 'required|min:18|max:18',
            'name' => 'required|max:255'
        ]);
        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->phone = $request->phone;
        $subscriber->name = $request->name;
        $subscriber->ip = \Illuminate\Support\Facades\Request::ip();
        $subscriber->save();
        return json_encode(['status' => 'success']);
    }
}