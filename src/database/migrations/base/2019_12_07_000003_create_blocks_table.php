<?php


use Svkuaod\PageConstructor\Models\Blocks\Block;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Block::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('key',64)->nullable();
            $table->string('title',255)->nullable();
            $table->json('data')->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Block::TABLE);
    }

}
