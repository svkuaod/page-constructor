<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Menu\Menu;

class AddForeignKeysToMenuTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Menu::TABLE, function (Blueprint $table) {
            $table->foreign('parent_id', Menu::TABLE . '_' . Menu::TABLE . '_id_fk')->references('id')->on(Menu::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Menu::TABLE, function (Blueprint $table) {
            $table->dropForeign(Menu::TABLE . '_' . Menu::TABLE . '_id_fk');
        });
    }

}
