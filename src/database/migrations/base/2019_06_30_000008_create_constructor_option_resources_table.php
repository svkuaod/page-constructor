<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;

class CreateConstructorOptionResourcesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ConstructorOptionResourse::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index(ConstructorOptionResourse::TABLE . '_' . ConstructorOptionResourse::TABLE . '_id_fk');
            $table->integer('option_id')->unsigned()->nullable()->index(ConstructorOptionResourse::TABLE . '_constructor_options_id_fk');
            $table->string('key')->nullable();
            $table->integer('order')->unsigned()->nullable()->default(1);
            $table->json('data')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ConstructorOptionResourse::TABLE);
    }

}
