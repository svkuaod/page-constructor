<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode;

class CreateMarketingCodesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MarketingCode::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('key', 64)->nullable();
            $table->string('name',255)->nullable();
            $table->text('body')->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->boolean('active')->nullable()->default(0);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MarketingCode::TABLE);
    }

}
