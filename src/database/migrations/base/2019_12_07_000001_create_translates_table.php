<?php

use Svkuaod\PageConstructor\Models\Translates\Translate as Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTranslatesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Model::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 128)->unique();
            $table->integer('resource_id')->unsigned()->nullable();
            $table->string('resource_key', 128)->nullable();
            $table->string('default_value', 1500)->nullable();
            $table->string('description', 512)->nullable();
            $table->string('image', 255)->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Model::TABLE);
    }

}
