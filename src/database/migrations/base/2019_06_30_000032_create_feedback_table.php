<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\FeedBack\FeedBackBase;

class CreateFeedbackTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FeedBackBase::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 64)->nullable();
            $table->string('name', 128)->nullable();
            $table->string('phone', 128)->nullable();
            $table->string('email', 128)->nullable();
            $table->string('body', 2048)->nullable();
            $table->integer('target_id')->nullable();
            $table->integer('admin_id')->nullable();
            $table->boolean('checked')->default(0);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FeedBackBase::TABLE);
    }

}
