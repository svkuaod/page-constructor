<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\TemplateCustom;

class CreateTemplatesCustomTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TemplateCustom::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('view')->nullable();
            $table->string('name')->nullable();
            $table->string('photo')->nullable();
            $table->boolean('active')->nullable()->default(0);
            $table->integer('order')->nullable()->default(1);
            $table->tinyInteger('is_deletable')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TemplateCustom::TABLE);
    }

}
