<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;

class CreateConstructorOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ConstructorOption::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index(ConstructorOption::TABLE . '_' . ConstructorOption::TABLE .'_id_fk');
            $table->integer('constructor_id')->unsigned()->nullable()->index(ConstructorOption::TABLE . '_' . Constructor::TABLE . '_id_fk');
            $table->integer('template_id')->unsigned()->nullable();
            $table->string('template_type',128)->nullable();
            $table->string('value',128)->nullable();
            $table->boolean('full_width')->nullable()->default(0);
            $table->string('style', 1024)->nullable();
            $table->string('background_color', 255)->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->json('data')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ConstructorOption::TABLE);
    }

}
