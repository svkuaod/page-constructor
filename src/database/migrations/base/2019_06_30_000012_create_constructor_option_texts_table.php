<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;

class CreateConstructorOptionTextsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ConstructorOptionText::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('option_id')->unsigned()->nullable()->index(ConstructorOptionText::TABLE . '_' . ConstructorOption::TABLE .'_id_fk');
            $table->integer('resource_id')->unsigned()->nullable()->index(ConstructorOptionText::TABLE . '_' . ConstructorOptionResourse::TABLE . '_id_fk');
            $table->string('key')->nullable();
            $table->json('data')->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ConstructorOptionText::TABLE);
    }

}
