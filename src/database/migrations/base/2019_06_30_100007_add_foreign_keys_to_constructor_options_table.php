<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;

class AddForeignKeysToConstructorOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ConstructorOption::TABLE, function (Blueprint $table) {
            $table->foreign('constructor_id', ConstructorOption::TABLE . '_' . Constructor::TABLE . '_id_fk')->references('id')->on(Constructor::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('parent_id', ConstructorOption::TABLE . '_' . ConstructorOption::TABLE .'_id_fk')->references('id')->on(ConstructorOption::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(ConstructorOption::TABLE, function (Blueprint $table) {
            $table->dropForeign(ConstructorOption::TABLE . '_' . Constructor::TABLE . '_id_fk');
            $table->dropForeign(ConstructorOption::TABLE . '_' . ConstructorOption::TABLE .'_id_fk');
        });
    }

}
