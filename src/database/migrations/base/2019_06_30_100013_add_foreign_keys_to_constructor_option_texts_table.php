<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;

class AddForeignKeysToConstructorOptionTextsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ConstructorOptionText::TABLE, function (Blueprint $table) {
            $table->foreign('option_id', ConstructorOptionText::TABLE . '_' . ConstructorOption::TABLE .'_id_fk')->references('id')->on(ConstructorOption::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('resource_id', ConstructorOptionText::TABLE . '_' . ConstructorOptionResourse::TABLE . '_id_fk')->references('id')->on(ConstructorOptionResourse::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(ConstructorOptionText::TABLE, function (Blueprint $table) {
            $table->dropForeign(ConstructorOptionText::TABLE . '_' . ConstructorOption::TABLE .'_id_fk');
            $table->dropForeign(ConstructorOptionText::TABLE . '_' . ConstructorOptionResourse::TABLE . '_id_fk');
        });
    }

}
