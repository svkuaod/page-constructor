<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use \Svkuaod\PageConstructor\Models\Constructor\Constructor;

class CreateConstructorTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Constructor::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 100)->nullable();
            $table->string('url', 180)->unique();
            $table->boolean('active')->nullable()->default(0);
            $table->integer('order')->unsigned()->nullable();
            $table->json('data')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Constructor::TABLE);
    }

}
