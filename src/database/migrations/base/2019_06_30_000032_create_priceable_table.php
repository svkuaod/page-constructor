<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\Prices\Price;

class CreatePriceableTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Price::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_id')->unsigned()->nullable();
            $table->string('resource_type',128)->nullable();
            $table->float('value')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Price::TABLE);
    }

}
