<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Reviews\ReviewBase;

class CreateReviewsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReviewBase::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('body', 2048)->nullable();
            $table->string('ip', 255)->nullable();
            $table->string('photo', 255)->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->boolean('active')->nullable()->default(0);
            $table->boolean('archive')->nullable()->default(0);
            $table->boolean('main_page')->nullable()->default(0);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReviewBase::TABLE);
    }

}
