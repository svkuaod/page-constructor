<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\SEO\SEO;

class CreateSeoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SEO::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('url',180)->nullable();
            $table->string('key',64)->nullable();
            $table->integer('resource_id')->unsigned()->nullable();
            $table->string('resource_type',128)->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SEO::TABLE);
    }

}
