<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Subscribers\Subscriber;

class CreateSubscribersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Subscriber::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 50)->nullable();
            $table->string('email', 180)->unique('subscribers_email_uindex');
            $table->string('phone', 180)->nullable();
            $table->string('name', 180)->nullable();
            $table->string('ip', 255);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Subscriber::TABLE);
    }

}
