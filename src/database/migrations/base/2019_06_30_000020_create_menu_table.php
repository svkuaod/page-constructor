<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Menu\Menu;

class CreateMenuTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Menu::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index(Menu::TABLE . '_' . Menu::TABLE . '_id_fk');
            $table->string('key', 100);
            $table->string('url')->nullable();
            $table->string('icon',255)->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->boolean('active')->nullable()->default(0);
            $table->integer('constructor_id')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Menu::TABLE);
    }

}
