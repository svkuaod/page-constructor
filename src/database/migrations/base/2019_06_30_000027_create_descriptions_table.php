<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\Descriptions\Description;

class CreateDescriptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Description::TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo',255)->nullable();
            $table->string('video',255)->nullable();
            $table->timestamp('date')->nullable();
            $table->integer('resource_id')->unsigned()->nullable();
            $table->string('resource_type',128)->nullable();
            $table->json('data')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Description::TABLE);
    }

}
