<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;

class AddForeignKeysToConstructorOptionResourcesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ConstructorOptionResourse::TABLE, function (Blueprint $table) {
            $table->foreign('parent_id', ConstructorOptionResourse::TABLE . '_' . ConstructorOptionResourse::TABLE . '_id_fk')->references('id')->on(ConstructorOptionResourse::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('option_id', ConstructorOptionResourse::TABLE . '_constructor_options_id_fk')->references('id')->on('constructor_options')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(ConstructorOptionResourse::TABLE, function (Blueprint $table) {
            $table->dropForeign(ConstructorOptionResourse::TABLE . '_' . ConstructorOptionResourse::TABLE . '_id_fk');
            $table->dropForeign(ConstructorOptionResourse::TABLE . '_constructor_options_id_fk');
        });
    }

}
