<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorInfo;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;

class AddForeignKeysToConstructorInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(ConstructorInfo::getTablePrefix() . $language, function (Blueprint $table) use($language){
                $table->foreign('constructor_id', ConstructorInfo::getTablePrefix() . $language. '_constructor_id_fk')->references('id')->on(Constructor::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(ConstructorInfo::getTablePrefix() . $language, function (Blueprint $table) use($language) {
                $table->dropForeign(ConstructorInfo::getTablePrefix() . $language . '_constructor_id_fk');
            });
        }
    }

}
