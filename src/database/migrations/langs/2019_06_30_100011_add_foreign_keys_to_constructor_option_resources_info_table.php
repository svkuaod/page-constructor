<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourceInfo;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;

class AddForeignKeysToConstructorOptionResourcesInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(ConstructorOptionResourceInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->foreign('option_resources_id', ConstructorOptionResourceInfo::getTablePrefix()  . $language .  '_cons_opt_res__fk')->references('id')->on(ConstructorOptionResourse::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(ConstructorOptionResourceInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->dropForeign(ConstructorOptionResourceInfo::getTablePrefix()  . $language . '__cons_opt_res__fk');
            });
        }
    }

}
