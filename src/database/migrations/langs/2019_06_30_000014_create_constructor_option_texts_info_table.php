<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionTextInfo;

class CreateConstructorOptionTextsInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::create(ConstructorOptionTextInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->integer('id', true);
                $table->integer('option_texts_id')->unsigned()->index(ConstructorOptionTextInfo::getTablePrefix() . $language . '_' . ConstructorOptionText::TABLE . '_id__fk');
                $table->text('value')->nullable();
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::drop(ConstructorOptionTextInfo::getTablePrefix() . $language);
        }
    }

}
