<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionTextInfo;

class AddForeignKeysToConstructorOptionTextsInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(ConstructorOptionTextInfo::getTablePrefix() . $language, function (Blueprint $table) use($language){
                $table->foreign('option_texts_id', ConstructorOptionTextInfo::getTablePrefix() . $language . '_' . ConstructorOptionText::TABLE . '_id__fk')->references('id')->on(ConstructorOptionText::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(ConstructorOptionTextInfo::getTablePrefix() . $language, function (Blueprint $table) use($language) {
                $table->dropForeign(ConstructorOptionTextInfo::getTablePrefix() . $language . '_' . ConstructorOptionText::TABLE . '_id__fk');
            });
        }
    }

}
