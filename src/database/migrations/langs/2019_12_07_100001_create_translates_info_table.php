<?php

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Translates\TranslateInfo as InfoModel;
use Svkuaod\PageConstructor\Models\Translates\Translate as Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTranslatesInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::create(InfoModel::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->increments('id');
                $table->integer(InfoModel::getForeignField())->unsigned()->index(InfoModel::getTablePrefix() . $language . '_' . InfoModel::getForeignField() . '_fk');
                $table->string('value', 1500)->nullable();
                $table->timestamps();
            });
            Schema::table(InfoModel::getTablePrefix() . $language, function (Blueprint $table) use($language){
                $table->foreign(InfoModel::getForeignField(), InfoModel::getTablePrefix() . $language . '_' . InfoModel::getForeignField() . '_fk')->references('id')->on(Model::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language){
            Schema::table(InfoModel::getTablePrefix() . $language, function (Blueprint $table) use($language) {
                $table->dropForeign(InfoModel::getTablePrefix() . $language . '_' . InfoModel::getForeignField() . '_fk');
            });
            Schema::drop(InfoModel::getTablePrefix() . $language);
        }
    }

}
