<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Menu\MenuInfo;
use Svkuaod\PageConstructor\Models\Menu\Menu;

class CreateMenuInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::create(MenuInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->integer('id', true);
                $table->integer('menu_id')->unsigned()->nullable()->index(MenuInfo::getTablePrefix() . $language . '_' . Menu::TABLE . '_id_fk');
                $table->string('name', 180)->nullable();
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language){
            Schema::drop(MenuInfo::getTablePrefix() . $language);
        }
    }

}
