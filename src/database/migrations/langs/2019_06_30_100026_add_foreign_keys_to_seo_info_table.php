<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\SEO\SEO;
use Svkuaod\PageConstructor\Components\SEO\SEOInfo;

class AddForeignKeysToSeoInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(SEOInfo::getTablePrefix() . $language, function (Blueprint $table) use($language){
                $table->foreign(SEO::TABLE . '_id', SEOInfo::getTablePrefix() . $language . '_' . SEO::TABLE . '_id_fk')->references('id')->on(SEO::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(SEOInfo::getTablePrefix() . $language, function (Blueprint $table) use($language) {
                $table->dropForeign(SEOInfo::getTablePrefix() . $language . '_' . SEO::TABLE . '_id_fk');
            });
        }
    }

}
