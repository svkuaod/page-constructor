<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\Descriptions\Description;
use Svkuaod\PageConstructor\Components\Descriptions\DescriptionInfo;

class AddForeignKeysToDescriptionsInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(DescriptionInfo::getTablePrefix() . $language, function (Blueprint $table) use($language){
                $table->foreign(Description::TABLE . '_id', DescriptionInfo::getTablePrefix() . $language . '_' . Description::TABLE . '_id_fk')->references('id')->on(Description::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(DescriptionInfo::getTablePrefix() . $language, function (Blueprint $table) use($language) {
                $table->dropForeign(DescriptionInfo::getTablePrefix() . $language . '_' . Description::TABLE . '_id_fk');
            });
        }
    }

}
