<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourceInfo;

class CreateConstructorOptionResourcesInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::create(ConstructorOptionResourceInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->increments('id');
                $table->integer('option_resources_id')->unsigned()->index(ConstructorOptionResourceInfo::getTablePrefix() . $language .  '__cons_opt_res__fk');
                $table->string('value', 1500)->nullable();
                $table->timestamps();
            });
        }

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::drop(ConstructorOptionResourceInfo::getTablePrefix() . $language);
        }
    }

}
