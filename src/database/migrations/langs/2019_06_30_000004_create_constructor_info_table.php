<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorInfo;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;

class CreateConstructorInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language){
            Schema::create(ConstructorInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->increments('id');
                $table->integer(Constructor::TABLE . '_id')->unsigned()->nullable()->index(ConstructorInfo::getTablePrefix() . $language . '_'. Constructor::TABLE . '_id_fk');
                $table->string('name', 255)->nullable();
                $table->string('url', 180)->nullable();
                $table->timestamps();
            });
        }

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language){
            Schema::drop(ConstructorInfo::getTablePrefix() . $language);
        }

    }

}
