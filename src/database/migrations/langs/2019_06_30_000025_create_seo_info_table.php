<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\SEO\SEOInfo;
use Svkuaod\PageConstructor\Components\SEO\SEO;

class CreateSeoInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::create(SEOInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->integer('id', true);
                $table->integer(SEO::TABLE . '_id')->unsigned()->nullable();
                $table->longText('meta_text')->nullable();
                $table->string('meta_title', 512)->nullable();
                $table->string('meta_keywords', 512)->nullable();
                $table->string('meta_description', 1024)->nullable();
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language){
            Schema::drop(SEOInfo::getTablePrefix() . $language);
        }
    }

}
