<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Menu\MenuInfo;
use Svkuaod\PageConstructor\Models\Menu\Menu;

class AddForeignKeysToMenuInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(MenuInfo::getTablePrefix() . $language, function (Blueprint $table) use($language){
                $table->foreign('menu_id', MenuInfo::getTablePrefix() . $language . '_' . Menu::TABLE . '_id_fk')->references('id')->on(Menu::TABLE)->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::table(MenuInfo::getTablePrefix() . $language, function (Blueprint $table) use($language) {
                $table->dropForeign(MenuInfo::getTablePrefix() . $language . '_' . Menu::TABLE . '_id_fk');
            });
        }
    }

}
