<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\Descriptions\DescriptionInfo;
use Svkuaod\PageConstructor\Components\Descriptions\Description;

class CreateDescriptionsInfoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {
            Schema::create(DescriptionInfo::getTablePrefix() . $language, function (Blueprint $table) use ($language) {
                $table->integer('id', true);
                $table->integer(Description::TABLE . '_id')->unsigned()->nullable();
                $table->string('name', 255)->nullable();
                $table->string('body_m', 1500)->nullable();
                $table->text('body')->nullable();
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (ConstructorConfig::getInstance()->getLanguages() as $language){
            Schema::drop(DescriptionInfo::getTablePrefix() . $language);
        }
    }

}
