<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;
use Svkuaod\PageConstructor\Models\Menu\Menu;
use Svkuaod\PageConstructor\Models\Menu\HeaderMenu;

class MenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table(Menu::TABLE)->delete();

        \DB::table(Menu::TABLE)->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'parent_id' => NULL,
                    'key' => HeaderMenu::getMenuKey(),
                    'url' => '',
                    'order' => 1,
                    'active' => 1,
                    'constructor_id' => NULL
                ),
            1 =>
                array(
                    'id' => 2,
                    'parent_id' => NULL,
                    'key' => HeaderMenu::getMenuKey(),
                    'url' => 'about',
                    'order' => 2,
                    'active' => 1,
                    'constructor_id' => NULL
                )

        ));


    }
}