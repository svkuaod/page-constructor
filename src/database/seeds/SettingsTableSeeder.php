<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;
use Svkuaod\PageConstructor\Models\Settings\Settings;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table(Settings::TABLE)->delete();

        \DB::table(Settings::TABLE)->insert(array(
            0 =>
                array(
                    'key' => 'email_admin',
                    'value' => 'test@test.test'
                ),
        ));


    }
}