<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;
use Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode;

class MarketingCodesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table(MarketingCode::TABLE)->delete();

        \DB::table(MarketingCode::TABLE)->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'parent_id' => NULL,
                    'key' => MarketingCode::HEAD_KEY,
                    'name' => 'Head',
                    'body' => null,
                    'order' => 1,
                    'active' => 1,
                    'created_at' => '2018-12-01 15:20:40',
                    'updated_at' => '2019-01-13 19:21:42',
                ),
            1 =>
                array(
                    'id' => 2,
                    'parent_id' => NULL,
                    'key' => MarketingCode::BODY_TOP_KEY,
                    'name' => 'Body Top',
                    'body' => null,
                    'order' => 2,
                    'active' => 1,
                    'created_at' => '2018-12-06 19:32:14',
                    'updated_at' => '2019-01-13 19:29:28',
                ),
            2 =>
                array(
                    'id' => 3,
                    'parent_id' => NULL,
                    'key' => MarketingCode::BODY_BOTTOM_KEY,
                    'name' => 'Body Bottom',
                    'body' => null,
                    'order' => 3,
                    'active' => 1,
                    'created_at' => '2019-01-13 17:36:38',
                    'updated_at' => '2019-01-13 20:04:12',
                ),
        ));


    }
}