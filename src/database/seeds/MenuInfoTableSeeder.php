<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Menu\MenuInfo;

class MenuInfoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        foreach (ConstructorConfig::getInstance()->getLanguages() as $language) {

            \DB::table(MenuInfo::getTablePrefix() . $language)->delete();

            \DB::table(MenuInfo::getTablePrefix() . $language)->insert(array(
                0 =>
                    array(
                        'id' => 1,
                        'menu_id' => 1,
                        'name' => 'Главная',
                    ),
                1 =>
                    array(
                        'id' => 2,
                        'menu_id' => 2,
                        'name' => 'О нас',
                    ),
            ));
        }



    }
}