<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array(
            0 =>
                array(
                    'name' => 'admin',
                    'email' => 'test@test.test',
                    'phone' => '80503333333',
                    'password' => bcrypt('test@test.test'),
                    'is_admin' => 1
                ),
        ));


    }
}