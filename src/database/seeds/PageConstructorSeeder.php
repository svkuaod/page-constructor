<?php
namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;

class PageConstructorSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MenuTableSeeder::class);
        $this->call(MenuInfoTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TemplateBaseTableSeeder::class);
//        $this->call(TemplateCustomTableSeeder::class);
        $this->call(MarketingCodesTableSeeder::class);
    }
}
