<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;
use Svkuaod\PageConstructor\Models\Constructor\TemplateCustom;

class TemplateCustomTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        \DB::table(TemplateCustom::TABLE)->delete();

        \DB::table(TemplateCustom::TABLE)->insert(array(
            0 =>
                array(
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Секции',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 3,
                    'is_deletable' => 0,
                ),
        ));

        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');


    }
}