<?php

namespace Svkuaod\PageConstructor\database\seeds;

use Illuminate\Database\Seeder;
use Svkuaod\PageConstructor\Models\Constructor\TemplateBase;

class TemplateBaseTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        \DB::table(TemplateBase::TABLE)->delete();
        \DB::table(TemplateBase::TABLE)->insert(array(
            1 =>
                array(
                    'id' => 1,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Фотографии',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            2 =>
                array(
                    'id' => 2,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Видео',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 2,
                    'is_deletable' => 0
                ),
            3 =>
                array(
                    'id' => 3,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Фотогалерея',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 3,
                    'is_deletable' => 0
                ),
            4 =>
                array(
                    'id' => 4,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Текстовые блоки',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 4,
                    'is_deletable' => 0
                ),
            5 =>
                array(
                    'id' => 5,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Прочие',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 8,
                    'is_deletable' => 0
                ),
            6 =>
                array(
                    'id' => 6,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Колонки',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 6,
                    'is_deletable' => 0
                ),
            7 =>
                array(
                    'id' => 7,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Паралакс',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 5,
                    'is_deletable' => 0
                ),
            8 =>
                array(
                    'id' => 8,
                    'parent_id' => NULL,
                    'view' => NULL,
                    'name' => 'Разделитель',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 7,
                    'is_deletable' => 0
                ),
            9 =>
                array(
                    'id' => 9,
                    'parent_id' => 1,
                    'view' => 'templates.photo.photo_one',
                    'name' => 'Фото',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            10 =>
                array(
                    'id' => 10,
                    'parent_id' => 1,
                    'view' => 'templates.photo.photo_one_name',
                    'name' => 'Фото с заголовком',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            11 =>
                array(
                    'id' => 11,
                    'parent_id' => 2,
                    'view' => 'templates.video.video_type',
                    'name' => 'Видео',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            12 =>
                array(
                    'id' => 12,
                    'parent_id' => 2,
                    'view' => 'templates.video.video_with_title_type',
                    'name' => 'Видео с заголовком',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            13 =>
                array(
                    'id' => 13,
                    'parent_id' => 2,
                    'view' => 'templates.video.video_youtube_type',
                    'name' => 'Видео youtube',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            14 =>
                array(
                    'id' => 14,
                    'parent_id' => 3,
                    'view' => 'templates.slider.slider_type_one',
                    'name' => 'Слайдер ',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            15 =>
                array(
                    'id' => 15,
                    'parent_id' => 3,
                    'view' => 'templates.slider.slider_type_preview',
                    'name' => 'Слайдер с превью',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            16 =>
                array(
                    'id' => 16,
                    'parent_id' => 3,
                    'view' => 'templates.slider.slider_type_preview_other',
                    'name' => 'Слайдер с превью другой фото',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            17 =>
                array(
                    'id' => 17,
                    'parent_id' => 4,
                    'view' => 'templates.text.text_editor_type_one',
                    'name' => 'Текстовый редактор',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            18 =>
                array(
                    'id' => 18,
                    'parent_id' => 5,
                    'view' => 'templates.other.infopage_type',
                    'name' => 'Секция',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            19 =>
                array(
                    'id' => 19,
                    'parent_id' => 5,
                    'view' => 'templates.other.h1_tytle_type',
                    'name' => 'Заголовок H1',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            20 =>
                array(
                    'id' => 20,
                    'parent_id' => 5,
                    'view' => 'templates.other.site_map',
                    'name' => 'SiteMap',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            21 =>
                array(
                    'id' => 21,
                    'parent_id' => 6,
                    'view' => 'templates.columns.columns_wrapper_type',
                    'name' => 'Колонки',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
            22 =>
                array(
                    'id' => 22,
                    'parent_id' => 6,
                    'view' => 'templates.columns.column_wrapper_type',
                    'name' => 'Пустая колонка',
                    'photo' => NULL,
                    'active' => 1,
                    'order' => 1,
                    'is_deletable' => 0
                ),
        ));
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}