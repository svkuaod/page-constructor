<?php
namespace Svkuaod\PageConstructor\Services\Mail;

use Illuminate\Support\Facades\Mail;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\FeedBack\FeedBackBase;
use Svkuaod\PageConstructor\Models\Feedback\InterfaceBaseFeedback;

class FeedBack extends AbstractEmail
{
    private $model;
    private $url;

    public function __construct(FeedBackBase $model)
    {
        $this->model = $model;
        $this->url = env('APP_URL') . '/admin/' . $this->model->getAdminSectionPath() .'/' . $this->model->id . '/edit';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))
            ->markdown(ConstructorConfig::getInstance()->getServiceMailPath() . '.feedback')->subject('Новая заявка')
            ->with(['model' => $this->model, 'url' => $this->url]);
    }

    public function sendMail($email)
    {
        Mail::to($email)->send($this);
    }
}