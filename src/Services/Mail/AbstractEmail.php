<?php

namespace Svkuaod\PageConstructor\Services\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

abstract class AbstractEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Build the message.
     *
     * @return $this
     */
    abstract public function build();

    abstract public function sendMail($email);
}
