<?php

namespace Svkuaod\PageConstructor\Entities\Constructor;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorInfo;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourceInfo;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionTextInfo;

class ConstructorEntity
{

    public function getPreview($type, $id)
    {
        switch ($type) {
            case 'option':
                $view = \Svkuaod\PageConstructor\Facade\Constructor::getRenderedOptionById($id);
                break;
            case 'page':
                $view = \Svkuaod\PageConstructor\Facade\Constructor::getRenderedPageById($id,null);
                break;
            default:
                $view = '';
        }
        $variables = array_merge(['view' => $view], ConstructorConfig::getInstance()->getPublicPaths());
        return view(ConstructorConfig::getInstance()->getPublicFolderPath() . '.preview', $variables)->render();
    }

    public function copyInfo($constructorId){
        $constructor = Constructor::getPageById($constructorId);
        $this->copyInfoByConstructor($constructor);
        if(!$constructor || !isset($constructor->options) || !count($constructor->options)) return false;
        foreach ($constructor->options as $option){
            $this->copyInfoByOption($option);
        }
        return ['status'=>'success'];
    }

    private function copyInfoByConstructor(Constructor $constructor){
        if(!$constructor->info) return false;
        $languages = ConstructorConfig::getInstance()->getLanguages();
        foreach ($languages as $lang) {
            if ($lang == session('locale')) continue;
            $replicateInfo = new ConstructorInfo();
            $replicateInfo->setLocaleToTable($lang);
            $replicateInfo = $replicateInfo->where('constructor_id',$constructor->id)->first();
            if(!$replicateInfo) {
                $replicateInfo = new ConstructorInfo();
                $replicateInfo->setLocaleToTable($lang);
            }
            $replicateInfo->constructor_id = $constructor->id;
            $replicateInfo->name = $constructor->info->name;
            $replicateInfo->save();
        }
    }

    private function copyInfoByOption(ConstructorOption $option){
        if(!$option) return false;
        if(isset($option->childs) && count($option->childs)){
            foreach ($option->childs as $subOption) $this->copyInfoByOption($subOption);
        }
        if(isset($option->resources) && count($option->resources)) {
            foreach ($option->resources as $resource) {
                $this->copyInfoByResource($resource);
            }
        }
        if(isset($option->text) && $option->text) {
            $this->copyInfoByText($option->text);
        }
        return true;
    }

    private function copyInfoByResource(ConstructorOptionResourse $resourse){
        if(!$resourse) return false;
        if(isset($resourse->childs) && count($resourse->childs)){
            foreach ($resourse->childs as $subResource) $this->copyInfoByResource($subResource);
        }
        if(!$resourse->info) return false;
        $languages = ConstructorConfig::getInstance()->getLanguages();
        foreach ($languages as $lang){
            if ($lang == session('locale')) continue;

            $replicateInfo = new ConstructorOptionResourceInfo();
            $replicateInfo->setLocaleToTable($lang);
            $replicateInfo = $replicateInfo->where('option_resources_id',$resourse->id)->first();
            if(!$replicateInfo) {
                $replicateInfo = new ConstructorOptionResourceInfo();
                $replicateInfo->setLocaleToTable($lang);
            }
            $replicateInfo->option_resources_id = $resourse->id;
            $replicateInfo->value = $resourse->info->value;
            $replicateInfo->save();
        }
        return true;
    }

    private function copyInfoByText(ConstructorOptionText $text)
    {
        if (!$text) return false;
        if (!$text->info) return false;
        $languages = ConstructorConfig::getInstance()->getLanguages();
        foreach ($languages as $lang) {
            if ($lang == session('locale')) continue;

            $replicateInfo = new ConstructorOptionTextInfo();
            $replicateInfo->setLocaleToTable($lang);
            $replicateInfo = $replicateInfo->where('option_texts_id', $text->id)->first();
            if (!$replicateInfo) {
                $replicateInfo = new ConstructorOptionTextInfo();
                $replicateInfo->setLocaleToTable($lang);
            }
            $replicateInfo->option_texts_id = $text->id;
            $replicateInfo->value = $text->info->value;
            $replicateInfo->save();
        }
        return true;
    }
}
