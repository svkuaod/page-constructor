<?php

namespace Svkuaod\PageConstructor\Entities\Constructor;

use Illuminate\Http\Request;
use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\Template;
use Svkuaod\PageConstructor\Models\Constructor\TemplateBase;
use Svkuaod\PageConstructor\Models\Constructor\TemplateCustom;

class OptionEntity
{
    public function add($constructorId, $templateId, $templateType, $parentId = null){
        $templateClass = Template::getTemplateClassByType($templateType);
        if(!$templateClass) return ['error'];
        $view = $templateClass::where('id',$templateId)->pluck('view')->first();
        if(!$view) return ['error'];
        $templates = collect([
            TemplateBase::getMorphKey() => TemplateBase::get(),
            TemplateCustom::getMorphKey() => TemplateCustom::get()
        ]);
        $viewPath = ConstructorConfig::getInstance()->getAdminConstructorPath() . '.' . $view;
        $option = new ConstructorOption([
            'template_id' => $templateId,
            'template_type' => $templateType,
            'order' => $this->getOrder($constructorId,$parentId)]);
        if ($parentId) $option->parent_id = $parentId;
        else $option->constructor_id = $constructorId;
        $option->save();
        $variables = ['option' => $option, 'templates' => $templates, 'parentId' => $parentId];
        $variables = array_merge($variables, ConstructorConfig::getInstance()->getResourceKeys(),
            ConstructorConfig::getInstance()->getAdminPaths(),
            ConstructorConfig::getInstance()->getResourceVariables($option));
        $resultView = view($viewPath, $variables)->render();
        return ['view' => $resultView, 'parent_id' => $parentId];
    }

    public function deleteRecursive(?ConstructorOption $option,$optionId = null)
    {
        if(!$option) $option = ConstructorOption::where('id', $optionId)->with(['childs', 'resources'])->first();
        if ($option->childs && count($option->childs)) {
            foreach ($option->childs as $child) {
                $this->deleteRecursive($child);
            }
        }
        if ($option->resources && count($option->resources)) {
            $resourceEntity = new ResourceEntity();
            foreach ($option->resources as $resource) {
                $resourceEntity->deleteResourceFile($resource);
            }
        }
        $option->delete();
    }

    public function changeBackgroundValue($optionId, $value)
    {
        $option = ConstructorOption::where('id', $optionId)->first();
        if ($option) {
            $option->background_color = $value;
            return $option->save();
        }
        return false;
    }

    public function fullWidthOption($optionId)
    {
        $option = ConstructorOption::where('id', $optionId)->first();
        if(!$option) return false;
        if ($option->full_width) $option->full_width = 0;
        else $option->full_width = 1;
        return $option->save();
    }

    public function changeOptionStyle($optionId,$style){
        $option = ConstructorOption::where('id', $optionId)->first();
        if(!$option) return false;
        $option->style = $style;
        return $option->save();
    }

    public function switchActive($optionId)
    {
        $option = ConstructorOption::where('id', $optionId)->first();
        if(!$option) return false;
        switch ($option->active) {
            case 1 :
                $option->active = 0;
                break;
            default:
                $option->active = 1;
        }
        return $option->save();
    }

    public function changeOrderOption($constructorId, $optionId, $parentId, $flag)
    {
        if ($constructorId) {
            $items = ConstructorOption::where('constructor_id', $constructorId)->get();
            $currentItem = $items->where('id', $optionId)->first();
        } else if ($parentId) {
            $items = ConstructorOption::where('parent_id', $parentId)->get();
            $currentItem = $items->where('id', $optionId)->first();
        }
        $this->changeOrder($currentItem, $items, $flag);
    }

    public function changeOrder($currentItem, $items, $flag)
    {
        $currentOrder = $currentItem->order;
        if ($currentItem) {
            if ($flag == 'up') {
                $target = $items->where('order', '<', $currentOrder)->sortByDesc('order')->first();
                if ($target) {
                    $currentItem->order = $target->order;
                    $currentItem->save();
                    $target->order = $currentOrder;
                    $target->save();
                }
            } else {
                $target = $items->where('order', '>', $currentOrder)->sortBy('order')->first();
                if (!$target) $target = $items->where('id', '!=', $currentItem->id)->first();
                if ($target) {
                    $currentItem->order = $target->order;
                    $currentItem->save();
                    $target->order = $currentOrder;
                    $target->save();
                }
            }
        }
    }

    public function generateColumns(Request $request){
        $request->validate([
            'option_id' => 'required|int',
            'items' => 'required|array'
        ]);
        $option = ConstructorOption::where('id',$request->option_id)->first();
        if(!$option) return ['error'];
        $pathColumnWrapper = ConstructorConfig::getInstance()->getAdminPaths()['pathColumnWrapper'];
        $pathColumnWrapper = str_replace(ConstructorConfig::getInstance()->getAdminConstructorPath() . '.','',$pathColumnWrapper);
        $templateId = TemplateBase::where('view',$pathColumnWrapper)->pluck('id')->first();
        if (!$templateId) return ['error'];
        foreach ($request->items as $key=>$columns){
            $column = new ConstructorOption();
            $column->value = $columns;
            $column->parent_id = $option->id;
            $column->template_id = $templateId;
            $column->template_type = 'base';
            $column->order = $key;
            $column->save();
        }
        return ['success'=>true];
    }

    private function getOrder($constructorId,$parentId){
        if($parentId) $builder = ConstructorOption::where('parent_id', $parentId);
        else $builder = ConstructorOption::where('constructor_id', $constructorId);
        $lastOrder = $builder->orderBy('order', 'desc')->pluck('order')->first();
        return $lastOrder  ? $lastOrder + 1 : 1;
    }
}
