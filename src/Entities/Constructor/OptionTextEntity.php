<?php

namespace Svkuaod\PageConstructor\Entities\Constructor;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionTextInfo;

class OptionTextEntity
{
    public function saveChangesAjax($optionId, $type, $value)
    {
        $entity = ConstructorOptionText::where('option_id', $optionId)->with('info')->first();
        if ($entity) {
            $entityInfo = ConstructorOptionTextInfo::where('option_texts_id', $entity->id)->first();
            if ($entityInfo) $entityInfo->value = $value;
            else {
                $entityInfo = new ConstructorOptionTextInfo();
                $entityInfo->option_texts_id = $entity->id;
                $entityInfo->value = $value;
            }
            $entityInfo->save();
        } else {
            $entity = new ConstructorOptionText(['option_id' => $optionId, 'key' => $type]);
            $entity->save();
            $this->setDefaultConstructorOptionTextInfo($entity,$value);
        }
    }

    private function setDefaultConstructorOptionTextInfo(ConstructorOptionText $resource,$value){
        $languages = ConstructorConfig::getInstance()->getLanguages();
        foreach ($languages as $lang){
            $infoModel = new ConstructorOptionTextInfo();
            $infoModel->setLocaleToTable($lang);
            $infoModel = $infoModel->where('option_texts_id', $resource->id)->first();
            if(!$infoModel) {
                $infoModel = new ConstructorOptionTextInfo($lang);
                $infoModel->setLocaleToTable($lang);
                $infoModel->option_texts_id = $resource->id;
                $infoModel->value = $value;
                $infoModel->save();
            }
            $infoModel = null;
        }
    }
}
