<?php

namespace Svkuaod\PageConstructor\Entities\Constructor;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\Services\StoreFile;
use Svkuaod\PageConstructor\Components\Services\UrlGenerator;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourceInfo;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionTextInfo;
use Illuminate\Http\Request;

class ResourceEntity
{
    public function switchActive($resourceId = null){
        $resource = ConstructorOptionResourse::find($resourceId);
        if(!$resource) return false;
        switch ($resource->active) {
            case 1 :
                $resource->active = 0;
                break;
            default:
                $resource->active = 1;
        }
        return $resource->save();
    }

    public function addResourceToOption($optionId, $key, $subView, $names ,$items = null){
        $resource = new ConstructorOptionResourse();
        $resource->key = $key;
        $resource->option_id = $optionId;
        $resource->order = $this->getOrder($optionId,$key);
        $resource->active = 1;
        $resource->save();
        $option = $resource->option;
        $variables = ['option' => $option, 'resource' => $resource, 'key' => $key, 'names' => json_decode($names,true), 'items' => json_decode($items,true)];
        $variables = array_merge($variables, ConstructorConfig::getInstance()->getResourceKeys(), ConstructorConfig::getInstance()->getAdminPaths());
        $view = view($subView, $variables)->render();
        return ['view' => $view];
    }

    public function addResourceToResource($resourceId, $key, $subView)
    {
        $resource = new ConstructorOptionResourse();
        $resource->key = $key;
        $resource->parent_id = $resourceId;
        $resource->order = $this->getOrderParent($resourceId,$key);
        $resource->save();
        $variables = ['resource' => $resource, 'key' => $key];
        $variables = array_merge($variables, ConstructorConfig::getInstance()->getResourceKeys(), ConstructorConfig::getInstance()->getAdminPaths());
        $view = view($subView, $variables)->render();
        return ['view' => $view];
    }

    public function deleteResource($resourceId)
    {
        $resource = ConstructorOptionResourse::where('id', $resourceId)->first();
        $this->deleteResourceFile($resource);
        $resource->delete();
    }

    public function deleteResourceFile(ConstructorOptionResourse $resource)
    {
        if ($resource->childs && count($resource->childs)) {
            foreach ($resource->childs as $item) {
                $this->deleteResourceFile($item);
            }
        }
        if (isset($resource->info->value)) {
            StoreFile::deleteAllFilesByPath($resource->info->value);
        }

    }

    public function changeOrderResource($optionId,$resourceId,$key,$flag)
    {
        $items = null;
        if ($optionId) $items = ConstructorOptionResourse::where('option_id', $optionId)
            ->where('key', $key)->get();
        else {
            $currentResource = ConstructorOptionResourse::find($resourceId);
            if ($currentResource && $currentResource->parent_id) {
                $items = ConstructorOptionResourse::where('parent_id', $currentResource->parent_id)->get();
            }
        }
        if (!$items) return false;
        $currentItem = $items->where('id', $resourceId)->first();
        (new OptionEntity())->changeOrder($currentItem, $items, $flag);
    }

    public function uploadFile($constructorId, $optionId, $resourceId, $prevValue, $key,  $file)
    {
        $path = '/constructor/' . $constructorId . '/' . $optionId . '/';
        if($file && $file->getClientOriginalExtension() === 'svg') $newFile = StoreFile::getPathStoredSvgFile($path, $file);
        else {
            $fileName = UrlGenerator::slug($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
            $newFile = StoreFile::getPathStoredFileAs($path, $fileName, $file);
        }
        if ($prevValue && $prevValue != $newFile) StoreFile::deleteAllFilesByPath($prevValue);
        if (isset($resourceId)) {
            $entity = ConstructorOptionResourse::where('id', $resourceId)->first();
        } else {
            $entity = new ConstructorOptionResourse();
            $entity->option_id = $optionId;
            $entity->key = $key;
        }
        $entity->save();
        $entityInfo = ConstructorOptionResourceInfo::where('option_resources_id', $entity->id)->first();
        if (!$entityInfo) {
            $entityInfo = new ConstructorOptionResourceInfo();
            $entityInfo->option_resources_id = $entity->id;
        }
        $entityInfo->value = $newFile;
        $entityInfo->save();
        return ['file_path' => $entityInfo->value];
    }

    public function changeResourceValue($optionId, $resourceId, $key, $value)
    {
        $resource = ConstructorOptionResourse::where('id', $resourceId)->first();
        if (!$resource) {
            $resource = new ConstructorOptionResourse(['option_id' => $optionId, 'key' => $key ?? '']);
            $resource->save();
            $this->setDefaultConstructorOptionResourceInfo($resource,$value);
            return ['resource_id' => $resource->id];
        }
        $resourceInfo = ConstructorOptionResourceInfo::where('option_resources_id', $resource->id)->first();
        if ($resourceInfo){
            $resourceInfo->option_resources_id = $resource->id;
            $resourceInfo->value = $value;
            $resourceInfo->save();
        }
        else{
            $this->setDefaultConstructorOptionResourceInfo($resource,$value);
        }
        return ['resource_id' => $resource->id];
    }

    public function changeResourceOptionTextValue($optionId, $resourceId, $key, $value){
        $resource = ConstructorOptionResourse::where('id', $resourceId)->first();
        if (!$resource) {
            $resource = new ConstructorOptionResourse(['option_id' => $optionId, 'key' => $key ?? '']);
            $resource->save();
        }
        $optionText = ConstructorOptionText::where('resource_id', $resource->id)->first();
        if (!$optionText) {
            $optionText = new ConstructorOptionText();
            $optionText->option_id = $optionId;
            $optionText->resource_id = $resource->id;
            $optionText->key = $key;
            $optionText->save();
        }
        $optionTextInfo = ConstructorOptionTextInfo::where('option_texts_id', $optionText->id)->first();
        if (!$optionTextInfo) $optionTextInfo = new ConstructorOptionTextInfo();
        $optionTextInfo->option_texts_id = $optionText->id;
        $optionTextInfo->value = $value;
        $optionTextInfo->save();
        return ['resource_id'=>$resource->id];
    }

    private function setDefaultConstructorOptionResourceInfo(ConstructorOptionResourse $resource,$value){
        $languages = ConstructorConfig::getInstance()->getLanguages();
        foreach ($languages as $lang){
            $infoModel = new ConstructorOptionResourceInfo();
            $infoModel->setLocaleToTable($lang);
            $infoModel = $infoModel->where('option_resources_id', $resource->id)->first();
            if(!$infoModel) {
                $infoModel = new ConstructorOptionResourceInfo($lang);
                $infoModel->setLocaleToTable($lang);
                $infoModel->option_resources_id = $resource->id;
                $infoModel->value = $value;
                $infoModel->save();
            }
            $infoModel = null;
        }
    }

    private function getOrder($optionId,$key){
        $lastOrder = ConstructorOptionResourse::where('option_id', $optionId)->where('key', $key)->orderBy('order', 'desc')->pluck('order')->first();
        return $lastOrder  ? $lastOrder + 1 : 1;
    }

    private function getOrderParent($parentId,$key){
        $lastOrder = ConstructorOptionResourse::where('parent_id', $parentId)->where('key', $key)->orderBy('order', 'desc')->pluck('order')->first();
        return $lastOrder  ? $lastOrder + 1 : 1;
    }
}
