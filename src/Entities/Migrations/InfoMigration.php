<?php

namespace Svkuaod\PageConstructor\Entities\Migrations;

use Svkuaod\PageConstructor\Components\ConstructorConfig;

abstract class InfoMigration
{
    private static $infoSuffixes = [];

    public static function getInfoSuffixes(){
        if(!self::$infoSuffixes || !count(self::$infoSuffixes)) self::setInfoSuffixes();
        return self::$infoSuffixes;
    }

    /**
     * @return array;
     */
    private static function setInfoSuffixes(){
        $languages = ConstructorConfig::getInstance()->getLanguages();
        if(!$languages || !count($languages)) $languages = ['ru'];
        foreach ($languages as $item) self::$infoSuffixes[] = '_info_' . $item;
    }
}