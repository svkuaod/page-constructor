$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).on('submit', 'form[data-send-ajax="true"]', function (e) {
    e.preventDefault();

    var form = $(this);
    var data = form.serialize();
    var action = form.attr('action');
    if(!action) return false

    var head = '';
    var body = '';

    form.removeAttr('action');

    $.ajax({
        dataType: 'json',
        method: 'POST',
        cache: false,
        url: action,
        data: data,
        success: function (data) {
            form.find('input:not([name="_token"])').val('');
            form.find('textarea').val('').text('');
            if (form.data('submit-success-title')) {
                head = form.data('submit-success-title');
            }
            if (form.data('submit-success-body')) {
                body = form.data('submit-success-body');
            }
            if (head || body) {
                swal(
                    head,
                    body,
                    'success'
                )
            }
            form.attr('action',action);
        },
        error: function (err) {
            form.attr('action',action);
            if (err.status === 401) {
                console.log('not authenticated user');
            } else if (err.status === 422) {

                var response = err.responseJSON;
                var errors = response.errors;

                var errorsHtml = '';

                $.each(errors, function (key, value) {
                    errorsHtml += ' • ' + value[0] + '\n';
                });

                swal({
                    title: 'Введены неверные данные',
                    text: errorsHtml,
                    icon: 'error'
                })

            } else {
                console.log('Упс...');
            }
        }
    });

});
$(document).ready(function () {
    $(".slider-type-one").owlCarousel({
            loop: false,
            nav: true,
            margin: 10,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive: {
                100: {
                    items: 1
                }
            }
        }
    );
});
$(document).ready(function () {
    $(".slider-type-three-navs").owlCarousel({
            loop: true,
            nav: true,
            margin: 10,
            navText: [
                '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>',
                '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        }
    );
});
$(document).ready(function (e) {
    $("input.phone").inputmask({"mask": "+38(099) 999-99-99"});
});