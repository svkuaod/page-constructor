function addNewOption(element) {
    let data = 'template_id=' + element.dataset.template_id + '&template_view=' + element.dataset.template_view +
        '&parent_id=' + element.dataset.parent_id + '&constructor_id=' + document.getElementById('constructor_id').value +
        '&template_type=' + element.dataset.template_type;
    sendXhrRequest({
        url: '/admin/constructor/add-option',
        method: 'POST',
        onload: function (data) {
            let res = JSON.parse(data.responseText);
            if (res.parent_id) {
                location.reload();
            }
            else {
                location.reload();
                // document.getElementById('construtor').insertAdjacentHTML("afterend",res.view)
            }
        },
        data: data
    });
}

function delete_option(element) {
    let data = 'option_id=' + element.dataset.option_id;
    swal({
        title: 'Удалить блок?',
        icon: "warning",
        buttons: true,
        showCancelButton: true,
        dangerMode: true,
        preConfirm: function () {
            return new Promise(function () {
                sendXhrRequest({
                    url: '/admin/constructor/delete-option',
                    method: 'POST',
                    onload: function (data) {
                        element.closest('.option-block').remove();
                    },
                    data: data
                });
            });
        },
        confirmButtonText: "Удалить",
        cancelButtonText: "Отмена"
    });
}

function deleteResource(element) {
    let data = 'resource_id=' + element.dataset.resource_id;
    swal({
        title: 'Удалить улемент?',
        icon: "warning",
        buttons: true,
        showCancelButton: true,
        dangerMode: true,
        preConfirm: function () {
            return new Promise(function () {
                sendXhrRequest({
                    url: '/admin/constructor/delete-resource',
                    method: 'POST',
                    onload: function (data) {
                        element.closest('.resource-block').remove();
                    },
                    data: data
                });
            });
        },
        confirmButtonText: "Удалить",
        cancelButtonText: "Отмена"
    });
}
function changeResourceStatus(resource_id) {
    if (!resource_id) return false;
    let data = 'resource_id=' + resource_id;
    sendXhrRequest({
        url: '/admin/constructor/active-resource',
        method: 'POST',
        onload: '',
        data: data
    });
}
function full_width_option(element) {
    let data = 'option_id=' + element.dataset.option_id;
    sendXhrRequest({
        url: '/admin/constructor/full-width-option',
        method: 'POST',
        onload: '',
        data: data
    });
}
function saveOptionStyle(option_id) {
    let data = 'option_id=' + option_id + '&style=' + document.getElementById('option_style_' + option_id).value;
    sendXhrRequest({
        url: '/admin/constructor/change-option-style',
        method: 'POST',
        onload: function (data) {
        },
        data: data
    });
}
function switch_active_option(element) {
    let data = 'option_id=' + element.dataset.option_id;
    sendXhrRequest({
        url: '/admin/constructor/switch-active-option',
        method: 'POST',
        onload: '',
        data: data
    });
}
function change_order_option(element, flag) {
    let data = 'option_id=' + element.closest('.sort-control').dataset.option_id +
        '&parent_id=' + element.closest('.sort-control').dataset.parent_id +
        '&constructor_id=' + element.closest('.sort-control').dataset.constructor_id + '&flag=' + flag;
    sendXhrRequest({
        url: '/admin/constructor/change-order-option',
        method: 'POST',
        onload: function (data) {
            location.reload();
        },
        data: data
    });
}

function changeOrderResource(element, flag) {
    let data = 'option_id=' + element.closest('.sort-control').dataset.option_id +
        '&resource_id=' + element.closest('.sort-control').dataset.resource_id +
        '&key=' + element.closest('.sort-control').dataset.key + '&flag=' + flag;
    sendXhrRequest({
        url: '/admin/constructor/change-order-resource',
        method: 'POST',
        onload: function (data) {
            location.reload();
        },
        data: data
    });
}

function upload_file_resource(element) {
    let formData = new FormData();
    formData.append('constructor_id', document.getElementById('constructor_id').value);
    formData.append('option_id', element.dataset.option_id);
    formData.append('resource_id', element.dataset.resource_id);
    formData.append('prev_value', element.dataset.prev_value);
    formData.append('key', element.dataset.key);
    formData.append('type', element.dataset.type);
    formData.append('file', element.files[0], element.files[0].name);
    formData.append('_token', document.querySelector('[name="csrf-token"]').getAttribute('content'));
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/admin/constructor/upload-file', true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            var res = JSON.parse(this.responseText);
            element.closest('.constructor-block').getElementsByClassName('file-preview')[0].setAttribute('src', '/storage/' + res.file_path);
            element.setAttribute('data-prev_value', res.file_path);
        } else {

        }
    };
    xhr.send(formData);
}

function addResourceToOption(element) {
    let data = 'option_id=' + element.dataset.option_id + '&key=' + element.dataset.key + '&sub_view=' + element.dataset.sub_view
        + '&names=' + element.dataset.names + '&items=' + element.dataset.items;
    sendXhrRequest({
        url: '/admin/constructor/add-resource-to-option',
        method: 'POST',
        onload: function (data) {
            let res = JSON.parse(data.responseText);
            console.log(res);
            element.closest('.resources-block').getElementsByClassName('resource-items')[0].insertAdjacentHTML("beforeend", res.view);
        },
        data: data
    });
}

function addResourceToResource(element) {
    let data = 'resource_id=' + element.dataset.resource_id + '&key=' + element.dataset.key + '&sub_view=' + element.dataset.sub_view;
    sendXhrRequest({
        url: '/admin/constructor/add-resource-to-resource',
        method: 'POST',
        onload: function (data) {
            let res = JSON.parse(data.responseText);
            element.closest('.resources-block').getElementsByClassName('resource-items')[0].insertAdjacentHTML("beforeend", res.view);
        },
        data: data
    });
}

function changeResourceValue(element) {
    let value = encodeURIComponent(element.value);
    let data = 'option_id=' + element.dataset.option_id + '&resource_id=' + element.dataset.resource_id +
        '&key=' + element.dataset.key + '&value=' + value;
    sendXhrRequest({
        url: '/admin/constructor/change-resource-value',
        method: 'POST',
        onload: function (data) {
            console.log(data);
            if (!data) return;
            let res = JSON.parse(data.responseText);
            element.dataset.resource_id = res.resource_id;
        },
        data: data
    });
}
function changeBackGroundColor(element) {
    let data = 'option_id=' + element.dataset.option_id + '&value=' + element.value;
    sendXhrRequest({
        url: '/admin/constructor/change-background-value',
        method: 'POST',
        onload: function (data) {
        },
        data: data
    });
}

function sendXhrRequest(params) {
    let xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        xhr.open(params.method, params.url, true);
    } else if (typeof XDomainRequest !== "undefined") {
        xhr = new XDomainRequest();
        xhr.open(params.method, params.url + ((/\?/).test(url) ? "&" : "?") + (new Date()).getTime(), true);
    } else {
        xhr = null;
    }
    if (!xhr) alert('XHR not supported!');
    if (params.data) {
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }
    if (params.onload) {
        xhr.onload = function () {
            params.onload(xhr);
        };
    }
    if (params.data) {
        if (params.data.indexOf('_token') < 0) {
            params.data += '&_token=' + document.querySelector('[name="csrf-token"]').getAttribute('content');
        }
        xhr.send(params.data);
    } else {
        xhr.send();
    }
}

function serializeForm(form) {
    let serialized = [];
    for (let i = 0; i < form.elements.length; i++) {
        let field = form.elements[i];
        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;
        if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
            serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
        }
    }
    return serialized.join('&');
}

function startSelectPhoto(element) {
    element.closest('.constructor-block').querySelector('input.upload-file').click();
}

function copyInfoData(id) {
    let data = 'constructor_id=' + id;
    swal({
        title: 'Все данные в других языковых таблицах будут перезаписаны',
        icon: 'warning',
        buttons: true,
        showCancelButton: true,
        dangerMode: true,
        preConfirm: function () {
            return new Promise(function () {
                sendXhrRequest({
                    url: '/admin/constructor/replicate-info',
                    method: 'POST',
                    onload: function (data) {
                        if(data.status == 200){
                            location.reload();
                        }
                    },
                    data: data
                });
            });
        },
        confirmButtonText: 'Записать',
        cancelButtonText: 'Отмена'
    });
}
