<script type="application/ld+json">
{
"@context": "https://schema.org",
"@type": "NewsArticle",
"mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "{{request()->root() . getLink($model->itemPath)}}"
    },
"headline": "Article headline",
"image":
    "{{request()->root() . $model->description->photoPath}}"
,
"datePublished": "{{$model->description->date}}",
"dateModified": "{{$model->updated_at}}",
"author": {
"@type": "Thing",
"name": "Admin"
},
"publisher": {
    "@type": "Organization",
    "name": "{{env('APP_URL')}}",
    "logo": {
        "@type": "ImageObject",
        "url": "{{request()->root() . '/logo.png'}}"
    }
},
"description": "{{$model->description->body_m}}"
}
</script>