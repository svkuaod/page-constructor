<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$meta_title ?? env('APP_NAME') ?? ''}}</title>
    <meta name="description" content="{{$meta_description ?? env('APP_NAME') ?? ''}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="twitter:card" content="{{request()->url()}}">
    <meta name="twitter:site" content="{{env('APP_URL')}}">
    <meta name="twitter:title" content="{{$meta_title ?? env('APP_NAME') ?? ''}}">
    <meta name="twitter:description" content="{{$meta_description ?? env('APP_NAME') ?? ''}}">
    <meta name="twitter:image:src" content="{{request()->root() . '/logo.png'}}">

    <meta property="og:locale" content="{{str_replace('_', '-', app()->getLocale()) }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$meta_title ?? env('APP_NAME') ?? ''}}">
    <meta property="og:description" content="{{$meta_description ?? env('APP_NAME') ?? ''}}">
    <meta property="og:url" content="{{request()->root()}}">
    <meta property="og:image" content="{{request()->root() . '/logo.png'}}">
    <meta property="og:site_name" content="{{env('APP_URL')}}">

    @if(request()->page) <link rel="canonical" href="{{request()->url()}}"/> @endif

    {!! \Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::getCodesByKey(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::HEAD_KEY) !!}

    @stack('styles')
</head>
<body>

{!! \Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::getCodesByKey(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::BODY_TOP_KEY) !!}

@yield('header')
@yield('bread_crumbs')

<main>
    @yield('content')
</main>
@yield('footer')
@stack('scripts')

{!! \Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::getCodesByKey(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::BODY_BOTTOM_KEY) !!}

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "{{env('APP_URL')}}",
  "description": "{{$meta_description ?? env('APP_NAME') ?? ''}}",
  "email": "{{implode(';',\Svkuaod\PageConstructor\Models\Settings\Settings::getEmailAdmin())}}",
  "legalName": "{{env('APP_URL')}}",
  "url": "{{env('APP_URL')}}",
  "logo": "{{request()->root() . '/logo.png'}}",
  "image": "{{request()->root() . '/logo.png'}}",
  "address":
  {
      "@type": "PostalAddress",
      "addressLocality": "Одесса",
      "addressRegion": "Одесская область",
      "addressCountry": "Украина",
      "postalCode": "65000",
      "streetAddress": ""
  },
  "contactPoint": [
    {
      "@type": "ContactPoint",
      "telephone" : "",
      "contactType": "sales"
    }
    ]
  }
</script>

</body>
</html>

