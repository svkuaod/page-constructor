<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        @if($resourceKeySliderOne && count($resourceKeySliderOne))
            <div id="slider_with_preview_{{$option->id ?? 0}}" class="carousel slide slider-with-preview"
                 data-ride="carousel">
                <div class="d-none d-sm-block">
                    <ol class="carousel-indicators">
                        @foreach($resourceKeySliderOne as $item)
                            @if($item->active)
                            <li data-target="#slider_with_preview_{{$option->id ?? 0}}" data-slide-to="{{$loop->index}}"
                                @if($loop->first) class="active" @endif
                                {{$item->getChildrenValuePathByKey($keyVarThree)}}
                                style="background-image: url({{$item->childs->where('key',$keyPhotoTwo)->first()->info->valuePath ?? $defaultImgSrc ?? ''}});">
                            </li>
                            @endif
                        @endforeach
                    </ol>
                </div>
                <div class="carousel-inner">
                    @foreach($resourceKeySliderOne as $item)
                        @if($item->active)
                        <div class="carousel-item @if($loop->first) active @endif">
                            <a class="d-block h-100-vh"
                               href="{{$item->getChildrenValuePathByKey($keyPhotoOne)}}"
                               data-fancybox="gallery-{{$option->id}}">
                                <img class="d-block w-100 h-100-vh ob-f-cove"
                                     src="{{$item->getChildrenValuePathByKey($keyPhotoOne)}}"
                                     alt="slide-{{$item->id}}">
                            </a>
                        </div>
                        @endif
                    @endforeach
                </div>
                <a class="carousel-control-prev t-d-none" href="#slider_with_preview_{{$option->id ?? 0}}" role="button"
                   data-slide="prev">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </a>
                <a class="carousel-control-next t-d-none" href="#slider_with_preview_{{$option->id ?? 0}}" role="button"
                   data-slide="next">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>
            </div>
        @endif
    </div>
</div>

