<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        @if($resourceKeySliderOne && count($resourceKeySliderOne))
            <div class="owl-carousel slider-type-one">
                @foreach($resourceKeySliderOne as $item)
                    @if($item->active)
                    <div class="item">
                        <a href="{{$item->getChildrenValuePathByKey($keySliderOne)}}"
                           data-fancybox="gallery-{{$option->id}}">
                            <img src="{{$item->getChildrenValuePathByKey($keySliderOne)}}" alt="slide-{{$item->id}}" class="img-fluid">
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        @endif
    </div>
</div>

