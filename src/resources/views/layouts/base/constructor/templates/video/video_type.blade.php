<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="constructor-video">
        @if($resourceKeyVideoOne && isset($resourceKeyVideoOne->info))
            <video autoplay muted loop class="video-fluid z-depth-1" width="100%" style="width: 100%">
                <source src="{{$resourceKeyVideoOne->info->valuePath ?? ''}}" type="video/mp4">
            </video>
        @endif
    </div>
</div>
