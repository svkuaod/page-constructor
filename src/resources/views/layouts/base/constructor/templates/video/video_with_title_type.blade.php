<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        <div class="video-block">
            @if($resourceKeyVideoOne && isset($resourceKeyVideoOne->info))
                <video autoplay muted loop class="video-fluid z-depth-1" width="100%" style="width: 100%">
                    <source src="{{$resourceKeyVideoOne->info->valuePath ?? ''}}" type="video/mp4">
                </video>
            @endif
            @if($resourceKeyVarOne && isset($resourceKeyVarOne->info))
                <div class="title">{{$resourceKeyVarOne->info->value ?? ''}}</div>
            @endif
        </div>
    </div>
</div>