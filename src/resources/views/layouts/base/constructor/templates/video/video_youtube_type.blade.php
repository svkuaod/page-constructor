<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        <div class="video-youtube-block">
            @if($resourceKeyVideoOne && isset($resourceKeyVideoOne->info))
                <div class="video">
                    <iframe style="width: 100%;height: 100%"
                            src="https://www.youtube.com/embed/{{$resourceKeyVideoOne->info->value}}"></iframe>
                </div>
            @endif
        </div>
    </div>
</div>