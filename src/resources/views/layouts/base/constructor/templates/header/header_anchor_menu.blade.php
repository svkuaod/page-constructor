@php($languages = \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getLanguages())
<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};"
     id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="container custom-container">
        <div class="d-flex justify-content-between pt-20 pb-20 f-w-w">
            <div class="logo d-flex align-items-center f-w-w">
                <img class="h-60" src="{{$resourceKeyPhotoOne->info->valuePath ?? ''}}" alt="logo"/>
            </div>
            <div class="d-flex flex-column float-right f-w-w">
                <div class="contacts d-flex j-c-end sm-j-c-c f-w-w">
                    <div class="c-white f-s-18 d-flex align-items-center mr-25 f-w-w sm-m-5">{{$resourceKeyVarOne->info->value ?? ''}}</div>
                    @if($resourceKeyChildOne && count($resourceKeyChildOne))
                        @foreach($resourceKeyChildOne as $item)
                            @if($item->active)
                                <a class="btn-red p-7-12 f-s-14"
                                   href="{{getLink($item->childs->where('key',$keyVarTwo)->first()->info->value ?? '#')}}">
                                    {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                </a>
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class="links d-flex pt-10 f-w-w sm-d-none">
                    @if($resourceKeyChildTwo && count($resourceKeyChildTwo))
                        @foreach($resourceKeyChildTwo as $item)
                            @if($item->active)
                                <a class="f-f-ossb f-s-14 c-white t-upper text-decoration-none ml-25"
                                   href="{{getLink($item->childs->where('key',$keyVarTwo)->first()->info->value ?? '#')}}">
                                    {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                </a>
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class="pos-f-t d-none sm-d-block">
                    <div class="collapse" id="navbarToggleExternalContent">
                        <div class="back-trans p-4">
                            @if($resourceKeyChildTwo && count($resourceKeyChildTwo))
                                @foreach($resourceKeyChildTwo as $item)
                                    @if($item->active)
                                        <div>
                                            <a class="f-f-ossb f-s-14 c-white t-upper text-decoration-none ml-25"
                                               href="{{getLink($item->childs->where('key',$keyVarTwo)->first()->info->value ?? '#')}}">
                                                {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                            </a>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <nav class="navbar navbar-dark back-trans">
                        <button class="navbar-toggler m-auto" type="button" data-toggle="collapse"
                                data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                </div>
            </div>
            <div class="d-flex j-c-end a-a-c h-100pr pv-5px sm-j-c-c">
                @foreach($languages as $item)
                    <a href="{{getLangSwitcherLink($item)}}"
                       class="lang-swithcher f-f-ossb fs-13px f-up mh-5px @if($item === app()->getLocale()) active @endif">
                        {{$item}}</a>
                    @if(!$loop->last)<span class="f-f-ossb fs-13px f-up">/</span>@endif
                @endforeach
            </div>
        </div>
    </div>
</div>
<div>

</div>

