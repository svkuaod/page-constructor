@php
    $headerMenu = \Svkuaod\PageConstructor\Models\Menu\HeaderMenu::where('active',1)->whereNull('parent_id')->with('childs')->orderBy('order')->get();
    $languages = \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getLanguages();
@endphp
@if(isset($headerMenu) && count($headerMenu))
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 bg-broune shadow-sm c-white">
        <h5 class="my-0 mr-md-auto font-weight-normal c-white t-upper"><a class="c-white t-dec-none"
                                                                          href="{{getLink('')}}">{{env('APP_NAME') ?? 'Promo'}}</a>
        </h5>
        <nav class="navbar navbar-expand-lg navbar-light bg-broune rounded">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsExample" aria-controls="navbarsExample" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarsExample">
                <ul class="navbar-nav mr-auto">
                    @foreach($headerMenu as $item)
                        <li class="nav-item @if($item->childs && count($item->childs)) dropdown @endif">
                            @if($item->childs && count($item->childs))
                                <a class="nav-link dropdown-toggle c-white t-dec-none"
                                   id="dropdown{{$item->id ?? ''}}"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$item->info->name ?? 'item'}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdown{{$item->id ?? ''}}">
                                    @foreach($item->childs as $subItem)
                                        <a class="nav-link c-white t-dec-none"
                                           href="{{getLink($subItem->constructor->publicUrl ?? $subItem->url)}}">{{$subItem->info->name ?? 'item'}}</a>
                                    @endforeach
                                </div>
                            @else
                                <a class="nav-link c-white t-dec-none"
                                   href="{{getLink($item->constructor->publicUrl ?? $item->url)}}">{{$item->info->name ?? 'item'}}</a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </nav>
        <div class="locale d-flex">
            <div class="d-flex j-c-end a-a-c h-100pr pv-5px sm-j-c-c">
                @foreach($languages as $item)
                    <a href="{{getLangSwitcherLink($item)}}"
                       class="lang-swithcher f-f-ossb fs-13px f-up mh-5px @if($item === app()->getLocale()) active @endif">
                        {{$item}}</a>
                    @if(!$loop->last)<span class="f-f-ossb fs-13px f-up">/</span>@endif
                @endforeach
            </div>
        </div>
    </div>
@endif