<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        <div class="photo-block">
            @if(isset($resourceKeyPhotoOne->info->value))
                <a data-fancybox="photo-{{$resourceKeyPhotoOne->id ?? ''}}"
                   href="{{$resourceKeyPhotoOne->info->valuePath}}">
                    <img src="{{$resourceKeyPhotoOne->info->valuePath}}" alt="photo-{{$option->id}}">
                </a>
            @endif
        </div>
    </div>
</div>

