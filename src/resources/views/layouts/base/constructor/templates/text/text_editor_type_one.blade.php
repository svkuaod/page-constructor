<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        <div class="col-12 p-0">
            @if(isset($option->text) && $option->text)
                {!!  $option->text->info->value ?? '' !!}
            @endif
        </div>
    </div>
</div>
