<footer>
    <div class="container-fluid" style="background-color: {{$option->background_color ?? '#fff'}};">
        <div class="container custom-container">
            <div class="row">

            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @if($resourceKeyChildOne && count($resourceKeyChildOne))
                    <div class="d-flex j-c-b f-w-w p-50-0">
                        @foreach($resourceKeyChildOne as $item)
                            @if($item->active)
                                @if($item->childs->where('key',$keyVarTwo)->first()->info->value ?? false)
                                    <a class="f-f-hncr c-white"
                                       href="{{$item->childs->where('key',$keyVarTwo)->first()->info->value}}">
                                        {!! $item->childs->where('key',$keyVarOne)->first()->info->value ?? '' !!}
                                    </a>
                                @else
                                    <span class="c-white">{!! $item->childs->where('key',$keyVarOne)->first()->info->value ?? '' !!}</span>
                                @endif
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
    </div>
</footer>
