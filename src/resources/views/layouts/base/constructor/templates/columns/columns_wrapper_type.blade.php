<div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
    <div class="row">
        @foreach($innerViews as $view)
                {!! $view ?? ''!!}
        @endforeach
    </div>
</div>