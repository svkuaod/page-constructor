<div class="col-12 col-lg-{{$option->value ?? 12}} p-0">
    <div class="{{$container}}" style=" background-color: {{$option->background_color ?? '#fff'}};">
        <div class="row">
            @foreach($innerViews as $view)
                <div class="col-sm-12 p-0">
                    {!! $view ?? ''!!}
                </div>
            @endforeach
        </div>
    </div>
</div>
