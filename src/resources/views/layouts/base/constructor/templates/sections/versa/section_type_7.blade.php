<div class="bg-dark pt90 pb90" id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mr-auto ml-auto text-center">
                <h3 class="text-white h3 mb20">
                    {{$resourceKeyVarOne->info->value ?? ''}}
                </h3>
                <p class="lead text-white text-muted mb20">
                    {{$resourceKeyVarTwo->info->value ?? ''}}
                </p>
                @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                    @foreach($resourceKeyChildOne as $item)
                        @if($item->active)
                            <a href="{{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}"
                               class="{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}">
                                {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                            </a>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
