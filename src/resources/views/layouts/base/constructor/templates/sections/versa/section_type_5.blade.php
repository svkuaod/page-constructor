<div class="bg-faded pt90" id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="container">
        <div class='feature-col saas-features pb90'>
            <div class='row align-items-center'>

                <div class='col-lg-5 mr-auto'>
                    <h3 class="mb30">{{$resourceKeyVarOne->info->value ?? ''}}</h3>
                    <div class="tabs-default tabs-icon">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-default mb30 justify-content-start" role="tablist">
                            @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                                @foreach($resourceKeyChildOne as $item)
                                    @if($item->active)
                                        <li class="nav-item" role="presentation">
                                            <a class="@if($loop->first)active @endif nav-link"
                                               href="#tb{{$item->id}}" aria-controls="tb{{$item->id}}" role="tab"
                                               data-toggle="tab">
                                                {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content text-left mb40">
                            @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                                @foreach($resourceKeyChildOne as $item)
                                    @if($item->active)
                                        <div role="tabpanel"
                                             class="tab-pane @if($loop->first)active show @endif fade"
                                             id="tb{{$item->id}}">
                                            <p>{{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}</p>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                    @if(isset($resourceKeyChildTwo) && count($resourceKeyChildTwo))
                        @foreach($resourceKeyChildTwo as $item)
                            @if($item->active)
                                <a href='{{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}'
                                   class='{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}'>
                                    {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                </a>
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class='col-lg-6 pt50-md'>
                    @if($resourceKeyPhotoOne && isset($resourceKeyPhotoOne->info))
                        <img src='{{$resourceKeyPhotoOne->info->valuePath ?? $defaultImgSrc}}' alt=''
                             class='img-fluid'>
                    @endif
                </div>
            </div>
        </div><!--feature col-->
    </div>
</div>

