<div class='saas-app-hero bg-faded' id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class='container pt100 pb100'>
        <div class='row align-items-center'>
            <div class='col-md-6'>
                <h2 class=' h1 mb20 font500'>
                    {{$resourceKeyVarOne->info->value ?? ''}}
                </h2>
                <p class='lead text-muted mb40'>
                    {{$resourceKeyVarTwo->info->value ?? ''}}
                </p>
                @if($resourceKeyChildOne && count($resourceKeyChildOne))
                    @foreach($resourceKeyChildOne as $item)
                        @if($item->active)
                            <a class='{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}'
                               href='{{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}'>
                                {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                            </a>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class='col-md-6'>
                <div class="owl-carousel owl-theme owl-image-header sm-mt-10">
                    @if($resourceKeySliderOne && count($resourceKeySliderOne))
                        @foreach($resourceKeySliderOne as $item)
                            @if($item->active && isset($item->info->valuePath))
                                <div class="item">
                                    <img src="{{$item->info->valuePath ?? $defaultImgSrc ?? ''}}" class="img-fluid">
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
