<div class=" bg-primary pb80 pt80" id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="container">
        <div class="center-title mb50 text-center">
            <h4 class="text-white">{{$resourceKeyVarOne->info->value ?? ''}}</h4>
        </div>
        <div class="row">
            <div class="col-md-8 mr-auto ml-auto">
                <div class="owl-carousel owl-theme owl-testimonials-full">
                    @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                        @foreach($resourceKeyChildOne as $item)
                            @if($item->active)
                                <div class="item">
                                    <div class="testimonial-full">
                                        <p class="lead text-white text-muted">
                                            {{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                        </p>
                                        <div class="clearfix testimonial-author">
                                            <img src="{{$item->childs->where('key',$keyPhotoOne)->first()->info->valuePath ?? ''}}"
                                                 alt="" class="img-fluid float-left mr-3 rounded-circle" width="60">
                                            <h5 class="font400 h6 mb0 pt5 text-white">{{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}</h5>
                                            <small class="text-white text-muted">{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}</small>
                                        </div>
                                    </div>
                                </div><!--/item-->
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
