<div class="container pt90 pb90" id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="center-title text-center mb50">
        <h4>{{$resourceKeyVarOne->info->value ?? ''}}</h4>
    </div>
    <div class="owl-carousel owl-theme owl-clients">
        @if($resourceKeySliderOne && count($resourceKeySliderOne))
            @foreach($resourceKeySliderOne as $item)
                @if($item->active)
                    <div class="item">
                        <img src="{{$item->info->valuePath ?? $defaultImgSrc ?? ''}}" alt="" class="img-fluid">
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div>
