<div class='container pt90 pb30' id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class='row align-items-center'>
        <div class='col-lg-4 mb30'>
            @if($resourceKeyPhotoOne && isset($resourceKeyPhotoOne->info))
                <img src='{{$resourceKeyPhotoOne->info->valuePath ?? $defaultImgSrc}}' alt=''
                     class='center-img img-fluid'>
            @endif
        </div>
        <div class='col-lg-7 ml-auto pt50-md'>
            <h3 class='mb20'>{{$resourceKeyVarOne->info->value ?? ''}}</h3>
            <p class='lead mb50'>
                {{$resourceKeyVarTwo->info->value ?? ''}}
            </p>
            <div class='row'>
                @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                    @foreach($resourceKeyChildOne as $item)
                        @if($item->active)
                            <div class="col-md-6 mb40">
                                <div class="icon-card-style1 p0 border0">
                                    <i class="{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}"></i>
                                    <div class="overflow-hiden">
                                        <h4 class="h6 font400">{{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}</h4>
                                        <p>
                                            {{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
