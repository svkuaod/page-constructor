<div class="pt100 pb60 container" id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="center-title text-center mb50">
        <h4>{{$resourceKeyVarOne->info->value ?? ''}}</h4>
    </div>
    <div class="row">
        @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
            @foreach($resourceKeyChildOne as $item)
                @if($item->active)
                    <div class="col-md-4 mb40">
                        <div class="{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}">
                            <h5>{{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}</h5>
                            @if(isset($item->childs) && count($item->childs->where('key',$keyChildOne)))
                                @foreach($item->childs->where('key',$keyChildOne)->sortBy('order') as $subItem)
                                    @if($subItem->active)
                                        <span class="{{$subItem->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}">
                                        {{$subItem->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}
                                            </span>
                                    @endif
                                @endforeach
                            @endif
                            <div class="price-tag">
                                <sup>$</sup>{{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}
                                <sub>/M</sub></div>
                            <ul class="list-unstyled">
                                @if(isset($item->childs) && count($item->childs->where('key',$keyChildTwo)))
                                    @foreach($item->childs->where('key',$keyChildTwo)->sortBy('order') as $subItem)
                                        @if($subItem->active)
                                            <li>{{$subItem->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}</li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <a href="{{$item->childs->where('key',$keyVarFive)->first()->info->value ?? ''}}"
                               class="{{$item->childs->where('key',$keyVarSix)->first()->info->value ?? ''}}">
                                {{$item->childs->where('key',$keyVarFour)->first()->info->value ?? ''}}
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div>

