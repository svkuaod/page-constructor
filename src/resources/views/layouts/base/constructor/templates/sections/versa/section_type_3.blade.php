<div class="bg-faded pt90 pb50" id="{{$resourceKeySectionId->info->value ?? ''}}">
    <div class="container">
        <div class="center-title text-center mb50">
            <h4 class="mb0">{{$resourceKeyVarOne->info->value ?? ''}}</h4>
            <p>{{$resourceKeyVarTwo->info->value ?? ''}}</p>
        </div>
        <div class="row">
            @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                @foreach($resourceKeyChildOne as $item)
                    @if($item->active)
                        <div class="col-md-4 mb40">
                            <div class="icon-card-style1 bg-white">
                                <i class="{{$item->childs->where('key',$keyVarThree)->first()->info->value ?? ''}}"></i>
                                <div class="overflow-hiden">
                                    <h4 class="h6 font400">{{$item->childs->where('key',$keyVarOne)->first()->info->value ?? ''}}</h4>
                                    <p>
                                        {{$item->childs->where('key',$keyVarTwo)->first()->info->value ?? ''}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif

        </div>
    </div>
</div>