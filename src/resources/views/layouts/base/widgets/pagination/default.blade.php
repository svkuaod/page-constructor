@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="item active" aria-current="page"><span>{{ $page }}</span></li>
                    @else
                        <li class="item"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
    </ul>
@endif
<style>
    .pagination{
        width: max-content;
        margin: auto;
    }
    .pagination .item {
        border: unset;
        border-radius: 30px;
        background-color: #efefef;
        color: #222;
        text-decoration: none;
        height: 25px;
        line-height: 1;
        padding: 5px 20px;
        margin: 0 10px;
        font-size: inherit;
        font-family: inherit;
        font-size: 16px;
    }
    .pagination .item a{
        text-decoration: none;
        color: #222;
    }
    .pagination .item.active{
        background: #c70000;
        color: #fff;
    }
</style>