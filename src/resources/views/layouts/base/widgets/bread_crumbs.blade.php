@if(isset($items) && count($items))
    <div class="container-fluid" style="background-color: #f5f5f5">
        <div class="pl-20px pv-15px">
            <div class="d-flex f-w-w">
                <a class="f-f-hnci fs-12px c-a7 t-d-none" href="{{getLink('')}}">@lang('page_constructor::fields.main_page')</a>
                @foreach($items as $item)
                    <i class="fa fa-long-arrow-right c-a7 ph-5px fs-12px lh-12px m-a-0" aria-hidden="true"></i>
                    @if(!$loop->last)
                        <a class="f-f-hnci fs-12px c-a7 t-d-none" href="{{getLink($item['url'])}}">{{$item['name']}}</a>
                    @else
                        <span class="f-f-hnci fs-12px c-a7 t-d-none">{{$item['name']}}</span>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endif
<script type="application/ld+json">
    {
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  {
    "@type": "ListItem",
    "position": 1,
    "name": "@lang('page_constructor::fields.main_page')",
    "item": "{{request()->root() . getLink(' ')}}"
  }
  @foreach($items as $item) ,{
    "@type": "ListItem",
    "position": {{$loop->iteration + 1}},
    "name": "{{$item['name']}}",
    "item": "{{request()->root() . getLink($item['url'])}}"
  }
@endforeach ]
}
</script>