@extends($pathMainView,['meta_title' => $meta_title ?? '', 'meta_keywords' => $meta_keywords ?? '', 'meta_description' => $meta_description ?? ''])

@push('styles')

<link href="{{ asset('vendor/page_constructor/packages/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/page_constructor/packages/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{asset('vendor/page_constructor/packages/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('vendor/page_constructor/packages/owl-carousel/css/owl.theme.default.min.css')}}" rel="stylesheet">
<link href="{{ asset('vendor/page_constructor/packages/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">

<link href="{{ asset('vendor/page_constructor/css/page_constructor_default.css')}}" rel="stylesheet">
<link href="{{ asset('vendor/page_constructor/css/page_constructor_custom.css')}}" rel="stylesheet">
<script src="{{ asset('vendor/page_constructor/packages/jquery/jquery-3.3.1.min.js')}}"></script>
@endpush

@section('header')
    @include($pathHeader)
@endsection
@section('bread_crumbs')
    {!! breadcrumbs()->render() !!}
@endsection
@section('content')
    {!! $view ?? ''!!}
@endsection
@section('footer')
    @include($pathFooter)
@endsection

@push('scripts')
<script src="{{ asset('vendor/page_constructor/packages/popper/js/popper.min.js') }}"></script>
<script src="{{ asset('vendor/page_constructor/packages/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/page_constructor/packages/owl-carousel/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('vendor/page_constructor/packages/fancybox/jquery.fancybox.min.js')}}"></script>
<script src="{{ asset('vendor/page_constructor/js/web.js')}}"></script>
<script src="{{ asset('vendor/page_constructor/packages/parallax/parallax.min.js')}}"></script>
<script src="{{ asset('vendor/page_constructor/packages/sweetalert/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('vendor/page_constructor/packages/masked/jquery.inputmask.bundle.js') }}" defer></script>

@endpush


