@php
    $rand = rand(00000,99999);
    $parentOptionId = $parentOptionId ?? '';
@endphp
<div class="container" style="margin-bottom: 100px">
    <div class="row">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                Добавить Опцию
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                @if(isset($templates) && count($templates))
                    @foreach($templates as $type=>$templateTypes)
                        @foreach($templateTypes as $template)
                            @if(!$template->parent_id && $template->active)
                                <li class="dropdown-submenu">
                                    <button type="button" class="btn btn-secondary" style="width: 100%;background: none"
                                            data-toggle="modal"
                                            data-target="#modal-{{$type}}-{{$template->id}}-{{$rand}}">
                                        {{$template->name}}
                                    </button>
                                </li>
                            @endif
                        @endforeach
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>

@foreach($templates as $type=>$templateTypes)
    @foreach($templateTypes as $template)
        @if(!$template->parent_id  && $template->active)
            <div class="modal fade" id="modal-{{$type}}-{{$template->id}}-{{$rand}}" tabindex="-1" role="dialog"
                 aria-labelledby="modalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered new-option-modal" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{$template->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                                @foreach($templateTypes as $templateSub)
                                    @if($template->id == $templateSub->parent_id && $templateSub->active)
                                        <div class="add-new-option" onclick="addNewOption(this)"
                                             data-template_id="{{$templateSub->id}}"
                                             data-template_view="{{$templateSub->view}}"
                                             data-parent_id="{{$parentOptionId}}"
                                             data-template_type="{{$type}}"
                                             data-dismiss="modal">
                                            <div class="select-option-body">
                                                <div class="name">
                                                    {{$templateSub->name}}
                                                </div>
                                                <div class="photo">
                                                    <img src="{{$templateSub->photo}}">
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    @endif
                                @endforeach
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endforeach


