<div class="background-color-option">
    <fieldset>
        <div>
            <label for="name">Цвет фона раздела:</label>
            <input type="color" class="option_background_color" onchange="changeBackGroundColor(this)"
                   data-option_id="{{$option->id ?? 0}}" name="color"
                   value="{{$option->background_color ?? '#ffffff'}}"/>
        </div>
    </fieldset>
</div>
