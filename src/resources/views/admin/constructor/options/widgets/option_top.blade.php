<div class="option-header">
    <div class="sort-control" data-option_id="{{$option->id ?? 0}}" data-parent_id="{{$option->parent_id ?? 0}}"
         data-constructor_id="{{$option->constructor_id ?? 0}}">
        <i class="fa fa-arrow-up" aria-hidden="true" onclick="change_order_option(this,'up')"></i>
        <i class="fa fa-arrow-down" aria-hidden="true" onclick="change_order_option(this,'down')"></i>
    </div>
    <div class="title">{{$blockName ?? $option->template->name ?? 'Блок'}}</div>
    <div class="options">
        <div class="active d-flex m-auto mr-5">
            <label class="switch mb-3" data-toggle="tooltip" title="Вкл/выключить отображение">
                <input type="checkbox" @if(isset($option) && $option->active) checked @endif
                onchange="switch_active_option(this)"
                       data-option_id="{{$option->id ?? 0}}">
                <span class="slider round"></span>
            </label>
        </div>
        <div class="preview">
            <a href="/preview/option/{{$option->id ?? 0}}" data-toggle="tooltip" title="Превью" target="_blank"><i
                        class="fa fa-eye" aria-hidden="true"></i>
            </a>
        </div>
        <div class="close">
            @if($hideClose ?? true)
                <span data-toggle="tooltip" title="Удалить" class="delete-option" data-option_id="{{$option->id ?? 0}}"
                      onclick="delete_option(this)"><i
                            class="fa fa-close" aria-hidden="true"></i></span>
            @endif
        </div>
    </div>
</div>
