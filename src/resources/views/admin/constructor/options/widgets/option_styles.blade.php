<div class="styles-option">
    <!-- Trigger the modal with a button -->
    <label>Дополнительные стили контейнера</label>
    <a type="button" class="btn btn-info btn-lg open" data-toggle="modal" data-target="#optionStyles-{{$option->id}}">Открыть</a>

    <!-- Modal -->
    <div id="optionStyles-{{$option->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Дополнительные стили контейнера</h4>
                </div>
                <div class="modal-body">
                   <textarea id="option_style_{{$option->id}}" placeholder="margin-top:25px;">{!! $option->style !!}</textarea>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-default" onclick="saveOptionStyle({{$option->id}})" data-dismiss="modal">Сохранить</a>
                </div>
            </div>
        </div>
    </div>
</div>
