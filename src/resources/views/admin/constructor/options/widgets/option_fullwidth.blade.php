<div class="full-width-option d-flex a-a-c">
    <label for="full-width-option">Отображать раздел на всю ширину экрана</label>
    <label class="switch mb-3 ml-5" data-toggle="tooltip" title="Вкл/выключить отображение">
        <input type="checkbox" @if(isset($option) && $option->full_width) checked @endif
        onchange="full_width_option(this)"
               data-option_id="{{$option->id ?? 0}}">
        <span class="slider round"></span>
    </label>
</div>
