<div class="option-block" id="option_{{$option->id ?? 0}}">
    @include ($pathOptionTop)
    @if($showFullWidth ?? true) @include ($pathOptionFullWidth) @endif
    @if($showBackground ?? true) @include ($pathOptionBackGround) @endif
    @if($showStyles ?? true) @include ($pathOptionStyles) @endif
    @yield('option_template')
</div>
<hr>