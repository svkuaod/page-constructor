@extends($pathOption)
@section('option_template')
    @include($pathResourceInput,['name' => 'Id секции(для якорного меню)', 'key'=>$keySectionId,'curResource'=>$resourceKeySectionId])
    @include($pathPhotoUpload,['name' => 'Logo', 'key'=>$keyPhotoOne,'curResource'=>$resourceKeyPhotoOne])
    @include($pathResourceInput,['name' => 'Телефон', 'key'=>$keyVarOne,'curResource'=>$resourceKeyVarOne])

    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeyChildOne && count($resourceKeyChildOne))
                @foreach($resourceKeyChildOne as $item)
                    @include($pathInputsBlockTwo,['key'=>$keyChildOne,'resource'=>$item,'subName' => 'Кнопка'])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);" data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildOne}}" data-sub_view="{{$pathInputsBlockTwo}}">Добавить кнопку</span>
        </div>
    </div>
    <hr>
    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeyChildTwo && count($resourceKeyChildTwo))
                @foreach($resourceKeyChildTwo as $item)
                    @include($pathInputsBlockTwo,['key'=>$keyChildTwo,'resource'=>$item,'subName' => 'Якорное меню'])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);" data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildTwo}}" data-sub_view="{{$pathInputsBlockTwo}}">Добавить якорное меню</span>
        </div>
    </div>
@endsection