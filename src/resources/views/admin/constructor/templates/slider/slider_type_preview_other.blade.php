@extends($pathOption)
@section('option_template')
    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeySliderOne && count($resourceKeySliderOne))
                @foreach($resourceKeySliderOne as $item)
                    @include($pathSliderItemWithPreview,['key'=>$keySliderOne,'resource'=>$item,'subName' => 'Фото'])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);"
                  data-option_id="{{$option->id ?? ''}}"
                  data-sub_view="{{$pathSliderItemWithPreview}}"
                  data-key="{{$keySliderOne}}">Добавить Фото в сдайдер</span>
        </div>
    </div>
@endsection