@extends($pathOption)
@section('option_template')
    @include($pathResourceTextArea,['name' => 'Контакты', 'key'=>$keyVarOne,'curResource'=>$resourceKeyVarOne])
    <hr>
    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeyChildOne && count($resourceKeyChildOne))
                @foreach($resourceKeyChildOne as $item)
                    @include($pathInputsBlockTwo,['key'=>$keyChildOne,'resource'=>$item,'subName' => 'Ссылка'])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);" data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildOne}}" data-sub_view="{{$pathInputsBlockTwo}}">Добавить ссылку</span>
        </div>
    </div>
@endsection