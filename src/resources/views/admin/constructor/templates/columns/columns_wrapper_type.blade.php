@extends($pathOption,['blockName' => 'колонки'])
@section('option_template')
@if(isset($innerViews) && count($innerViews))
    <div class="row">
    @foreach($innerViews as $view)
        {!! $view !!}
    @endforeach
    </div>
@else
    <div data-columns-selector style="margin-top: 25px;margin-bottom: 25px; text-align: center">
        <div class="d-flex" data-columns>
            @for($i=1;$i<=12;$i++)
                <div data-items="1" data-id="{{$i}}" data-column
                     style="width: 50px;height: 50px;background-color: #00a7d0;
                 border: 2px solid #00a65a;text-align: center;margin: auto">col-1
                </div>
                @if($i<12)
                    <div data-combine-columns>+</div>@endif
            @endfor
        </div>
        <div style="margin:25px 10px;">
            <a class="preview-page" data-generate-columns>Сгенерировать</a>
            <a class="preview-page" style="background-color: #f39c12" data-refresh-page>Обновить</a>
        </div>
    </div>

    <script src="/vendor/page_constructor/packages/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).on('click', '[data-refresh-page]', function () {
            location.reload();
        });
        $(document).on('click', '[data-combine-columns]', function () {
            let prev = $(this).prev();
            let next = $(this).next();
            let items = prev.data('items') + next.data('items');
            prev.width(prev.width() + next.width());
            prev.data('items', items);
            prev.html('col-' + items);
            $(this).remove();
            next.remove();
        })
        $(document).on('click', '[data-generate-columns]', function () {
            let columns = $(this).parents(['data-columns-selector']).find('[data-column]');
            console.log(columns);
            let items = [];
            let option_id = '{{$option->id ?? 0}}';
            columns.each(function (k, v) {
                items.push($(v).data('items'));
            })
            $.ajax({
                dataType: 'json',
                method: 'POST',
                cache: false,
                url: '/admin/constructor/cols',
                data: {items: items, option_id: option_id},
                success: function (data) {
                    if (data.success) location.reload();
                }
            });
        });
    </script>
    @endif

@endsection




