<div class="col-lg-{{$option->value ?? 12}}">
    @include ($pathOptionTop,['blockName' => 'col-lg-' . $option->value ?? 12 , 'hideClose' => false])
    @include ($pathOptionFullWidth)
    @include ($pathOptionBackGround)

    @if(isset($innerViews) && count($innerViews))
        @foreach($innerViews as $view)
            {!! $view !!}
        @endforeach
    @endif
    @include($pathConstructorOptions,['parentOptionId' => $option->id])
</div>





