@extends($pathOption,['showBackground'=>false,'showFullWidth'=>false])
@section('option_template')
    @include($pathPhotoUpload,['key'=>$keyPhotoOne,'resource'=>$resourceKeyPhotoOne])
    @include($pathResourceInput,['name' => 'Заглавие', 'key'=>$keyVarOne,'resource'=>$resourceKeyVarOne])
    @include($pathResourceInput,['name' => 'Описание','key'=>$keyVarTwo,'resource'=>$resourceKeyVarTwo])
    <div class="resources-block">
        <div class="resource-items">
            @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                @foreach($resourceKeyChildOne as $item)
                    @include($pathInputsBlockThree,['key'=>$keyChildOne,'resource'=>$item])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);"
                  data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildOne}}"
                  data-sub_view="{{$pathInputsBlockThree}}"
            >Добавить Блок</span>
        </div>
    </div>
@endsection







