@extends($pathOption,['showBackground'=>false,'showFullWidth'=>false])
@section('option_template')
    @include($pathResourceInput,['name' => 'Заглавие', 'key'=>$keyVarOne,'resource'=>$resourceKeyVarOne])
    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeySliderOne && count($resourceKeySliderOne))
                @foreach($resourceKeySliderOne as $item)
                    @include($pathSliderItem,['key'=>$keySliderOne,'resource'=>$item])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);"
                  data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keySliderOne}}"
                  data-sub_view="{{$pathSliderItem}}"
            >Добавить Аватар компании</span>
        </div>
    </div>
@endsection