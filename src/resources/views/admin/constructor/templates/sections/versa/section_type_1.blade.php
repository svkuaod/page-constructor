@extends($pathOption,['showBackground'=>false,'showFullWidth'=>false])
@section('option_template')
    @include($pathResourceInput,['name' => 'Заглавие', 'key'=>$keyVarOne,'resource'=>$resourceKeyVarOne])
    @include($pathResourceInput,['name' => 'Описание','key'=>$keyVarTwo,'resource'=>$resourceKeyVarTwo])
    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeyChildOne && count($resourceKeyChildOne))
                @foreach($resourceKeyChildOne as $item)
                    @include($pathInputsBlockThree,['key'=>$keyChildOne,'resource'=>$item,'subName' => 'кнопка'])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);" data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildOne}}" data-sub_view="{{$pathInputsBlockThree}}">Добавить Кнопку</span>
        </div>
    </div>
    <div class="resources-block">
        <div class="resource-items">
            @if($resourceKeySliderOne && count($resourceKeySliderOne))
                @foreach($resourceKeySliderOne as $item)
                    @include($pathSliderItem,['key'=>$keySliderOne,'resource'=>$item, 'subName' => 'картинка'])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);"
                  data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keySliderOne}}"
                  data-sub_view="{{$pathSliderItem}}"
            >Добавить Фото в сдайдер</span>
        </div>
    </div>
@endsection