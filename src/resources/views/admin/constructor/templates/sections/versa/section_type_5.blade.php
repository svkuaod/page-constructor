@extends($pathOption,['showBackground'=>false,'showFullWidth'=>false])
@section('option_template')
    @include($pathPhotoUpload,['key'=>$keyPhotoOne,'resource'=>$resourceKeyPhotoOne])
    @include($pathResourceInput,['name' => 'Заглавие', 'key'=>$keyVarOne,'resource'=>$resourceKeyVarOne])
    <div class="resources-block">
        <div class="resource-items">
            @if(isset($resourceKeyChildOne) && count($resourceKeyChildOne))
                @foreach($resourceKeyChildOne as $item)
                    @include($pathInputsBlockTwo,['key'=>$keyChildOne,'resource'=>$item])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);"
                  data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildOne}}"
                  data-sub_view="{{$pathInputsBlockTwo}}"
            >Добавить табик</span>
        </div>
    </div>
    <div class="resources-block">
        <div class="resource-items">
            @if(isset($resourceKeyChildTwo) && count($resourceKeyChildTwo))
                @foreach($resourceKeyChildTwo as $item)
                    @include($pathInputsBlockThree,['key'=>$keyChildTwo,'resource'=>$item])
                @endforeach
            @endif
        </div>
        <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToOption(this);"
                  data-option_id="{{$option->id ?? ''}}"
                  data-key="{{$keyChildTwo}}"
                  data-sub_view="{{$pathInputsBlockThree}}"
            >Добавить кнопку</span>
        </div>
    </div>
@endsection




