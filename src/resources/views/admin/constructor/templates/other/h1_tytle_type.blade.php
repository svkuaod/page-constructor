@extends($pathOption)
@section('option_template')
    @include($pathResourceInput,['name' => 'Title h1', 'key'=>$keyVarOne,'curResource'=>$resourceKeyVarOne])
    @include($pathResourceInput,['name' => 'Class', 'key'=>$keyVarTwo,'curResource'=>$resourceKeyVarTwo])
    @include($pathResourceSelect,['name' => 'Tag', 'key'=>$keyVarThree,'curResource'=>$resourceKeyVarThree,'items'=>['h1','h2','h3','h4']])
@endsection