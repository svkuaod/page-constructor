@php
    $pages = \Svkuaod\PageConstructor\Models\Constructor\Custom\SectionInfoPage::where('active',1)->with('info')->get();
@endphp
@extends($pathOption)
@section('option_template')
    @include($pathResourceSelect,['name' => 'Выберите инфостраницу', 'key'=>$keyInnerInfoPage,'curResource'=>$resourceKeyInnerInfoPage, 'items'=>$pages])
@endsection