<div class="col-md-12 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathResourceInput,['key'=>$keyVarTwo])
            @include($pathResourceInput,['key'=>$keyVarThree])
        </div>

        <div class="resources-block">
            <div class="resource-items">
                @if(isset($resource->childs) && count($resource->childs->where('key',$keyChildOne)))
                    @foreach($resource->childs->where('key',$keyChildOne)->sortBy('order') as $item)
                        @include($pathInputsBlockTwo,['resource'=>$item])
                    @endforeach
                @endif
            </div>
            <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToResource(this);"
                  data-resource_id="{{$resource->id ?? ''}}"
                  data-key="{{$keyChildOne}}"
                  data-sub_view="{{$pathInputsBlockTwo}}"
            >Добавить плашку</span>
            </div>
        </div>
        <div class="resources-block">
            <div class="resource-items">
                @if(isset($resource->childs) && count($resource->childs->where('key',$keyChildTwo)))
                    @foreach($resource->childs->where('key',$keyChildTwo)->sortBy('order') as $item)
                        <div class="col-xs-12">
                            @include($pathInputsBlockOne,['resource'=>$item])
                        </div>
                    @endforeach
                @endif

            </div>
            <div class="add-resource-block">
            <span class="add-resource" onclick="addResourceToResource(this);"
                  data-resource_id="{{$resource->id ?? ''}}"
                  data-key="{{$keyChildTwo}}"
                  data-sub_view="{{$pathInputsBlockOne}}"
            >Добавить Опцию</span>
            </div>
        </div>
        <div class="form-group option-resource-item">
            @include($pathResourceInput,['key'=>$keyVarFour])
            @include($pathResourceInput,['key'=>$keyVarFive])
            @include($pathResourceInput,['key'=>$keyVarSix])
        </div>
    </div>

</div>
