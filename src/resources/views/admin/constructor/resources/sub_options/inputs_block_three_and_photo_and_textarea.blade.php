<div class="col-sm-6 col-md-4 col-lg-3 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathResourceTextArea,['key'=>$keyVarTwo])
            @include($pathResourceInput,['key'=>$keyVarThree])
            @include($pathPhotoUpload,['key'=>$keyPhotoOne])
            @include($pathResourceInput,['key'=>$keyVarFour])
        </div>
    </div>
</div>
