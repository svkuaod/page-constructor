<div class="col-sm-12 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathResourceSelect,['key'=>$keyVarOne,'items' => $items ?? []])
        </div>
    </div>
</div>
