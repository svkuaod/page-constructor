<div class="col-sm-12 col-lg-6 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathResourceTextArea,['key'=>$keyVarTwo])
            @include($pathResourceInput,['key'=>$keyVarThree])
            @include($pathResourceInput,['key'=>$keyVarFour])
        </div>
    </div>
</div>
