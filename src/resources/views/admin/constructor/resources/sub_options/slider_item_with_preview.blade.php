<div class="col-md-3 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathPhotoUpload,['key'=>$keyPhotoOne])
            <div style="width: 50%">
                @include($pathPhotoUpload,['key'=>$keyPhotoTwo])
            </div>
        </div>
    </div>
</div>
