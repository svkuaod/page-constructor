<div class="col-sm-12 col-lg-6 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathPhotoUpload,['key'=>$keyPhotoOne])
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathResourceInput,['key'=>$keyVarTwo])
            @include($pathResourceTextArea,['key'=>$keyVarThree])
        </div>
    </div>
</div>
