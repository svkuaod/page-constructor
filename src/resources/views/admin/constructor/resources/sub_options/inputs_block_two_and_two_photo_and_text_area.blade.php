<div class="col-sm-12 col-lg-6 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathPhotoUpload,['type'=>'svg','key'=>$keyPhotoOne])
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathPhotoUpload,['key'=>$keyPhotoTwo])
            @include($pathResourceInput,['key'=>$keyVarTwo])
            @include($pathResourceTextArea,['key'=>$keyVarThree])
        </div>
    </div>
</div>
