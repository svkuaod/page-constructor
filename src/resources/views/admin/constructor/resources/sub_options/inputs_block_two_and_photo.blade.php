<div class="col-sm-6 col-md-4 col-lg-3 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathPhotoUpload,['key'=>$keyPhotoOne])
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathResourceInput,['key'=>$keyVarTwo])
        </div>
    </div>
</div>
