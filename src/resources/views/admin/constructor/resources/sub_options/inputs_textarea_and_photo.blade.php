<div class="col-sm-12 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathPhotoUpload,['key'=>$keyPhotoOne])
            @include($pathResourceInput,['key'=>$keyVarOne])
            @include($pathResourceTextArea,['key'=>$keyVarTwo])
        </div>
    </div>
</div>
