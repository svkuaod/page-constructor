<div class="col-md-4 resource-block">
    <div class="body">
        @include ($pathSubOptionTop)
        <div class="form-group option-resource-item">
            @include($pathPhotoUpload,['key'=>$keyPhotoOne])
            @include($pathResourceTextArea,['key'=>$keyVarOne])
        </div>
    </div>
</div>
