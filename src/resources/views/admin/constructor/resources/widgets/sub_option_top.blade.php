<div class="option-header">
    <div class="sort-control" data-resource_id="{{$resource->id ?? 0}}" data-key="{{$key ?? ''}}"
         data-option_id="{{$resource->option->id ?? 0}}">
        <i class="fa fa-arrow-up" aria-hidden="true" onclick="changeOrderResource(this,'up')"></i>
        <i class="fa fa-arrow-down" aria-hidden="true" onclick="changeOrderResource(this,'down')"></i>
    </div>
    <div class="title">{{$subName ?? ''}}</div>
    <div class="close"><span class="delete-resource" data-resource_id="{{$resource->id ?? 0}}"
                             onclick="deleteResource(this)"><i class="fa fa-close" aria-hidden="true"></i></span></div>
</div>
<div>
    <div class="show">
        <span>Отбражение</span>
        <label class="switch mb-3 ml-5" data-toggle="tooltip" title="Вкл/выключить отображение">
            <input type="checkbox" @if($resource->active) value="1" checked="checked" @else value="0"
                   @endif onchange="changeResourceStatus({{$resource->id ?? 0}})">
            <span class="slider round"></span>
        </label>
    </div>
</div>
