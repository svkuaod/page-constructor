@php($curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key))
<div class="constructor-file-upload-block">
    <div class="form-group constructor-block">
        <label>Загрузка Видео</label>
        <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-default btn-file-upload-constructor">
                            Browse… <input type="file" class="upload-file" onchange="upload_file_resource(this);"
                                           data-option_id="{{$option->id ?? ''}}"
                                           data-resource_id="{{$curResource->id ?? ''}}"
                                           data-prev_value="{{$curResource->info->value ?? ''}}"
                                           data-key="{{$key}}">
                        </span>
                    </span>
        </div>
        <video class="file-preview video-fluid z-depth-1 video-preview-block" autoplay loop controls muted
               style="width: 100%">
            <source type="video/mp4"
                    @if($curResource) src="{{isset($curResource->info) ? $curResource->info->valuePath : '/images/default-image.png'}}" @endif/>
        </video>
    </div>
</div>


