@php($curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key))
<div class="constructor-file-upload-block">
    <div class="form-group constructor-block">
        <div><label>{{$names[$key] ?? $name ?? 'Загрузка Картинки'}}</label></div>
        <div class="btn btn-primary upload-button upload-file" style="max-width: 200px;"
             onclick="startSelectPhoto(this)">
            <i class="fa fa-upload"></i>
            Выбор изображения
        </div>
        <input type="file" class="upload-file hidden" onchange="upload_file_resource(this);"
               data-option_id="{{$option->id ?? ''}}"
               data-resource_id="{{$curResource->id ?? ''}}"
               data-prev_value="{{$curResource->info->value ?? ''}}"
               data-key="{{$key}}"/>
        <div style="margin-top: 10px">
                <img class='file-preview'
                     @if($curResource) src="{{isset($curResource->info) ? $curResource->info->valuePath : '/vendor/page_constructor/templates/default.jpg'}}" @endif/>
        </div>
    </div>
</div>


