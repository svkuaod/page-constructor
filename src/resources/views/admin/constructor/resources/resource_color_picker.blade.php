@php($curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key))
<div class="form-item constructor-block">
    <label for="name">Выберите цвет:</label>
    <input type="color" class="form-control" onchange="changeResourceValue(this)"
           value="{{$curResource->info->value ?? ''}}"
           data-option_id="{{$option->id ?? 0}}"
           data-resource_id="{{$curResource->id ?? 0}}"
           data-key="{{$key ?? ''}}"/>
</div>