@php($curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key))
<div class="form-item constructor-block">
    <label for="name" class="control-label">{{$names[$key] ?? $name ?? 'Название элемента'}}</label>
    <select class="form-control" onchange="changeResourceValue(this);"
            data-option_id="{{$option->id ?? 0}}"
            data-resource_id="{{$curResource->id ?? 0}}"
            data-key="{{$key ?? ''}}" required>
            @if(isset($items) && count($items))
                <option value="">Выберите</option>
                @foreach($items as $item)
                    @php($id = $item->id ?? $item)
                    <option value="{{$id}}" @if(isset($curResource) && $curResource->info && $curResource->info->value == $id) selected @endif>{{$item->name ?? $item->info->name ?? $item ?? ''}}</option>
                @endforeach
            @endif
    </select>
</div>