@php($curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key))
<div class="form-item constructor-block">
    <label for="name" class="control-label">{{$names[$key] ?? $name ?? 'Название элемента'}}</label>
    <textarea class="form-control" name="text-type-field-{{$curResource->id ?? 0}}"
              data-key="{{$key ?? ''}}"
              data-option_id="{{$option->id ?? ''}}"
              data-resource_id="{{$curResource->id ?? ''}}"
              placeholder="{{$names[$key] ?? $name ?? 'Название элемента'}}">{!! $curResource->optionText->info->value ?? '' !!}
    </textarea>
</div>
<script src="/packages/sleepingowl/ckeditor/ckeditor.js"></script>
<script>
    @php( $timer_id = uniqid().rand(123,125424) )
    let timer_{{$timer_id}};
    setTimeout(function () {
        CKEDITOR.replace("text-type-field-{{$resource->id ?? 0}}", {
            filebrowserUploadUrl: '/admin/ckeditor/upload/image?_token=' + $('meta[name="csrf-token"]').attr('content'),
        }).on('change', function (event) {
            if (timer_{{$timer_id}}) clearTimeout(timer_{{$timer_id}});
            timer_{{$timer_id}} = setTimeout(function () {
                let editor = event.editor;
                let element = editor.element;
                let key = element.data('key');
                let option_id = element.data('option_id') * 1;
                let resource_id = element.data('resource_id') * 1;
                let value = editor.getData();
                $.ajax({
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    url: '/admin/constructor/change-resource-option-text-value',
                    data: {key: key, option_id: option_id, resource_id: resource_id, value: value},
                    success: function (data) {
                        element.data('resource_id', data.resource_id);
                    },
                });
            }, 1000);
        });
    }, 500)
</script>