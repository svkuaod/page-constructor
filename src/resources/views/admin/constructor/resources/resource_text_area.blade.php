@php($curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key))
<div class="form-item constructor-block">
    <label for="name" class="control-label">{{$names[$key] ?? $name ?? 'Название элемента'}}</label>
    <textarea class="form-control"
              placeholder="{{$names[$key] ?? $name ?? 'Название элемента'}}" onchange="changeResourceValue(this);"
              data-option_id="{{$option->id ?? 0}}"
              data-resource_id="{{$curResource->id ?? 0}}"
              data-key="{{$key ?? ''}}">{!! $curResource->info->value ?? '' !!}
    </textarea>
</div>