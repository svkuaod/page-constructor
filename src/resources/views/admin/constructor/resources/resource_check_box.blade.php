@php
$curResource = $curResource ?? Svkuaod\PageConstructor\Facade\Constructor::getChildResourceByKey($resource ?? null,$key ?? '');
$active = $curResource->info->value ?? null;
@endphp
<div class="form-item constructor-block">
    <label for="name" class="control-label">{{$names[$key] ?? $name ?? 'Название элемента'}}</label>
    <input type="checkbox" @if($active == 'on') checked value="off" @else value="on" @endif
           onchange="changeResourceValue(this);"
           data-option_id="{{$option->id ?? 0}}"
           data-resource_id="{{$curResource->id ?? 0}}"
           data-key="{{$key ?? ''}}">
</div>
