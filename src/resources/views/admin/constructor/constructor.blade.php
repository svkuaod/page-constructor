<input type="hidden" id="constructor_id" value="{{$model->id ?? ''}}">
<div class="construtor-container" id="construtor">
    @if($renderedTemplates && count($renderedTemplates))
        @foreach($renderedTemplates as $renderedTemplate)
            {!! $renderedTemplate !!}
        @endforeach
    @endif
</div>
@include('page_constructor::admin.constructor.options.constructor_options')
