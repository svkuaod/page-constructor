<div class="col-12">
        <textarea name="text-type-field-{{$option->id ?? ''}}" class="text-type-field"
                  data-option-id="{{$option->id ?? ''}}"
                  data-type="{{\Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText::KEY_TEXT}}">
            {!! $option->text->info->value ?? '' !!}
        </textarea>
</div>
<script src="/packages/sleepingowl/ckeditor/ckeditor.js"></script>
<script>
    @php( $timer_id = uniqid().rand(123,125424) )
    let timer_{{$timer_id}};
    setTimeout(function () {
            CKEDITOR.replace("text-type-field-{{$option->id ?? 0}}", {
                    filebrowserUploadUrl: '/admin/ckeditor/upload/image?_token=' + $('meta[name="csrf-token"]').attr('content'),
            }).on('change', function (e) {
                    if (timer_{{$timer_id}}) clearTimeout(timer_{{$timer_id}});
                    timer_{{$timer_id}} = setTimeout(function () {
                            var type = '{{\Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionText::KEY_TEXT}}'
                            var option_id = '{{$option->id ?? ''}}';
                            var text = e.editor.getData();
                            setTimeout(function () {
                                    $.ajax({
                                            type: 'POST',
                                            cache: false,
                                            dataType: 'json',
                                            url: '/admin/constructor/save-changes',
                                            data: {type: type, option_id: option_id, value: text},
                                            success: function (data) {
                                            },
                                    });
                            }, 1000);
                    }, 1000);
            });
    }, 500)
</script>