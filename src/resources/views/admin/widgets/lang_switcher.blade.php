<div style="margin: 10px">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button"
                data-toggle="dropdown">{{strtoupper(session('locale') ?? 'ru')}}
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            @if(isset($languages) && count($languages))
                @foreach($languages as $lang)
                    <li><a href="/admin/lang/{{$lang}}">{{strtoupper($lang)}}</a></li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
