<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'captcha' => ':attribute код не совпадает',
    'accepted' => ':attribute должен быть отмечен',
    'active_url' => ':attribute не является валидным URL',
    'after' => ':attribute должен быть датой после :date',
    'after_or_equal' => ':attribute должен быть датой после либо эквивалентным :date',
    'alpha' => ':attribute должен содержать только буквы',
    'alpha_dash' => ':attribute должен содержать только буквы, цирфы и дефисы',
    'alpha_num' => ':attribute должен содержать только буквы и цифры',
    'array' => ':attribute должен быть массивом',
    'before' => ':attribute должен быть датой перед :date',
    'before_or_equal' => ':attribute должен быть датой перед или эквивалентным :date',
    'between' => [
        'numeric' => ':attribute должен быть в диапазоне от :min до :max',
        'file' => ':attribute должен иметь размер от :min до :max килобайт',
        'string' => ':attribute должен содержать от :min до :max символов',
        'array' => ':attribute должен сожержать от :min до :max элементов',
    ],
    'boolean' => ':attribute поле должно содержать только true или false',
    'confirmed' => ':attribute не совпадает',
    'date' => ':attribute не является корректной датой',
    'date_format' => ':attribute не сооветствует формату :format',
    'different' => ':attribute и :other не должны содержать одинаковых значений',
    'digits' => ':attribute должен быть :digits цирфами',
    'digits_between' => ':attribute должен быть цифрами в диапазоне от :min до :max ',
    'dimensions' => ':attribute содержит некорректные параметры изображения',
    'distinct' => ':attribute поле содержит неуникальное значение',
    'email' => ':attribute должен быть корректным email адресом',
    'exists' => 'Выбранный :attribute уже существует',
    'file' => ':attribute должен быть файлом',
    'filled' => ':attribute поле обязательно',
    'image' => ':attribute должен быть изображением',
    'in' => 'Выбранный :attribute некорректен',
    'in_array' => ':attribute поле не существует в :other',
    'integer' => ':attribute должен быть целым числом',
    'ip' => ':attribute должен быть корректным IP адресом',
    'json' => ':attribute должен быть корректной JSON строкой',
    'max' => [
        'numeric' => ':attribute не должен быть больше чем :max',
        'file' => ':attribute не должен быть больше чем :max килобайт',
        'string' => ':attribute не должен быть больше чем :max символов',
        'array' => ':attribute не должен содержать больше чем :max элементов',
    ],
    'mimes' => ':attribute должен быть файлом типа: :values',
    'mimetypes' => ':attribute должен быть файлом типа: :values',
    'min' => [
        'numeric' => ':attribute должен быть минимум :min',
        'file' => ':attribute должен быть минимум :min килобайт',
        'string' => ':attribute должен быть минимум :min символов',
        'array' => ':attribute должен содержать минимум :min элементов',
    ],
    'not_in' => 'Выбранный :attribute не корректен',
    'numeric' => ':attribute должен быть числом',
    'present' => ':attribute должен быть отмечен',
    'regex' => ':attribute не соответствует формату',
    'required' => ':attribute обязательно',
    'required_if' => ':attribute обязательно, когда :other соответствует :value',
    'required_unless' => ':attribute обязательно, пока :other соответствует :values',
    'required_with' => ':attribute обязательно, когда :values отмечен',
    'required_with_all' => ':attribute обязательно, когда :values отмечены',
    'required_without' => ':attribute обязательно, когда :values не отмечен',
    'required_without_all' => ':attribute обязательно, когда ни один из :values отмечены',
    'same' => ':attribute и :other должны совпадать',
    'size' => [
        'numeric' => ':attribute должен быть размером :size',
        'file' => ':attribute должен быть :size килобай',
        'string' => ':attribute должен быть :size символов',
        'array' => ':attribute должен содержать :size элементов',
    ],
    'string' => ':attribute должен быть строкой',
    'timezone' => ':attribute должен быть корректной временной зоной',
    'unique' => ':attribute уже занят',
    'uploaded' => ':attribute невозможно загрузить',
    'url' => ':attribute некорректная ссылка',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [
        'email' => 'email',
        'name' => 'Имя',
        'address' => 'адрес',
        'city' => 'Город',
        'district' => 'район',
        'category' => 'категория',
        'sub-category' => 'подкатегория',
        'password' => 'пароль',
        'tel' => 'телефон',
        'captcha' => 'проверочный код',
        'password_confirmation' => 'подтверждение пароля',
        'tel_confirm' => 'код из sms',
        'vk' => 'vkontakte',
        'fb' => 'facebook',
        'yt' => 'youtube',
        'ig' => 'instagram',
        'site' => 'адрес сайта',
        'message' => 'Комментарий',
        'quantity' => 'Количество',
        'bank_name' => 'Банк-партнер',
        'account' => 'Р/с',
        'mfo' => 'МФО',
        'edrpoy' => 'ЕДРПОУ',
        'tiket' => 'Тикет',
        'ordered_balance' => 'Заказать на баланс',
        'ordered_referral' => 'Перенести на реферальные уровни',
        'date' => 'Дата',
        'driver_licence' => 'Наличие прав',
    ],
];
