<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'password' => 'Пароль должен быть длиной как минимум 6 символов и совпадать с подтверждением.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Ссылка на сброс пароля была выслана вам на email!',
    'token' => 'Данная ссылка для сброса пароля недействительна.',
    'user' => "Пользователя с данным email-адресом не существует.",
];
