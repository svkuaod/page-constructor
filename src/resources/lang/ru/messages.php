<?php
return [
    'successfully_submitted' => 'Заявка успешно отправлена',
    'successfully' => 'Успешно!',
    'you_can_add_review' => 'Вы можете оставить свой отзыв',
    'head_error' => 'Ошибка отправки',
    'body_error' => 'Ошибка отправки',
    'you_can_order_callback' => 'Вы можете заказать обратный звонок',
    'you_can_order_skype_cons' => 'Вы можете записаться на Skype консультацию',
    'school_security' => 'ШКОЛА ТЕЛОХРАНИТЕЛЕЙ «АНТИКИЛЛЕР»',
    'order_service' => 'Заказать Услугу',
];