<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

\Illuminate\Support\Facades\Auth::routes();

$languages = \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getLanguages() ?? ['ru'];
$customRoutes = \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getRoutes();

Route::prefix('/admin')->middleware(['web',
    \Svkuaod\PageConstructor\Middleware\IsAdminMiddleware::class
])->group(function () {
    Route::get('/lang/{lang}', function (\Illuminate\Http\Request $request) {
        if (!$request->lang) $request->lang = config('app.locale');
        session(['locale' => $request->lang]);
        return redirect()->back();
    });
    Route::prefix('constructor')
        ->namespace('\Svkuaod\PageConstructor\Controllers')
        ->group(function () {
            Route::post('/add-option', 'Constructor\AdminConstructorController@addOption');
            Route::post('/cols', 'Constructor\AdminConstructorController@generateColumns');
            Route::post('/delete-option', 'Constructor\AdminConstructorController@deleteOption');
            Route::post('/delete-resource', 'Constructor\AdminConstructorController@deleteResource');
            Route::post('/full-width-option', 'Constructor\AdminConstructorController@fullWidthOption');
            Route::post('/change-order-option', 'Constructor\AdminConstructorController@changeOrderOption');
            Route::post('/change-order-resource', 'Constructor\AdminConstructorController@changeOrderResource');
            Route::post('/change-option-style', 'Constructor\AdminConstructorController@changeOptionStyle');
            Route::post('/upload-file', 'Constructor\AdminConstructorController@uploadFile');
            Route::post('/change-resource-value', 'Constructor\AdminConstructorController@changeResourceValue');
            Route::post('/change-resource-option-text-value', 'Constructor\AdminConstructorController@changeResourceOptionTextValue');
            Route::post('/add-resource-to-option', 'Constructor\AdminConstructorController@addResourceToOption');
            Route::post('/add-resource-to-resource', 'Constructor\AdminConstructorController@addResourceToResource');
            Route::post('/save-changes', 'Constructor\AdminConstructorController@saveChangesAjax');
            Route::post('/active-resource', 'Constructor\AdminConstructorController@changeActiveResource');
            Route::post('/change-background-value', 'Constructor\AdminConstructorController@changeBackgroundValue');
            Route::post('/switch-active-option', 'Constructor\AdminConstructorController@switchActiveOption');
            Route::post('/replicate-info', 'Constructor\AdminConstructorController@copyInfoConstructorModelById');
        });
});

Route::middleware(['web'])->group(function () {
    Route::get('/home', function () {
        if (auth()->user()->is_admin) return redirect('/admin');
        return redirect('/');
    })->name('home');
    Route::get('/logout', function () {
        if (auth()->check()) auth()->logout();
        return redirect('/');
    });
});

$multiLangsRoutes = function () use($customRoutes) {
    Route::middleware([
        'web',
        \Svkuaod\PageConstructor\Middleware\ShareDataMiddleware::class,
        \Svkuaod\PageConstructor\Middleware\HeaderViewMiddleware::class,
        \Svkuaod\PageConstructor\Middleware\FooterViewMiddleware::class
    ])->group(function () use($customRoutes) {
        Route::get('/preview/{type}/{id}', '\Svkuaod\PageConstructor\Controllers\Constructor\AdminConstructorController@getPreview');
        if($customRoutes && count($customRoutes)){
            foreach ($customRoutes as $path=>$item){
                if(isset($item['where'])){
                    if($item['where']) Route::get($path, $item['controller'])->where($item['where'][0],$item['where'][1]);
                    else Route::get($path, $item['controller']);
                }
                else{
                    if($path === '/{url}') Route::get($path, $item)->where(['url' => '^(?!admin($|\/)).*$']);
                    else Route::get($path, $item);
                }
            }
        }
    });
};
foreach ($languages as $lang) {
    Route::prefix($lang)->middleware([
        \Svkuaod\PageConstructor\Middleware\LangMiddleware::class
    ])->group(function () use ($multiLangsRoutes) {
        $multiLangsRoutes();
    });
}
$multiLangsRoutes();

Route::middleware([
    'throttle:30,1',
])->group(function (){
    Route::post(Svkuaod\PageConstructor\Models\Subscribers\Subscriber::getFormPath(), '\Svkuaod\PageConstructor\Controllers\Subscribers\SubscriberController@addSubscriber');
    Route::post(\Svkuaod\PageConstructor\Models\FeedBack\FeedBackBase::getControllerActionPath() . '{key}', '\Svkuaod\PageConstructor\Controllers\FeedBack\FeedBackController@store');
    Route::post(\Svkuaod\PageConstructor\Models\Reviews\ReviewPublic::getFormPath(), '\Svkuaod\PageConstructor\Controllers\Reviews\ReviewController@addPublicReview');
});








