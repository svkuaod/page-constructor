<?php

namespace Svkuaod\PageConstructor;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Svkuaod\PageConstructor\Commands\InstallCms;

class PageConstructorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'page_constructor');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'page_constructor');
        $this->publishes([
            __DIR__ . '/config/page_constructor.php' => config_path('page_constructor.php'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/views/layouts' => resource_path('views/vendor/page_constructor/layouts'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/views/services' => resource_path('views/vendor/page_constructor/services'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/views/admin' => resource_path('views/vendor/page_constructor/admin'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/images' => public_path('vendor/page_constructor/images'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/css' => public_path('vendor/page_constructor/css'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/js' => public_path('vendor/page_constructor/js'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/fonts' => public_path('vendor/page_constructor/fonts'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/packages' => public_path('vendor/page_constructor/packages'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/page_constructor'),
        ]);
    }

}
