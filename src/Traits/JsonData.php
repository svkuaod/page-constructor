<?php

namespace Svkuaod\PageConstructor\Traits;

use Svkuaod\PageConstructor\Components\Services\StoreFile;
use Illuminate\Http\File;

trait JsonData
{
    /**
     * @var array
     */
    protected $dataArray = [];

    protected function getDataArray(){
        if(!$this->data) return [];
        if(!$this->dataArray) $this->dataArray = json_decode($this->data,true);
        return $this->dataArray;
    }

    public function getDataField($field) {
        return $this->getDataArray()[$field] ?? null;
    }

    protected function setDataField($field, $value = null){
        $this->dataArray[$field] = $value;
        $this->data = json_encode($this->dataArray);
    }

    protected function setPhotoToData($field, $value = null, $folder = null)
    {
        if(strpos($value,'/storage/') !== false) return $this->setDataField($field,str_replace('/storage/','', $value));
        $dataPhoto = $this->getDataField($field);
        if(strpos($value,'/storage/') === 0) $value = str_replace('/storage/','',$value);
        if ($dataPhoto == $value || $value == StoreFile::$defaultImagePath) return;
//        if(!isset($this->attributes['id'])) $this->save();
        StoreFile::deleteFileByPath($dataPhoto);
        if(!$value) return $this->setDataField($field, null);
        if(!$folder) $folder = $this->table . '/' . $this->attributes['id'];
        $file = new File($value);
        if($file && $file->getExtension() === 'svg') $path = StoreFile::getStoredSvgFilePath($folder, $file);
        else $path = StoreFile::getStoredFilePath($folder, $file);
        \Illuminate\Support\Facades\File::delete($value);
        $this->setDataField($field, $path);
    }
}
