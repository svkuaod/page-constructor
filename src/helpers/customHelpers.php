<?php
function getLink(?string $url)
{
    if(!$url) return isMainPage() ? "#" : '/';
    if(strpos($url,'#') === 0) return $url;
    if(app()->getLocale('locale') && app()->getLocale('locale') !== \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getDefaultLanguage()){
        $suffix = $url ? '/' . $url : '';
        return '/' . app()->getLocale('locale') . $suffix;
    }
    if(substr($url,0,1) === '/') return $url;
    return '/' . $url;
}
function breadcrumbs(string $view = null)
{
    return Svkuaod\PageConstructor\Models\Constructor\PublicBreadCrumbs::getInstance($view);
}
function getLangSwitcherLink($prefix){
    if($prefix === app()->getLocale()) return '#';
    $langs = \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getLanguages();
    $path = explode('/',request()->getPathInfo());
    if(isset($path[0])) array_shift($path);
    if(isset($path[0]) && in_array($path[0],$langs)) array_shift($path);
    $res = ($prefix === \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getDefaultLanguage()) ? '' :  '/' . $prefix;
    foreach ($path as $key=>$value){
        if(!$value) continue;
        $res .= '/' . $value;
    }
    return $res ? $res : '/';
}
function getCurrentMenuItemPath(){
    $langs = \Svkuaod\PageConstructor\Components\ConstructorConfig::getInstance()->getLanguages();
    $path = explode('/',request()->getPathInfo());
    if(isset($path[0])) array_shift($path);
    if(isset($path[0]) && in_array($path[0],$langs)) array_shift($path);
    return $path[0] ?? '';
}
function isMainPage(){
    return request()->path() === '/';
}
function getMainPageLink(){
    if(isMainPage()) return '#';
    return getLink('/');
}
