<?php

namespace Svkuaod\PageConstructor\Middleware;

use Closure;
use Svkuaod\PageConstructor\Facade\Constructor;
use Svkuaod\PageConstructor\Models\Constructor\Custom\HeaderPage;

class HeaderViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('header_page', Constructor::getRenderedPageByKey(HeaderPage::getPageKey()));
        return $next($request);
    }
}
