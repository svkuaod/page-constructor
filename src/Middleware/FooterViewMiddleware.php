<?php

namespace Svkuaod\PageConstructor\Middleware;

use Closure;
use Svkuaod\PageConstructor\Facade\Constructor;
use Svkuaod\PageConstructor\Models\Constructor\Custom\FooterPage;

class FooterViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('footer_page', Constructor::getRenderedPageByKey(FooterPage::getPageKey()));
        return $next($request);
    }
}
