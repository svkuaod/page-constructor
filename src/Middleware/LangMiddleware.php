<?php

namespace Svkuaod\PageConstructor\Middleware;

use Closure;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = $request->route()->getPrefix();
        if ($lang && substr($lang, 0, 1) == '/') $lang = substr_replace($lang, '', 0, 1);
        if ($lang) {
            session(['locale' => $lang]);
            app()->setLocale($lang);
        } else session(['locale' => app()->getLocale()]);

        return $next($request);
    }
}
