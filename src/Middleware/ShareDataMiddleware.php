<?php

namespace Svkuaod\PageConstructor\Middleware;

use Closure;
use Svkuaod\PageConstructor\Components\ShareData\ShareData;

class ShareDataMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('shareData', ShareData::getInstance());
        return $next($request);
    }
}
