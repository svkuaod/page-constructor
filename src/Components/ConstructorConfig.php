<?php

namespace Svkuaod\PageConstructor\Components;

use Illuminate\Support\Facades\Config;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;

final class ConstructorConfig
{
    private static $instance;

    private $vendorKey;
    private $configFileName;
    private $template;
    private $layoutsPath;
    private $layoutsTemplatePath;
    private $appPath;
    private $publicFolderPath;
    private $adminConstructorPath;
    private $configs = [];
    private $publicPaths = [];
    private $adminPaths = [];
    private $resourcePaths = [];
    private $languages = [];
    private $routes = [];

    private function __construct(){
        $this->vendorKey = 'page_constructor';
        $this->configFileName = $this->vendorKey;
        $this->configs = Config::get($this->configFileName);
        $this->template = $this->configs['template'] ?? 'base';
        $this->layoutsPath = $this->vendorKey . '::layouts';
        $this->layoutsTemplatePath = $this->layoutsPath . '.' . $this->template;
        $this->appPath = $this->layoutsTemplatePath . '.app';
        $this->publicFolderPath = $this->layoutsTemplatePath . '.constructor';
        $this->adminConstructorPath = $this->vendorKey . '::admin.constructor';
    }

    public static function getInstance() : self {
        return self::$instance ?? new self();
    }

    /**
     * @return string
     */
    public function getVendorKey(): string{
        return $this->vendorKey;
    }

    /**
     * @return string
     */
    public function getTemplateFolderPath(): string{
        return $this->layoutsTemplatePath;
    }

    /**
     * @return string
     */
    public function getTemplate(): string{
        return $this->template;
    }

    /**
     * @return string
     */
    public function getAppPath(): string{
        return $this->appPath;
    }

    /**
     * @return string
     */
    public function getPublicFolderPath(): string{
        return $this->publicFolderPath;
    }

    /**
     * @return string
     */
    public function getAdminConstructorPath(): string{
        return $this->adminConstructorPath;
    }

    /**
     * @return array
     */
    public function getConfigs(): array{
        return $this->configs;
    }

    /**
     * @return string
     */
    public function getKeyInnerInfoPage(): ?string {
        return $this->configs['resources_keys']['single_items']['keyInnerInfoPage'] ?? null;
    }

    /**
     * @return string
     */
    public function getStorageDisc(): string {
        return $this->configs['storage_disc'] ?? 'public';
    }

    public function getImageSizesArr(){
        $imageSizes = $this->configs['image_sizes'] ?? null;
        return is_array($imageSizes) ? $imageSizes : null;
    }    
    
    public function getSettingsKeysArr(){
        $settingsKeys = $this->configs['settings_keys'] ?? null;
        return is_array($settingsKeys) ? $settingsKeys : null;
    }

    /**
     *
     * @return integer
     */
    public function getImageDefaultSize(): int {
        return $this->getImageSizesArr()['default'] ?? 1920;
    }

    /**
     * @return string
     */
    public function getImagePrefixes(): array {
        $arr = $this->getImageSizesArr() ?? [];
        if($arr && isset($arr['default'])) unset($arr['default']);
        return array_keys($arr);
    }

    /**
     * @return string
     */
    public function getServiceMailPath(): string{
        return $this->vendorKey . '::services.mail';
    }

    /**
     * @return string
     */
    public function getDefaultLanguage(): string{
        return $this->configs['default_language'] ?? 'ru';
    }

    public function getPublicPaths() : array{
        if(empty($this->publicPaths)) $this->publicPaths = [
            'pathApp' => $this->appPath,
            'pathMainView' => $this->vendorKey . '::layouts.main',
            'pathHeader' => $this->vendorKey . '::layouts.' . $this->template . '.widgets.header',
            'pathFooter' => $this->vendorKey . '::layouts.' . $this->template . '.widgets.footer',
        ];
        return $this->publicPaths;
    }

    public function getAdminPaths() : array{
        if(empty($this->adminPaths) && !empty($this->configs['admin_views_paths'])){
            foreach ($this->configs['admin_views_paths'] as $key => $item) {
                $this->adminPaths[$key] = $item;
            }
        }
        return $this->adminPaths;
    }

    public function getLanguages() : array{
        if(empty($this->languages) && is_array($this->configs['languages'])){
            $this->languages = $this->configs['languages'];
        }
        return $this->languages;
    }

    public function getRoutes() : array{
        if(empty($this->routes) && is_array($this->configs['routes'])){
            $this->routes = $this->configs['routes'];
        }
        return $this->routes;
    }

    public function getResourceKeys() : array{
        if(empty($this->resourcePaths) && !empty($this->configs['resources_keys'])){
            foreach ($this->configs['resources_keys'] as $type) {
                foreach ($type as $key => $item) {
                    $this->resourcePaths[$key] = $item;
                }
            }
        }
        return $this->resourcePaths;
    }

    public function getResourceVariables(ConstructorOption $option) : array{
        $resourceArray = [];
        $resourceKeys = $this->configs['resources_keys'] ?? null;
        if (!isset($option->resources) || !$resourceKeys || !is_array($resourceKeys)) return $resourceArray;

        foreach ($resourceKeys as $keyType => $type) {
            foreach ($type as $key => $item) {
                switch ($keyType) {
                    case 'single_items':
                        $resourceArray['resource' . ucfirst($key)] = $option->resources->where('key', $item)->first();
                        break;
                    case 'array_items':
                        $resourceArray['resource' . ucfirst($key)] = $option->resources->where('key', $item);
                        break;
                }
            }
        }
        return $resourceArray;
    }

}
