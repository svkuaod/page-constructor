<?php

namespace Svkuaod\PageConstructor\Components\SEO;

use Illuminate\Database\Eloquent\Model;
use Svkuaod\PageConstructor\Components\Services\CheckImplementing;
use AdminFormElement;

trait SeoableAdmin
{
    protected function getSeoFields(Model $model){
        if(!CheckImplementing::isImplementing(get_class($model),SeoableInterface::class)) return null;
        return [
            AdminFormElement::text('seo.meta_title', 'Title'),
            AdminFormElement::text('seo.meta_keywords', 'KeyWords'),
            AdminFormElement::textarea('seo.meta_description', 'Description'),
            AdminFormElement::ckeditor('seo.meta_text', 'SeoText'),
        ];
    }
}