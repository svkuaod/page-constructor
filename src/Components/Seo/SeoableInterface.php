<?php

namespace Svkuaod\PageConstructor\Components\SEO;


interface SeoableInterface
{
    public function seo();
    public function getSeoData() : array;
}