<?php

namespace Svkuaod\PageConstructor\Components\SEO;

use Svkuaod\PageConstructor\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class SEO extends BaseModel
{
    const TABLE = 'seo';
    protected $table = self::TABLE;

    private $infoModel;

    public function getInfoModel() : ?SEOInfo{
        return $this->infoModel;
    }

    protected $fillable = ['resource_id','resource_type'];

    protected static function boot()
    {
        parent::boot();
        static::saved(function (Model $model){
            if($info = $model->getInfoModel()) $info->save();
        });
    }

    public function getMetaTitleAttribute(){
        return $this->info->meta_title ?? '';
    }

    public function getMetaKeyWordsAttribute(){
        return $this->info->meta_keywords ?? '';
    }

    public function getMetaDescriptionAttribute(){
        return $this->info->meta_description ?? '';
    }

    public function getMetaTextAttribute(){
        return $this->info->meta_text ?? '';
    }

    public function setMetaTitleAttribute($value){
        $this->setInfoField('meta_title',$value);
    }

    public function setMetaKeyWordsAttribute($value){
        $this->setInfoField('meta_keywords',$value);
    }

    public function setMetaDescriptionAttribute($value){
        $this->setInfoField('meta_description',$value);
    }

    public function setMetaTextAttribute($value){
        $this->setInfoField('meta_text',$value);
    }

    public function setUrlAttribute($value){
        if(substr($value,0,1) === '/') $this->attributes['url'] = substr($value,1);
        else $this->attributes['url'] = $value;
    }

    public function getResourceInfoNameAttribute(){
        return $this->resource->info->name ?? null;
    }

    public function getResourceUrlAttribute(){
        return $this->resource->url ?? null;
    }

    public function resource()
    {
        return $this->morphTo();
    }

    public function info(){
        return $this->hasOne(SEOInfo::class,'seo_id','id');
    }

    private function setInfoField($field,$value){
        if(!$value) return null;
        if(!$this->infoModel) {
            if(!isset($this->attributes['id'])) $this->save();
            $this->infoModel = $this->info ?? new SEOInfo();
            $this->infoModel->seo_id = $this->attributes['id'];
        }
        $this->infoModel->$field = $value;
    }

}