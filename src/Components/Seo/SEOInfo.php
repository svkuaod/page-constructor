<?php

namespace Svkuaod\PageConstructor\Components\SEO;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class SEOInfo extends BaseInfoModel
{
    protected $fillable = ['seo_id'];

    protected static function getBaseTable()
    {
        return SEO::TABLE;
    }
}
