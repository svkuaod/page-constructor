<?php

namespace Svkuaod\PageConstructor\Components\SEO;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\Descriptions\DescriptionableInterface;

class StructureData
{
    public static function setStructureData(DescriptionableInterface $model,$view = 'news_article'){
        $viewPath = ConstructorConfig::getInstance()->getVendorKey() . '::services.structure_data.' . $view;
        $data = view($viewPath,['model' => $model])->render();
        view()->share('structure_data',$data);
    }
}