<?php

namespace Svkuaod\PageConstructor\Components\SEO;

trait Seoable
{
    public function seo(){
        return $this->morphOne(SEO::class,'resource');
    }

    public function getSeoData() : array {
        $seo = $this->seo ?? SEO::where('url',request()->path())->with('info')->first();
        if(!$seo) return [];
        return [
            'meta_title' => $seo->meta_title ?? '',
            'meta_keywords' => $seo->meta_keywords ?? '',
            'meta_description' => $seo->meta_description ?? '',
            'meta_text' => $seo->meta_text ?? '',
        ];
    }
}