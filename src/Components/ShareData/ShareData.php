<?php

namespace Svkuaod\PageConstructor\Components\ShareData;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Blocks\Block;
use Svkuaod\PageConstructor\Models\Blocks\Custom\SocialBlock;
use Svkuaod\PageConstructor\Models\Menu\Menu;
use Svkuaod\PageConstructor\Models\Translates\Translate;
use Illuminate\Database\Eloquent\Collection;

final class ShareData
{
    /**
     * @var ShareData
     */
    private static $instance;

    /**
     * @var Collection
     */
    public $translates;

    /**
     * @var Collection
     */
    public $headerMenu;

    /**
     * @var Collection
     */
    public $footerMenu;

    /**
     * @var array
     */
    public $languages;

    /**
     * @var Collection
     */
    public $socials;

    /**
     * @var string
     */
    public $currentLanguage;

    public function __construct()
    {
        $this->currentLanguage = app()->getLocale('locale');
        $this->languages = ConstructorConfig::getInstance()->getLanguages();
        $this->translates = Translate::where('active', 1)->with('info')->get()->pluck('info.value', 'key');
        $menu = Menu::where('active', 1)->with('info')->orderBy('order')->get()->groupBy('key');
        $this->headerMenu = $menu->get('header_menu') ?? new Collection();
        $this->footerMenu = $menu->get('header_menu') ?? new Collection();
        $this->socials = SocialBlock::where('active', 1)->with('info')->orderBy('order')->get();
    }

    /**
     * @return ShareData
     */
    public static function getInstance()
    {
        if (!self::$instance) self::$instance = new ShareData();
        return self::$instance;
    }

    public function appendData($key, $value)
    {
        $this->$key = $value;
    }


}
