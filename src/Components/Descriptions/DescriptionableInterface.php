<?php

namespace Svkuaod\PageConstructor\Components\Descriptions;

interface DescriptionableInterface
{
    public function description();
}