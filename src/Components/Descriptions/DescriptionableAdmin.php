<?php

namespace Svkuaod\PageConstructor\Components\Descriptions;

use Illuminate\Database\Eloquent\Model;
use Svkuaod\PageConstructor\Components\Services\CheckImplementing;
use AdminFormElement;

trait DescriptionableAdmin
{
    protected function getDescriptionFields(Model $model){
        if(!CheckImplementing::isImplementing(get_class($model),DescriptionableInterface::class)) return null;
        return [
            AdminFormElement::text('description.name', 'Название'),
            AdminFormElement::text('description.body_m', 'Краткое описание'),
            AdminFormElement::textarea('description.body', 'Описание'),
            AdminFormElement::image('description.photoPath', 'Фото'),
            AdminFormElement::text('description.videoYoutube', 'Видео'),
            AdminFormElement::datetime('description.date', 'custom date'),
        ];
    }
}