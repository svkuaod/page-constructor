<?php

namespace Svkuaod\PageConstructor\Components\Descriptions;

use Svkuaod\PageConstructor\Components\Services\StoreFile;
use Svkuaod\PageConstructor\Models\BaseModel;
use Illuminate\Http\File;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Description extends BaseModel
{
    const TABLE = 'descriptions';
    protected $table = self::TABLE;

    private $infoModel;

    protected $fillable = ['resource_id','resource_type'];

    protected static function boot()
    {
        parent::boot();
        static::saved(function (Model $model){
            if($info = $model->getInfoModel()) $info->save();
        });
    }

    public function getNameAttribute(){
        return $this->info->name ?? '';
    }

    public function getBodyMAttribute(){
        return $this->info->body_m ?? '';
    }

    public function getBodyAttribute(){
        return $this->info->body ?? '';
    }

    public function setNameAttribute($value){
        $this->setInfoField('name',$value);
    }

    public function setBodyMAttribute($value){
        $this->setInfoField('body_m',$value);
    }

    public function setBodyAttribute($value){
        $this->setInfoField('body',$value);
    }

    public function getInfoModel() : ?DescriptionInfo{
        return $this->infoModel;
    }

    public function getPhotoPathAttribute(){
        return StoreFile::getStoragePath($this,'photo');
    }

    public function setPhotoPathAttribute($value)
    {
        if ($this->photoPath == $value) return true;
        if(!isset($this->attributes['id'])) $this->save();
        StoreFile::deleteFileByPath($this->photo);
        if(!$value) return true;
        $folderPath = StoreFile::getMainPhotoFolder($this);
        $file =  new File($value);
        if($file && $file->getExtension() === 'svg') $this->photo = StoreFile::getPathStoredSvgFile($folderPath, $file);
        else $this->photo = StoreFile::getStoredFilePath($folderPath, $file);
    }

    public function setVideoYoutubeAttribute($value)
    {
        $regexRule = "/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/";
        preg_match($regexRule,$value,$matches);
        if(is_array($matches) && count($matches)>0) $this->video = last($matches);
        else $this->video = '';
    }

    public function getVideoYoutubeAttribute()
    {
        return $this->video;
    }

    public function info(){
        return $this->hasOne(DescriptionInfo::class,'descriptions_id','id');
    }

    public function resource()
    {
        return $this->morphTo();
    }

    private function setInfoField($field,$value){
        if(!$this->infoModel) {
            if(!isset($this->attributes['id'])) $this->save();
            $this->infoModel = $this->info ?? new DescriptionInfo();
            $this->infoModel->descriptions_id = $this->attributes['id'];
        }
        $this->infoModel->$field = $value;
    }



}