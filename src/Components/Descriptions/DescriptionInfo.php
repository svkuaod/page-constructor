<?php

namespace Svkuaod\PageConstructor\Components\Descriptions;

use Svkuaod\PageConstructor\Models\BaseInfoModel;

class DescriptionInfo extends BaseInfoModel
{
    protected $fillable = ['descriptions_id'];

    protected static function getBaseTable()
    {
        return Description::TABLE;
    }
}
