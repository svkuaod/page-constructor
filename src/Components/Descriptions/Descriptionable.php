<?php

namespace Svkuaod\PageConstructor\Components\Descriptions;

trait Descriptionable
{
    public function description(){
        return $this->morphOne(Description::class,'resource');
    }
}