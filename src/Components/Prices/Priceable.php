<?php

namespace Svkuaod\PageConstructor\Components\Prices;

trait Priceable
{
    public function price(){
        return $this->morphOne(Price::class,'resource');
    }
}