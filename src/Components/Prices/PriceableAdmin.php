<?php

namespace Svkuaod\PageConstructor\Components\Prices;

use Illuminate\Database\Eloquent\Model;
use Svkuaod\PageConstructor\Components\Services\CheckImplementing;
use AdminFormElement;
use Svkuaod\PageConstructor\Components\Services\PriceService;

trait PriceableAdmin
{
    protected function getPriceFields(Model $model){
        if(!CheckImplementing::isImplementing(get_class($model),PriceableInterface::class)) return null;
        return [
            AdminFormElement::text('price.value', 'Цена'),
        ];
    }
}