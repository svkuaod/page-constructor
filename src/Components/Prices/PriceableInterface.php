<?php

namespace Svkuaod\PageConstructor\Components\Prices;


interface PriceableInterface
{
    public function price();
}