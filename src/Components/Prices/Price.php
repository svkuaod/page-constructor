<?php

namespace Svkuaod\PageConstructor\Components\Prices;

use Svkuaod\PageConstructor\Models\BaseModel;

class Price extends BaseModel
{
    const TABLE = 'priceable';
    protected $table = self::TABLE;

    protected $fillable = ['resource_id','resource_type'];

    public function resource()
    {
        return $this->morphTo();
    }
}