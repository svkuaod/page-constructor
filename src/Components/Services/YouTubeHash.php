<?php

namespace Svkuaod\PageConstructor\Components\Services;

final class YouTubeHash
{
    public static function getYoutubeHash($value){
        if(!$value) return null;
        if(strlen($value) === 11) return $value;
        $regexRule = "/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/";
        preg_match($regexRule,$value,$matches);
        if(is_array($matches) && count($matches)>0) return last($matches);
        else return null;
    }
}