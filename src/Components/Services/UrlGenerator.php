<?php

namespace Svkuaod\PageConstructor\Components\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

final class UrlGenerator
{
    public static function getUniqueUrl($url, $name, $id, $model)
    {
        if ($url) $url = self::slug($url, '-');
        else $url = self::slug($name, '-');
        return self::createUrl($url,$id,$model);
    }

    private static function createUrl($url, $id, Model $model)
    {
        $check = $model::select('url')->where('url', 'like', $url)->where('id', '!=', $id)->first();
        if ($check) {
            $url = self::createUrl($url . '-' . $id, $id, $model);
        }
        return $url;
    }

    public static function slug($title, $separator = '-', $language = 'en')
    {
        $title = $language ? Str::ascii($title, $language) : $title;

        // Convert all dashes/underscores into separator
        $flip = $separator === '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

        // Replace @ with the word 'at'
        $title = str_replace('@', $separator.'at'.$separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s\/\-]+!u', '', Str::lower($title));

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![\s]+!u', $separator, $title);

        return trim($title);
    }
}