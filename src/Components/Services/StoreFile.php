<?php

namespace Svkuaod\PageConstructor\Components\Services;

use http\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Svkuaod\PageConstructor\Components\ConstructorConfig;

final class StoreFile
{
    public static $defaultImagePath = '/images/default_image.png';

    private function __construct(){}

    public static function getPathStoredFileAs($filePath, $fileName, $file)
    {
        $img = $file ? self::getImageFile($file) : null;
        if (!$img) return null;
        $path = self::getStoredResizedImagePath($img,$filePath . $fileName,ConstructorConfig::getInstance()->getImageDefaultSize());
        foreach (ConstructorConfig::getInstance()->getImagePrefixes() as $prefix){
            $size = ConstructorConfig::getInstance()->getImageSizesArr()[$prefix] ?? null;
            if(!$size || !is_int($size)) continue;
            self::getStoredResizedImagePath($img,$filePath . $prefix . $fileName,$size);
        }
        return $path;
    }

    public static function getStoredFilePath($folderPath, $file = null)
    {
        if ($file) return Storage::disk(self::getStorageDisc())->putFile($folderPath, $file);
        else return null;
    }

    public static function deleteAllFilesByPath($filePath)
    {
        self::deleteFileByPath($filePath);
        $imgArr = explode('/',$filePath);
        $fileName = array_pop($imgArr);
        foreach (ConstructorConfig::getInstance()->getImagePrefixes() as $prefix){
            self::deleteFileByPath(implode('/',$imgArr) . '/' . $prefix . $fileName);
        }
    }

    public static function deleteFileByPath($filePath)
    {
        if (Storage::disk(self::getStorageDisc())->exists($filePath)) {
            Storage::disk(self::getStorageDisc())->delete($filePath);
        }
    }

    public static function getStoragePath(Model $model,$field = 'photo', $prefix = null){
        if (!isset($model->$field)) return 'vendor/page_constructor/images/default_image.png';
        if ($prefix && file_exists('storage/' . $prefix . $model->$field)) return '/storage/' . $prefix . $model->$field;
        if (file_exists('storage/' . $model->$field)) return '/storage/' . $model->$field;
        return file_exists($model->$field) ? '/' . $model->$field : $model->$field;
    }

    public static function getDataStoragePath($path = null, $prefix = null){
        if(!$path) return self::$defaultImagePath;
        if ($prefix && file_exists('storage/' . $prefix . $path)) return '/storage/' . $prefix . $path;
        if (file_exists('storage/' . $path)) return '/storage/' . $path;
        return file_exists($path) ? '/' . $path : self::$defaultImagePath;
    }

    public static function getMainPhotoFolder(Model $model = null){
        if(!$model) return 'all';
        return $model->getTable() . '/' . ($model->id ?? 'all');
    }

    public static function getAdditionalPhotoFolder(Model $model = null){
        return self::getMainPhotoFolder($model) . '/more';
    }

    private static function getStorageDisc(){
        return ConstructorConfig::getInstance()->getStorageDisc();
    }

    public static function saveFileByField($model,$field,$value){
        if(static::getStoragePath($model,$field) == $value) return true;
        self::deleteFileByPath($model->$field);
        if($value) $model->$field = self::getStoredFilePath($model->folderPath . ($model->id ?? 'new'), new File($value));
    }


    public static function getImageWithPrefix($imagePath, $prefix = null)
    {
        if(!$prefix) return $imagePath;
        $path = explode('/', $imagePath);
        $fileName = $prefix . array_pop($path);
        return implode('/', $path) . '/' . $fileName;
    }

    public static function storeFileToStorage($filePath,$file){
        if(substr($filePath,-1) ==='/') $filePath = substr($filePath,0,-1);
        return Storage::disk(self::getStorageDisc())->put($filePath,$file);
    }

    public static function getStoredResizedImagePath(\Intervention\Image\Image $img,$filePath, $width = 1920)
    {
        if($img->width() > $width){
            $img->resize($width, null, function ($i) {
                $i->aspectRatio();
            });
            $img->orientate();
        }
        $img->stream();
        Storage::disk(self::getStorageDisc())->put($filePath,$img);
        return $filePath;
    }

    public static function getImageFile($image)
    {
        try {
            $img = \Intervention\Image\Facades\Image::make($image);
        } catch (Exception $e) {
            \Illuminate\Support\Facades\Log::debug($e->getMessage());
            $img = null;
        }
        return $img;
    }

    public static function getSvgPath($path){
        if(strpos($path,'/storage/') === 0) $path = substr($path,1);
        $path = @file_get_contents($path);
        if(strpos($path,'<svg') !== false) return $path;
        return null;
    }

    public static function getPathStoredSvgFile($filePath,$file){
        $fileExtension = '';
        if($file instanceof File) $fileExtension = $file->getExtension();
        elseif ($file instanceof UploadedFile) $fileExtension = $file->getClientOriginalExtension();
        else $fileExtension = null;
        if(!$fileExtension || !file_exists($file) || $fileExtension != 'svg') return null;
        $svgPath = self::storeFileToStorage($filePath, $file);
        //Storage save svg as txt
        if(pathinfo($svgPath,PATHINFO_EXTENSION ) !== $fileExtension){
            $info = pathinfo($svgPath);
            $newFileName = $info['dirname'] . '/' .  $info['filename'] . '.' . $fileExtension;
            $res = rename('storage/' . $svgPath, 'storage/' . $newFileName);
            $svgPath = $res ? $newFileName : $svgPath;
        }
        unlink($file);
        return $svgPath;
    }

    public static function getStoredSvgFilePath($filePath,$file){
        if($file instanceof File) $fileExtension = $file->getExtension();
        elseif ($file instanceof UploadedFile) $fileExtension = $file->getClientOriginalExtension();
        else $fileExtension = null;
        if(!$fileExtension || !file_exists($file) || $fileExtension != 'svg') return null;
        $svgPath = self::storeFileToStorage($filePath, $file);

        if(pathinfo($svgPath,PATHINFO_EXTENSION ) !== $fileExtension){
            $info = pathinfo($svgPath);
            $newFileName = $info['dirname'] . '/' .  $info['filename'] . '.' . $fileExtension;
            $res = rename('storage/' . $svgPath, 'storage/' . $newFileName);
            $svgPath = $res ? $newFileName : $svgPath;
        }
        unlink($file);
        return $svgPath;
    }

}
