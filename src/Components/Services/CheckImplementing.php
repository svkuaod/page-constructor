<?php

namespace Svkuaod\PageConstructor\Components\Services;

final class CheckImplementing
{
    public static function isImplementing($class,$interface):bool{
        $interfaces = class_implements($class);
        if (isset($interfaces[$interface])) {
            return true;
        }
        return false;
    }

}