<?php

namespace Svkuaod\PageConstructor\Tools;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Components\SEO\SeoableInterface;
use Svkuaod\PageConstructor\Components\Services\CheckImplementing;
use Svkuaod\PageConstructor\Models\Constructor\Constructor;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOption;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorOptionResourse;
use Svkuaod\PageConstructor\Models\Constructor\Custom\FooterPage;
use Svkuaod\PageConstructor\Models\Constructor\Custom\HeaderPage;
use Svkuaod\PageConstructor\Models\Constructor\Custom\MainPage;
use Svkuaod\PageConstructor\Models\Constructor\Custom\NotFoundPage;

final class ConstructorTool
{
    private $optionalData;

    public function getRenderedPage(?Constructor $page)
    {
        return $this->renderInfoPage($page);
    }

    public function getRenderedPageById($id, $container = null)
    {
        return $this->renderInfoPage(Constructor::getPageById($id), $container);
    }

    public function getRenderedPageByKey($key)
    {
        return $this->renderInfoPage(Constructor::getPageByKey($key));
    }

    public function getRenderedPageByUrl($url)
    {
        return $this->renderInfoPage(Constructor::getPageByUrl($url));
    }

    public function getRenderedOptionById($id)
    {
        $option = ConstructorOption::getOptionById($id);
        if ($option) return $this->renderOption($option);
        return null;
    }

    public function getNotFoundPageResponse(){
        $page = NotFoundPage::getPageByKey(NotFoundPage::getPageKey());
        if(!$page) return 'not found';
        $view = $this->getRenderedPage($page);
        $header_page = $this->getRenderedPageByKey(HeaderPage::getPageKey());
        $footer_page = $this->getRenderedPageByKey(FooterPage::getPageKey());
        $renderedOptions = ['view' => $view, 'header_page' => $header_page, 'footer_page' => $footer_page];
        $variables = array_merge($renderedOptions, ConstructorConfig::getInstance()->getPublicPaths());
        breadcrumbs()->addItem($page->info->name ?? '',$page->url ?? '');
        if(CheckImplementing::isImplementing(get_class($page),SeoableInterface::class)) $variables = array_merge($variables,$page->getSeoData());
        return response()->view(ConstructorConfig::getInstance()->getAppPath(), $variables, 404);
    }

    public function setOptionalData($key,$value){
        $this->optionalData[$key] = $value;
    }

    public static function getChildResourceByKey(?ConstructorOptionResourse $resource, $key)
    {
        if(!$resource) return null;
        $childResource = $resource->childs->where('key', $key)->first();
        if (!$childResource) {
            $childResource = new ConstructorOptionResourse();
            $childResource->parent_id = $resource->id;
            $childResource->key = $key;
            $childResource->save();
        }
        return $childResource;
    }

    private function renderInfoPage(?Constructor $page, $container = null)
    {
        if(!$page) return false;
        $view = '';
        foreach ($page->options as $option) {
            $view .= $this->renderOption($option, $container);
        }
        return $view;
    }

    private function renderOption(ConstructorOption $option, $container = null)
    {
        $innerViews = [];
        if (isset($option->childs) && count($option->childs)) {
            foreach ($option->childs as $child) {
                $innerViews[] = $this->renderOption($child);
            }
        }
        $template = $option->template->view;
        if (!$container) $container = $option->full_width ? 'container-fluid' : 'custom-container container';
        $data = '';
        $innerKey = ConstructorConfig::getInstance()->getKeyInnerInfoPage();
        $resourceInner = $innerKey ? $option->resources->where('key', $innerKey)->first() : null;
        $pageInnerId = $resourceInner ? $resourceInner->info->value ?? null : null;

        if ($pageInnerId) $data = $this->getRenderedPageById($pageInnerId, $container);

        $variables = ['option' => $option, 'container' => $container, 'data' => $data, 'innerViews' => $innerViews, 'optionalData' => $this->optionalData];
        $variables = array_merge($variables,
            ConstructorConfig::getInstance()->getResourceKeys(),
            ConstructorConfig::getInstance()->getPublicPaths(),
            ConstructorConfig::getInstance()->getResourceVariables($option));
        $fullPath = ConstructorConfig::getInstance()->getPublicFolderPath() . '.' . $template;
        $view = view($fullPath, $variables)->render();
        return $view;
    }
}