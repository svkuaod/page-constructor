<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor;

use AdminColumn;
use AdminColumnEditable;
use AdminColumnFilter;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;

use Svkuaod\PageConstructor\Components\Descriptions\DescriptionableAdmin;
use Svkuaod\PageConstructor\Components\Prices\PriceableAdmin;
use Svkuaod\PageConstructor\Components\SEO\SeoableAdmin;


/**
 * Class BaseModel
 *
 * @property \Svkuaod\PageConstructor\Models\BaseModel $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class BaseModel extends Section implements Initializable
{
    use SeoableAdmin;
    use DescriptionableAdmin;
    use PriceableAdmin;

    /**
     * @return DisplayInterface
     */
    public abstract function onDisplay();

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public abstract function onEdit($id);

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
    }

}
