<?php

namespace Svkuaod\PageConstructor\AdminSections\FeedBack;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Navigation\Page;

/**
 * Class FeedBack
 *
 * @property \Svkuaod\PageConstructor\Models\FeedBack\FeedBack $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class FeedBack extends FeedBackBase
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Обратная связь';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\FeedBack\FeedBack::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('feedback')->addPage($page);
        });
    }
}
