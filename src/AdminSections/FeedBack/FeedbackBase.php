<?php

namespace Svkuaod\PageConstructor\AdminSections\FeedBack;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class FeedBackBase
 *
 * @property \Svkuaod\PageConstructor\Models\FeedBack\FeedbackBase $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class FeedBackBase extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Обратная связь';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->setDisplaySearch(true)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Name'),
                AdminColumn::text('phone', 'Phone'),
                AdminColumn::text('email', 'Email'),
                AdminColumn::text('checked', 'Checked'),
                AdminColumn::text('created_at', 'Created at')
            )->setApply([
                function ($query) {
                    $query->orderBy('created_at', 'desc');
                },])->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('checked', 'Отработанно?'),
            AdminFormElement::text('name', 'Имя'),
            AdminFormElement::text('phone', 'Телефон'),
            AdminFormElement::text('email', 'Email'),
            AdminFormElement::textarea('body', 'Описание'),
            AdminFormElement::date('created_at', 'Дата создания')->setReadOnly(1),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

}
