<?php

namespace Svkuaod\PageConstructor\AdminSections\MarketingCodes;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class MarketingCode
 *
 * @property \Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class MarketingCode extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Коды маркетинга';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::tree();
        $display->setRootParentId(null);
        $display->setParentField('parent_id');
        $display->setOrderField('order');
        $display->setValue('name');
        $display->setMaxDepth(2);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $model = \Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::find($id);
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('name', 'Название'),
            AdminFormElement::select('parent_id', 'Родитель')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->whereNull('parent_id');
                })
                ->setModelForOptions(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::class)
                ->setDisplay('name')
                ->required(),
            AdminFormElement::textarea('body', 'Код'),
        ]);
        return $form;
    }

    public function isEditable(Model $model)
    {
        if($model->key) return false;
        return true;
    }

    public function isDeletable(Model $model)
    {
        if($model->key) return false;
        return true;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('name', 'Название'),
            //AdminFormElement::select('key', 'Ключ')->setEnum(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::getArrKeys())->nullable(),
            AdminFormElement::select('parent_id', 'Родитель')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->whereNull('parent_id');
                })
                ->setModelForOptions(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::class)
                ->setDisplay('name')
                ->required(),
            AdminFormElement::textarea('body', 'Код'),
        ]);
        return $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('settings')->addPage($page);
        });
    }

}
