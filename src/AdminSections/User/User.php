<?php

namespace Svkuaod\PageConstructor\AdminSections\User;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class User
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class User extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->setDisplaySearch(true)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::text('email', 'Email'),
                AdminColumn::text('phone', 'Телефон'),
                AdminColumnEditable::checkbox('is_admin', 'Да', 'Нет')->setLabel('Администратор?'),
                AdminColumn::text('created_at', 'Created at')
            )->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('is_admin', 'Администратор?'),
            AdminFormElement::text('name', 'Имя'),
            AdminFormElement::text('email', 'Email'),
            AdminFormElement::text('phone', 'Телефон'),
            AdminFormElement::password('password', 'Пароль пользователя')
                ->hashWithBcrypt()
                ->setHelpText('Оставьте пустым, чтобы сохранить текущий пароль пользователя.'),
            AdminFormElement::date('created_at', 'Дата создания')->setReadOnly(1),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\App\User::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('users')->addPage($page);
        });
    }

}
