<?php

namespace Svkuaod\PageConstructor\AdminSections\Reviews;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class ReviewMain
 *
 * @property \Svkuaod\PageConstructor\Models\Reviews\ReviewMain $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class ReviewMain extends ReviewBase
{

    /**
     * @var string
     */
    protected $title = 'Отзывы на главной';


    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Reviews\ReviewMain::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('reviews')->addPage($page);
        });
    }

}
