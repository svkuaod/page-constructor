<?php

namespace Svkuaod\PageConstructor\AdminSections\Reviews;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class ReviewBase
 *
 * @property \Svkuaod\PageConstructor\Models\Reviews\ReviewBase $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class ReviewBase extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Отзывы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->setDisplaySearch(true)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Name'),
                AdminColumn::text('email', 'Phone'),
                AdminColumn::text('country', 'Email'),
                AdminColumn::text('body', 'body'),
                AdminColumn::text('active', 'Проверен'),
                AdminColumn::text('main_page', 'Отображать на Главной?'),
                AdminColumn::text('created_at', 'Created at')
            )->setApply([
                function ($query) {
                    $query->orderBy('created_at', 'desc');
                },])->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Проверен?'),
            AdminFormElement::text('name', 'Имя'),
            AdminFormElement::text('email', 'Email'),
            AdminFormElement::text('country', 'Страна'),
            AdminFormElement::textarea('body', 'Отзыв'),
            AdminFormElement::text('ip', 'IP адресс')->setReadOnly(1),
            AdminFormElement::checkbox('main_page', 'Отображать на Главной?'),
            AdminFormElement::date('created_at', 'Дата создания')->setReadOnly(1),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

}
