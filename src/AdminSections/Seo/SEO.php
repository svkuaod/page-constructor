<?php

namespace Svkuaod\PageConstructor\AdminSections\Seo;

use AdminColumn;
use AdminColumnEditable;
use AdminColumnFilter;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;
use Svkuaod\PageConstructor\AdminSections\Constructor\BaseModel;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorInfoPage;


/**
 * Class SEO
 *
 * @property \Svkuaod\PageConstructor\Components\SEO\SEO $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class SEO extends BaseModel
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'SEO';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()->setDisplaySearch(false)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('url', 'Url'),
                AdminColumn::text('resource_type', 'Model Name'),
                AdminColumn::text('ResourceInfoName', 'Model Name'),
                AdminColumn::datetime('created_at', 'Create at'),
                AdminColumnEditable::checkbox('active', 'Да', 'Нет')->setLabel('Включен?')
            )->paginate(50);
        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('Id')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Url')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Model Class')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Model Name')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Created_at')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('active')->setOperator(FilterInterface::CONTAINS),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $model = \Svkuaod\PageConstructor\Components\SEO\SEO::where('id',$id)->with('resource')->first();
        $fields = [AdminFormElement::checkbox('active', 'Active')];
        if($model->resource ?? false) $fields = array_merge($fields,[
            AdminFormElement::text('resource_type', 'Resource Class')->setReadOnly(1),
            AdminFormElement::text('resourceInfoName', 'Resource Name')->setReadOnly(1),
            AdminColumn::relatedLink('resource.url'),
        ]);
        else $fields = array_merge($fields,[
            AdminFormElement::text('url','Full Url')->unique()->addValidationRule('max:175'),
        ]);
        $fields = array_merge($fields,[
            AdminFormElement::text('meta_title','title')->addValidationRule('max:512'),
            AdminFormElement::text('meta_keywords','keywords')->addValidationRule('max:512'),
            AdminFormElement::textarea('meta_description','description')->addValidationRule('max:1024'),
            AdminFormElement::ckeditor('meta_text','seo text'),
        ]);
        $form = AdminForm::form()->setElements($fields);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Components\SEO\SEO::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('seo')->addPage($page);
        });
    }
}
