<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor;

/**
 * Class TemplateBase
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\TemplateBase $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class TemplateBase extends Template
{
    protected function getModelClass(){
        return \Svkuaod\PageConstructor\Models\Constructor\TemplateBase::class;
    }

    /**
     * @var string
     */
    protected $title = 'Шаблоны (Базовые)';

}
