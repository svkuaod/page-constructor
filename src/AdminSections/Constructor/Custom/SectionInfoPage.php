<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor\Custom;

use SleepingOwl\Admin\Navigation\Page;
use Svkuaod\PageConstructor\AdminSections\Constructor\Constructor;


/**
 * Class SectionInfoPage
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\Custom\SectionInfoPage $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class SectionInfoPage extends Constructor
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Повторяющиеся секции';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Constructor\Custom\SectionInfoPage::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('filling')->addPage($page);
        });
    }
}
