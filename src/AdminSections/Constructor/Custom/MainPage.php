<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor\Custom;

use SleepingOwl\Admin\Navigation\Page;
use Svkuaod\PageConstructor\AdminSections\Constructor\Constructor;
use Svkuaod\PageConstructor\Models\Constructor\Custom\MainPage as EntityPage;


/**
 * Class MainPage
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\Custom\MainPage $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class MainPage extends Constructor
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Главная страница';

    /**
     * @var string
     */
    protected $alias;


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $model = EntityPage::first();
        if (!$model) {
            $model = new EntityPage();
            $model->active = '1';
            $model->url = EntityPage::getPageKey();
            $model->save();
        }
        return redirect('admin/main_pages/' . $model->id . '/edit');
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Constructor\Custom\MainPage::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('filling')->addPage($page);
        });
    }
}
