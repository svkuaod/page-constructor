<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor\Custom;

use SleepingOwl\Admin\Navigation\Page;
use Svkuaod\PageConstructor\AdminSections\Constructor\Constructor;
use Svkuaod\PageConstructor\Models\Constructor\Custom\NotFoundPage as EntityPage;


/**
 * Class NotFoundPage
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\Custom\NotFoundPage $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class NotFoundPage extends Constructor
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = '404 страница';

    /**
     * @var string
     */
    protected $alias;


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $model = EntityPage::first();
        if (!$model) {
            $model = new EntityPage();
            $model->active = '1';
            $model->url = EntityPage::getPageKey();
            $model->save();
        }
        return redirect('admin/not_found_pages/' . $model->id . '/edit');
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Constructor\Custom\NotFoundPage::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('filling')->addPage($page);
        });
    }
}
