<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class Template
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\Template $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class Template extends Section implements Initializable
{
    protected abstract function getModelClass();

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Шаблоны конструктора';

    /**
     * @var string
     */
    protected $alias;


    public function isDeletable(Model $model)
    {
        if ($model->is_deletable) return true;
        else return false;
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::tree();
        $display->setRootParentId(null);
        $display->setParentField('parent_id');
        $display->setOrderField('order');
        $display->setValue('name');
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('name', 'Название'),
            AdminFormElement::text('view', 'Имя шаблона'),
            AdminFormElement::image('photoAdmin', 'Превью'),
            AdminFormElement::select('parent_id', 'Родитель')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->whereNull('parent_id');
                })
                ->setModelForOptions($this->getModelClass())
                ->setDisplay('name')
                ->nullable(),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('name', 'Название'),
            AdminFormElement::select('parent_id', 'Родитель')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->whereNull('parent_id');
                })
                ->setModelForOptions($this->getModelClass())
                ->setDisplay('name')
                ->nullable(),
        ]);
        return $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page($this->getModelClass());
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('filling')->addPage($page);
        });
    }
}
