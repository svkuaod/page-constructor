<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor;

use AdminColumn;
use AdminColumnEditable;
use AdminColumnFilter;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;

use Svkuaod\PageConstructor\Components\ConstructorConfig;
use Svkuaod\PageConstructor\Models\Constructor\TemplateBase;
use Svkuaod\PageConstructor\Models\Constructor\TemplateCustom;

/**
 * Class Constructor
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\Constructor $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class Constructor extends BaseModel
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()->setOrder([[0, 'desc']])->setDisplaySearch(false)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('info.name', 'Name'),
                AdminColumn::text('url', 'Url'),
                AdminColumn::datetime('created_at', 'Url'),
                AdminColumnEditable::checkbox('active', 'Да', 'Нет')->setLabel('Включен?')
            )->paginate(50);
        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('Id')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Name')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Url')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Created_at')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('active')->setOperator(FilterInterface::CONTAINS),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $model = \Svkuaod\PageConstructor\Models\Constructor\Constructor::where('id', $id)->with(['options'])->first();
        $generalTab = [
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('info.name', 'Название')->unique()->required(),
            AdminFormElement::text('url', 'Url')->unique(),
            AdminFormElement::html($this->getButtonsBlock($id)),

            $this->getAjaxConstructor($model)
        ];
        $previewTab = $this->getDescriptionFields($this->model);
        $seoTab = $this->getSeoFields($this->model);
        $priceTab = $this->getPriceFields($this->model);
        $tabsArr = ['Конструктор' => new FormElements($generalTab),];
        if($priceTab) $tabsArr = array_merge($tabsArr,['Цена' => new FormElements($priceTab)]);
        if($previewTab) $tabsArr = array_merge($tabsArr,['Превью' => new FormElements($previewTab)]);
        if($seoTab) $tabsArr = array_merge($tabsArr,['SEO' => new FormElements($seoTab)]);
        $tabsArr = array_merge($tabsArr,$this->getAdditionalTabs());
        $tabs = AdminDisplay::tabbed($tabsArr);
        return AdminForm::panel()->setHtmlAttribute('enctype', 'multipart/form-data')->addBody($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('info.name', 'Название')->unique()->required(),
            AdminFormElement::hidden('url', 'Url')->unique(),
        ]);
        return $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    protected function getAdditionalTabs(){
        return [];
    }

    /**
     * Initialize class.
     */

    private function getAjaxConstructor(\Svkuaod\PageConstructor\Models\Constructor\Constructor $infoPage)
    {
        $templates = collect([
            TemplateBase::getMorphKey() => TemplateBase::get(),
            TemplateCustom::getMorphKey() => TemplateCustom::get()
        ]);
        $renderedTemplates = [];
        foreach ($infoPage->options as $option) {
            $renderedTemplates[] = $this->getRenderedView($option, $templates);
        }
        $view = AdminFormElement::view(ConstructorConfig::getInstance()->getAdminConstructorPath() . '.constructor', compact('templates', 'infoPage', 'renderedTemplates'), function (Model $model, Request $request) {
        });
        return $view;
    }

    private function getRenderedView($option, $templates)
    {
        $innerViews = [];
        if (isset($option->childs) && count($option->childs)) {
            $optionChilds = $option->childs->sortBy('order');
            foreach ($optionChilds as $child) {
                $innerViews[] = $this->getRenderedView($child, $templates);
            }
        }
        $variables = ['option' => $option, 'templates' => $templates, 'innerViews' => $innerViews];
        $template = ConstructorConfig::getInstance()->getAdminConstructorPath() . '.' . $option->template->view;
        $variables = array_merge($variables, ConstructorConfig::getInstance()->getResourceKeys(), 
            ConstructorConfig::getInstance()->getAdminPaths(), 
            ConstructorConfig::getInstance()->getResourceVariables($option));
        $renderedViews = view($template, $variables)->render();
        return $renderedViews;
    }

    private function getButtonsBlock($id){
        return '<div style="display: flex;justify-content: space-between">'
        . $this->getPreviewButton($id) . $this->getCopyButton($id)
        . '</div>';
    }

    private function getPreviewButton($id)
    {
        return '<div style="margin:25px"><a class="preview-page" href="/preview/page/' . $id . '" target="_blank">Предпросмотр страницы</a></div>';
    }

    private function getCopyButton($id)
    {
        if(count(ConstructorConfig::getInstance()->getLanguages()) > 1)
            return '<div style="margin:25px"><a class="preview-page" onclick="copyInfoData(' . $id . ')">Дублировать на все языки</a></div>';
        else return '';
    }


}
