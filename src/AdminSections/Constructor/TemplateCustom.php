<?php

namespace Svkuaod\PageConstructor\AdminSections\Constructor;

/**
 * Class TemplateCustom
 *
 * @property \Svkuaod\PageConstructor\Models\Constructor\TemplateCustom $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class TemplateCustom extends Template
{
    protected function getModelClass(){
        return \Svkuaod\PageConstructor\Models\Constructor\TemplateCustom::class;
    }

    /**
     * @var string
     */
    protected $title = 'Шаблоны (custom)';

}
