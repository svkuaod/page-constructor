<?php

namespace Svkuaod\PageConstructor\AdminSections\Blocks;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Page;
use Svkuaod\PageConstructor\AdminSections\Constructor\BaseModel;

/**
 * Class Block
 *
 * @property \Svkuaod\PageConstructor\Models\Blocks\Block $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class Block extends BaseModel
{
    /**
     * @return array
     */
    protected function customFields() : array {
        return [];
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'страницы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @inheritDoc
     */
    protected function getNavigationId()
    {
        return 'filling';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->setDisplaySearch(true)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('info.url', 'Url'),
                AdminColumn::text('title', 'Title'),
                AdminColumn::text('active', 'Active'),
                AdminColumn::text('created_at', 'Created at')
            )->setApply([
                function ($query) {
                    $query->orderBy('created_at', 'desc');
                },])->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $generalTab = [
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('title', 'Название'),
            AdminFormElement::ckeditor('info.body', 'Контент'),
        ];
        if($this->customFields()) $generalTab = array_merge($generalTab,$this->customFields());
        $tabsArr = ['Основное' => new FormElements($generalTab)];
        $tabs = AdminDisplay::tabbed($tabsArr);
        return AdminForm::panel()->setHtmlAttribute('enctype', 'multipart/form-data')->addBody($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * @return string
     */
    protected abstract function getModelClass();


    public function initialize()
    {
        $page = new Page($this->getModelClass());
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById($this->getNavigationId())->addPage($page);
        });
    }

}
