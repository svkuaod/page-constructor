<?php

namespace Svkuaod\PageConstructor\AdminSections\Blocks;

use Svkuaod\PageConstructor\Models\Blocks\Custom\ContactBlock as EntityPage;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Form\FormElements;
use AdminFormElement;
use AdminDisplay;
use AdminForm;
use AdminColumnEditable;
use AdminColumn;

/**
 * Class ContactBlock
 *
 * @property \Svkuaod\PageConstructor\Models\Blocks\Custom\ContactBlock $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class ContactBlock extends Block
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Блок контакты';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return \Svkuaod\PageConstructor\Models\Blocks\Custom\ContactBlock::class;
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        if (!$model = EntityPage::first()) {
            $model = new EntityPage();
            $model->title = 'Блок контакты';
            $model->active = '1';
            $model->save();
        }
        return redirect('admin/contact_blocks/' . $model->id . '/edit');

    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $generalTab = [
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('phones', 'Телефоны')->setHelpText('Разделитель ","'),
            AdminFormElement::text('phonesLinks', 'Номера(только цифры)')->setHelpText('Разделитель ","'),
            AdminFormElement::text('emails', 'Email')->setHelpText('Разделитель ","'),
        ];
        $tabsArr = ['Основное' => new FormElements($generalTab)];
        $tabs = AdminDisplay::tabbed($tabsArr);
        return AdminForm::panel()->setHtmlAttribute('enctype', 'multipart/form-data')->addBody($tabs);
    }

}
