<?php

namespace Svkuaod\PageConstructor\AdminSections\Blocks;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Form\FormElements;
use AdminFormElement;
use AdminDisplay;
use AdminForm;
use AdminColumnEditable;
use AdminColumn;

/**
 * Class SocialBlock
 *
 * @property \Svkuaod\PageConstructor\Models\Blocks\Custom\SocialBlock $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class SocialBlock extends Block
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Социальные сети';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return \Svkuaod\PageConstructor\Models\Blocks\Custom\SocialBlock::class;
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()->setDatatableAttributes(['bInfo' => false])->setDisplaySearch(true)->paginate(10);
        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setApply(function ($query) {
            $query->orderBy('order', 'asc');
        });

        $display->setColumns([
            $photo = AdminColumn::image('imagePath', 'Photo<br/><small>(image)</small>')
                ->setHtmlAttribute('class', 'hidden-sm hidden-xs foobar')
                ->setWidth('100px'),

            AdminColumn::text('title', 'Title'),
            AdminColumn::link('linkPath', 'Link'),
            AdminColumnEditable::checkbox('active', 'Active'),
            AdminColumn::order()
                ->setLabel('Order')
                ->setHtmlAttribute('class', 'text-center')
                ->setWidth('100px'),
        ]);
        $photo->getHeader()->setHtmlAttribute('class', 'hidden-sm hidden-xs');
        return $display;

    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $generalTab = [
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('title', 'Название'),
            AdminFormElement::image('imagePath', 'Картинка'),
            AdminFormElement::image('imageTwoPath', 'Картинка (Доп)'),
            AdminFormElement::text('linkPath', 'Ссылка на социальную сеть'),
        ];
        $tabsArr = ['Основное' => new FormElements($generalTab)];
        $tabs = AdminDisplay::tabbed($tabsArr);
        return AdminForm::panel()->setHtmlAttribute('enctype', 'multipart/form-data')->addBody($tabs);
    }

}
