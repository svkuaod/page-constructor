<?php

namespace Svkuaod\PageConstructor\AdminSections\Subscribers;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class Subscriber
 *
 * @property \Svkuaod\PageConstructor\Models\Subscribers\Subscriber $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Subscriber extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Подписчики';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->setDisplaySearch(true)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('email', 'Email'),
                AdminColumn::text('phone', 'Телефон'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::text('ip', 'IP'),
                AdminColumn::text('created_at', 'Created at')
            )->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $form = AdminForm::form()->setElements([
            //  AdminFormElement::text('key', 'Ключ')->setReadOnly(1),
            AdminFormElement::text('email', 'Email')->unique(),
            AdminFormElement::text('phone', 'Телефон'),
            AdminFormElement::text('name', 'Имя'),
            AdminFormElement::text('ip', 'IP'),
            AdminFormElement::date('created_at', 'Дата создания')->setReadOnly(1),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Subscribers\Subscriber::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('subscribers')->addPage($page);
        });
    }

}
