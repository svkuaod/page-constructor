<?php

namespace Svkuaod\PageConstructor\AdminSections\Translates;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use Svkuaod\PageConstructor\AdminSections\Constructor\BaseModel;
use SleepingOwl\Admin\Navigation\Page;

/**
 * Class Translate
 *
 * @property \Svkuaod\PageConstructor\Models\Translates\Translate $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Translate extends BaseModel
{
    /**
     * @return array
     */
    protected function customFields() : array {
        return [];
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Переводы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @inheritDoc
     */
    protected function getNavigationId()
    {
        return 'blocks';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()->setDisplaySearch(false)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                //AdminColumn::image('imagePath', 'Preview')->setHtmlAttribute('class', 'hidden-sm hidden-xs foobar')->setWidth('200px')->setOrderable(false),
                AdminColumn::text('key', 'Key'),
                AdminColumn::text('info.value', 'Value'),
                AdminColumn::text('description', 'Description'),
                AdminColumnEditable::checkbox('active', 'Active'),
                AdminColumn::text('created_at', 'Created at')
            )->setApply([
                function ($query) {
                    $query->orderBy('created_at', 'desc');
                },])->paginate(30);
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Ключ')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::text()->setPlaceholder('Значение')->setOperator(FilterInterface::CONTAINS),
            AdminColumnFilter::select()->setPlaceholder('Описание')->setOperator(FilterInterface::CONTAINS),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('key', 'Key'),
            AdminFormElement::text('description', 'Описание'),
            //AdminFormElement::image('imagePath', 'Превью')->setReadOnly(!Permissions::isAuthRoot()),
            AdminFormElement::text('info.value', 'Значение'),
            AdminFormElement::date('updated_at', 'Дата создания')->setReadOnly(1),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return \Svkuaod\PageConstructor\Models\Translates\Translate::class;
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Translates\Translate::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('filling')->addPage($page);
        });
    }
}
