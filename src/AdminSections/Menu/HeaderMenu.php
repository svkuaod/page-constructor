<?php

namespace Svkuaod\PageConstructor\AdminSections\Menu;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Navigation\Page;
use Svkuaod\PageConstructor\Models\Constructor\ConstructorInfoPage;

/**
 * Class HeaderMenu
 *
 * @property \Svkuaod\PageConstructor\Models\Menu\HeaderMenu $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class HeaderMenu extends Menu
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Хедер меню';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Menu\HeaderMenu::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('menu')->addPage($page);
        });
    }
}
