<?php

namespace Svkuaod\PageConstructor\AdminSections\Menu;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;
use Svkuaod\PageConstructor\AdminSections\Constructor\BaseModel;
use Svkuaod\PageConstructor\Components\Descriptions\DescriptionableAdmin;
use Svkuaod\PageConstructor\Models\Constructor\Custom\InfoPage;

/**
 * Class Menu
 *
 * @property \Svkuaod\PageConstructor\Models\Menu\Menu $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
abstract class Menu extends BaseModel
{
    use DescriptionableAdmin;
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Главное меню';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::tree();
        $display->setRootParentId(null);
        $display->setParentField('parent_id');
        $display->setOrderField('order');
        $display->setValue('nameAdmin');
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $generalTab = [
            AdminFormElement::checkbox('active', 'Active'),
            AdminFormElement::text('info.name', 'Название'),
            AdminFormElement::text('urlAdmin', 'Url'),
            AdminFormElement::select('parent_id', 'Родитель')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->whereNull('parent_id');
                })
                ->setModelForOptions(\Svkuaod\PageConstructor\Models\Menu\Menu::class)
                ->setDisplay('info.name')
                ->nullable(),
            AdminFormElement::select('constructor_id', 'Инфостраница')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->where('active', '1');
                })
                ->setModelForOptions(InfoPage::class)
                ->setDisplay('info.name')
                ->nullable(),
            AdminFormElement::image('iconPath', 'Фото'),
        ];
        $previewTab = $this->getDescriptionFields($this->model);
        $tabsArr = ['Основное' => new FormElements($generalTab),];
        if($previewTab) $tabsArr = array_merge($tabsArr,['Превью' => new FormElements($previewTab)]);
        $tabs = AdminDisplay::tabbed($tabsArr);
        return AdminForm::panel()->setHtmlAttribute('enctype', 'multipart/form-data')->addBody($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

}
