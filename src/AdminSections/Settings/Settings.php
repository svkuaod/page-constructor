<?php

namespace Svkuaod\PageConstructor\AdminSections\Settings;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Section;

/**
 * Class Settings
 *
 * @property \Svkuaod\PageConstructor\Models\Settings\Settings $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Settings extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Настройки';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->setDisplaySearch(true)->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('key', 'Ключ'),
                AdminColumn::text('value', 'Значение'),
                AdminColumn::text('created_at', 'Created at')
            )->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $form = AdminForm::form()->setElements([
            AdminFormElement::text('key', 'Ключ')->setReadOnly(1),
            AdminFormElement::text('value', 'Значение')->setHelpText('Перечень значений перечисляйте через: ;'),
            AdminFormElement::date('created_at', 'Дата создания')->setReadOnly(1),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::select('key', 'Ключ')->setEnum(\Svkuaod\PageConstructor\Models\Settings\Settings::getArrKeys())->unique(),
            AdminFormElement::text('value', 'Значение')->setHelpText('Перечень значений перечисляйте через: ;'),
        ]);
        return $form;
    }

    function isDeletable(Model $model)
    {
        return false;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $page = new Page(\Svkuaod\PageConstructor\Models\Settings\Settings::class);
        $page->setPriority(1);
        app()->booted(function () use ($page) {
            \AdminNavigation::getPages()->findById('settings')->addPage($page);
        });
    }

}
