Page-constructor
=================
[![Laravel 5](https://img.shields.io/badge/Laravel-5-orange.svg?style=flat-square)](http://laravel.com)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

Пакет для удобного создания и использования инфостраниц. Включает в себя модули:
- создания меню
- создание главной страницы доступной по пути `/`
- создания инфостраниц доступных по пути `/{url}`
- создание seo данных
Требования: 
- php 7.1; 
- Laravel 5.6
- SleepingOwlAdmin panel v.4*

Установка
------------------
Установка пакета с помощью Composer.

```
composer require svkuaod/page-constructor
```

Добавьте в файл `config/app.php` вашего проекта в конец массива `providers` :

```
Svkuaod\PageConstructor\PageConstructorServiceProvider::class,
```

После этого выполните в консоли команду публикации нужных ресурсов:

```
php artisan vendor:publish --provider="Svkuaod\PageConstructor\PageConstructorServiceProvider"
```

Запустите миграции 
```
php artisan migrate --path=vendor/svkuaod/page-constructor/src/database/migrations/base
php artisan migrate --path=vendor/svkuaod/page-constructor/src/database/migrations/langs
```

Запустите сидинг базовых возможностей конструктора
```
php artisan db:seed --class=Svkuaod\PageConstructor\database\seeds\PageConstructorSeeder
```

добавьте в `app/Providers/AppServiceProvider.php` в метод `public function boot()` 
```
        $widgetsRegistry = $this->app[\SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface::class];
           foreach ($this->widgets as $widget) {
              $widgetsRegistry->registerWidget($widget);
           }
```
добавьте в `app/Providers/AppServiceProvider.php` метод
```
 protected $widgets = [
        \Svkuaod\PageConstructor\Widgets\LangSwitcher::class,
    ];
```
добавьте в `app/Providers/AppServiceProvider.php` в метод `public function register()`
`
require_once __DIR__.'/../../vendor/svkuaod/page-constructor/src/helpers/customHelpers.php';
`

*Для корректного отображения главной страницы проекта настроенной через конструктор требуется удалить 
из текущего `routes/web.php` роут для урла `'/'` (главная страница)

Для интеграции в админ панель sleepingowl в файле `app/Providers/AdminSectionsServiceProvider.php` добавьте 
 - в `protected $sections`:
```
    \App\User::class => 'Svkuaod\PageConstructor\AdminSections\User\User',

    \Svkuaod\PageConstructor\Models\Menu\HeaderMenu::class => 'Svkuaod\PageConstructor\AdminSections\Menu\HeaderMenu',
    
    \Svkuaod\PageConstructor\Models\FeedBack\FeedBack::class => 'Svkuaod\PageConstructor\AdminSections\FeedBack\FeedBack',
    
    \Svkuaod\PageConstructor\Models\Reviews\ReviewMain::class => 'Svkuaod\PageConstructor\AdminSections\Reviews\ReviewMain',
    \Svkuaod\PageConstructor\Models\Reviews\ReviewPublic::class => 'Svkuaod\PageConstructor\AdminSections\Reviews\ReviewPublic',
    
    \Svkuaod\PageConstructor\Models\Subscribers\Subscriber::class => 'Svkuaod\PageConstructor\AdminSections\Subscribers\Subscriber',
    \Svkuaod\PageConstructor\Models\Settings\Settings::class => 'Svkuaod\PageConstructor\AdminSections\Settings\Settings',
                      
    \Svkuaod\PageConstructor\Models\Constructor\TemplateCustom::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\TemplateCustom',
                     
    \Svkuaod\PageConstructor\Components\SEO\SEO::class => 'Svkuaod\PageConstructor\AdminSections\SEO\SEO',
    
    \Svkuaod\PageConstructor\Models\MarketingCodes\MarketingCode::class => 'Svkuaod\PageConstructor\AdminSections\MarketingCodes\MarketingCode',
                     
    \Svkuaod\PageConstructor\Models\Constructor\Custom\InfoPage::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\Custom\InfoPage',
    \Svkuaod\PageConstructor\Models\Constructor\Custom\MainPage::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\Custom\MainPage',
    \Svkuaod\PageConstructor\Models\Constructor\Custom\HeaderPage::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\Custom\HeaderPage',
    \Svkuaod\PageConstructor\Models\Constructor\Custom\FooterPage::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\Custom\FooterPage',
    \Svkuaod\PageConstructor\Models\Constructor\Custom\SectionInfoPage::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\Custom\SectionInfoPage',
    \Svkuaod\PageConstructor\Models\Constructor\Custom\NotFoundPage::class => 'Svkuaod\PageConstructor\AdminSections\Constructor\Custom\NotFoundPage',

    \Svkuaod\PageConstructor\Models\Translates\Translate::class => 'Svkuaod\PageConstructor\AdminSections\Translates\Translate',
    \Svkuaod\PageConstructor\Models\Blocks\Custom\ContactBlock::class => 'Svkuaod\PageConstructor\AdminSections\Blocks\Custom\ContactBlock',
    \Svkuaod\PageConstructor\Models\Blocks\Custom\SocialBlock::class => 'Svkuaod\PageConstructor\AdminSections\Blocks\Custom\SocialBlock',
```
- в `public function boot`:
  ```
          \SleepingOwl\Admin\Facades\Meta::addCss('custom', 'vendor/page_constructor/css/admin.css', 'admin-default');
          \SleepingOwl\Admin\Facades\Meta::addJs('custom', 'vendor/page_constructor/js/admin.js', 'admin-default');
  ```

В файле `app/Admin/navigation.php` добавьте секции:
```
              [
                  'title' => 'Наполнение',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'filling',
              ],
              [
                  'title' => 'Меню',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'menu',
              ],
              [
                  'title' => 'Обратная связь',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'feedback',
              ],
              [
                  'title' => 'Подписчики',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'subscribers',
              ],
              [
                  'title' => 'Настройки',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'settings',
              ],
              [
                  'title' => 'Пользователи',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'users',
              ],
              [
                  'title' => 'Отзывы',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'reviews',
              ],
              [
                  'title' => 'Сео настройки',
                  'icon'  => 'fa fa-circle',
                  'id'   => 'seo',
              ],
```
Базовая локализация `ru`. Для этого в `config/app.php` нужно установить `'locale' => 'ru',`

Для использования аутентификаций в админ панель :
в файле `config/sleeping_owl.php` добавить в масив `'middleware'` алиас `\Svkuaod\PageConstructor\Middleware\IsAdminMiddleware::class`. 
Пример : `'middleware' => ['web',\Svkuaod\PageConstructor\Middleware\IsAdminMiddleware::class],`

Выполните для использования базовой аутентификаций `php artisan make:auth`
Выполните для использования загрузки файлов `php artisan storage:link`

В `routes/web.php` оставьте только `\Illuminate\Support\Facades\Auth::routes();`;

Дефолтный логин в админ панель `test@test.test`
Пароль `test@test.test`

404 страница создается в админ панеле как автономная страница для всех запросов через контролер конструктор.
Для всех остальных запросов для обработки abort(404) нужно:
`app/Exceptions/Handler.php` в методе `public function render($request, Exception $exception)` добавить:
        `if ($this->isHttpException($exception)) {
             if ($exception->getStatusCode() == 404) {
                 return \Svkuaod\PageConstructor\Facade\Constructor::getNotFoundPageResponse();
             }
         }
         return parent::render($request, $exception);`
Использование
-------------

Базовые настройки пакета находятся в `config/page_constructor.php`
Ключ `template` - используемый «дизайн» для отрисовки публичной части, папка с шаблонами `resources/views/vendor/page_constructor/layouts/{template}`. 
Все новые файлы стилей и библиотек нужно подключать в файле  `resources/views/vendor/page_constructor/layouts/{template}/app.blade.php`.

В админ панели в группе `наполнение` расположены секции:
- главная страница : доступная по `/`;
- шаблоны конструктора: для необходимости дополнения опций конструктора 
- инфостраницы :  доступная по `/pages/{url}`  где url указанный в данной моделе, под названием страницы;
- блог :  доступная по `/blog/{url}`  где url указанный в данной моделе, под названием страницы;

В админ панели в группе `меню` расположены секции:
- главное меню : в моделе меню можно выбрать `инфостраницу` на которую будет ссылаться данный пункт меню, в данном случае `url` будет использоваться из модели `инфостраницы`. 