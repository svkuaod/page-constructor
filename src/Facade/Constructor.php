<?php

namespace Svkuaod\PageConstructor\Facade;

use Svkuaod\PageConstructor\Tools\ConstructorTool;

/**
 * Class Constructor
 *
 * @see ConstructorTool
 *
 * @method static getRenderedPage($page)
 * @method static getRenderedPageById($id, $container)
 * @method static getRenderedPageByKey($key)
 * @method static getRenderedOptionById($id)
 * @method static getRenderedPageByUrl($url)
 * @method static getNotFoundPageResponse()
 * @method static setOptionalData($key,$value)
 * @method static getChildResourceByKey(?$resource, $key)
 */
final class Constructor
{
    private static $instance = null;

    public static function __callStatic($method, $args)
    {
        if(!self::$instance) self::$instance = new ConstructorTool();
        return (self::$instance)->$method(...$args);
    }
}